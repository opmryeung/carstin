<?php
/*
Template Name: Full Width Page - Order Form - Additional Countertop
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							if(isset($_GET['message'])){
								echo '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Notice!</h4>'.$_GET['message'].'</div>';
							}
							
							echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
							
							echo '<form action="/process-post/?next=/review-info/&from=/additional-countertop/&session='.$order_session.'&insert='.$_GET['insert'].'" method="post" role="form">';
							echo '<field>';
							echo '<legend>Additional Countertop</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'AdditionalCounterTop','AdditionalCounterTop',(isset($_GET['AdditionalCounterTop'])?$_GET['AdditionalCounterTop']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Add more countertops or Submit order</button>';
							echo '</field>';
							echo '</form>';
							
							echo '</div>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>