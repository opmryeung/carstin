<?php
/*
 Template Name: Full Width Page - Login
 */
?>
<?php get_header(); ?>
<?php

include_once 'core-functions.php';
//include_once 'config.php';

date_default_timezone_set('America/Chicago');

/*$mysqli = new mysqli('localhost','opm_carstindevwp','a-s_2l;[cPFM','opm_carstindevwp');

$mysqli_data = new mysqli('localhost','opm_dealdevdata',')#BpEa-5F2!}','opm_dealdevdata');
*/

$mysqli = new mysqli('127.0.0.1','carstinstone','bnjQKXWN9Zg9Ks3pTh3H','wp_carstinstone');

//print_r($mysqli);


$mysqli_data = new mysqli('127.0.0.1','carstinstone','bnjQKXWN9Zg9Ks3pTh3H','wp_carstinstone');


require_once ( ABSPATH . 'wp-includes/class-phpass.php');


	$wp_hasher = new PasswordHash(8, TRUE);

	$data = readSQL($mysqli, "SELECT * FROM wp_wp_eMember_members_tbl WHERE user_name = '".$_POST['inputUsername']."'");

//echo "SELECT * FROM wp_wp_eMember_members_tbl WHERE user_name = '".$_POST['inputUsername']."'";

//print_r($data);

	$staticData = readSQL($mysqli_data, "SELECT * FROM DEALERS WHERE `Ship To Number` = '".$_POST['inputUsername']."'");

//print_r($staticData);

	$inputLog = date('Y-m-d H:i:s ').$_SERVER['REMOTE_ADDR'].'-'.$_POST['inputUsername'].'-'.$_POST['inputPassword'];
	
	
	// $check = $wp_hasher->CheckPassword($_POST['inputPassword'], $data[0]['password']);
	
	$check = !empty($data) ? $wp_hasher->CheckPassword($_POST['inputPassword'], $data[0]['password']) : false;

	if(empty($data))
	$inputLog.= ' - Username NOT Found';
	else $inputLog.= ' - Username Found';

	if($check){
		$inputLog.= '- Password Match';
		
		if(empty($staticData)){
			$userName = $data[0]['user_name'];
			$fullName = $data[0]['first_name'].' '.$data[0]['last_name'];
			$compName = $data[0]['company_name'];
			$phone = $data[0]['phone'];
			$email = $data[0]['email'];
			$sales = 'N/A';
		}else{
			$userName = $staticData[0]['Ship To Number'];
			$fullName = $staticData[0]['Primary Customer Contact'];
			$compName = $staticData[0]['Name'];
			$phone = $staticData[0]['Phone Number'];
			$email = $staticData[0]['Primary email'];
			$sales = $staticData[0]['Salesperson'];
		}
		
		
		
	}else $inputLog.= ' - Password NOT Match';

//echo $inputLog . " ---";

	if($_POST['inputUsername'] == 'guest no guest' && $_POST['inputPassword'] == 'guest is guest'){
		$check=true;
		$userName = 'Guest001';
		$fullName = 'John Smith';
		$compName = 'ACME';
		$phone = '972-123-4567';
		$email = 'guest@guest.com';
		$sales = 'Mary Sue';
		
		$inputLog.= ' [GUEST LOGIN]';
	}

	$inputLog.= ' - '.$_SERVER['HTTP_USER_AGENT'];

	if($_POST['submit']=='1')
		file_put_contents(get_template_directory().'/LOGIN_LOG/'.date('Ymd').'.log',$inputLog."\n",FILE_APPEND);

?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span12 clearfix" role="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>
			role="article">
			
		<!-- end article header --> <section class="post_content">

		<?php if($check){?>
			<form action="/calculate" method="post" class="form-horizontal" role="form">
			<input type="hidden" name="inputName" value="<?php echo $fullName.' - '.$userName; ?>">
			<input type="hidden" name="inputAcc" value="<?php echo $userName; ?>">
			<input type="hidden" name="inputCompany" value="<?php echo $compName; ?>">
			<input type="hidden" name="inputPhone" value="<?php echo $phone; ?>">
			<input type="hidden" name="inputEmail" value="<?php echo $email; ?>">
			<input type="hidden" name="inputSales" value="<?php echo $sales; ?>">
			<!-- <h1><strong>Login Successful</strong></h1>  -->
			<?php $file = file_get_contents(get_template_directory().'/ColorFile.ini'); ?>
			<p><a href="http://dealers.carstinbrands.com/wp-content/themes/wordpress-bootstrap-master/ColorGraph/<?php echo $file; ?>" class="btn btn-inverse btn-large" style="float:right;" target="_blank">View Color Menu</a></p>
			<p><button type="submit" class="btn btn-large btn-inverse" name="fromLogin" value="1">+ Create A New Quote Project</button></p>
			
			</form>
			<?php 
			
			$previousProjects = getProjectList($mysqli_data, $userName);
			
			echo '<hr/><h3>Project Log</h3>';
			echo printProjectTable($previousProjects);
			
			?>
		<?php }else{ ?>
			<div>
			<form action="/login/" method="post" class="form-horizontal" role="form">
				<div class="control-group">
					<label class="control-label" for="inputUsername">Username</label>
					<div class="controls">
						<input type="text" id="inputUsername" name="inputUsername" required autofocus>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">Password</label>
					<div class="controls">
						<input type="password" id="inputPassword" name="inputPassword"  required>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<!-- <label class="checkbox"> <input type="checkbox"> Remember me </label>  -->
						<button type="submit" class="btn" name="submit" value="1">Sign in</button>
					</div>
				</div>
			</form>
			</div>
		<?php } ?>

		</section> <!-- end article section --> <footer>

		<p class="clearfix">
		<?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?>
		</p>

		</footer> <!-- end article footer --> </article>
		<!-- end article -->

		<?php endwhile; ?>

		<?php else : ?>

		<article id="post-not-found"> <header>
		<h1>
		<?php _e("Not Found", "bonestheme"); ?>
		</h1>
		</header> <section class="post_content">
		<p>
		<?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?>
		</p>
		</section> <footer> </footer> </article>

		<?php endif; ?>

	</div>
	<!-- end #main -->

	<?php //get_sidebar(); // sidebar 1 ?>

</div>
<!-- end #content -->

<?php 
if($mysqli)
$mysqli->close();

if($mysqli_data)
$mysqli_data->close();
?>

	<?php get_footer(); ?>