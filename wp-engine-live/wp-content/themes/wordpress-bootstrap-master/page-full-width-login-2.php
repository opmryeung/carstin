<?php
/*
 Template Name: Full Width Page - Login - 2
 */
?>
<?php get_header(); ?>
<?php

include_once 'core-functions.php';

include_once 'Objects.php';
include_once 'config.php';

date_default_timezone_set('America/Chicago');

//$mysqli = new mysqli('205.178.146.105','carstindb','2sH8q7gUiJ2g','carstinwp');

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
require_once ( ABSPATH . 'wp-includes/class-phpass.php');

$wp_hasher = new PasswordHash(8, TRUE);

$data = readSQL($mysqli, "SELECT * FROM wp_wp_eMember_members_tbl WHERE user_name = '".$_POST['inputUsername']."'");

$staticData = readSQL($mysqli_data, "SELECT * FROM DEALERS WHERE `Ship To Number` = '".$_POST['inputUsername']."'");

$inputLog = date('Y-m-d H:i:s ').$_SERVER['REMOTE_ADDR'].'-'.$_POST['inputUsername'].'-'.$_POST['inputPassword'];

$check = $wp_hasher->CheckPassword($_POST['inputPassword'], $data[0]['password']);

if(empty($data))
$inputLog.= ' - Username NOT Found';
else $inputLog.= ' - Username Found';

if($check){
	
	$Contact = new Contact();
	
	$inputLog.= '- Password Match';
	
	if(empty($staticData)){
		
		$Contact->setAccount($data[0]['user_name']);
		$Contact->setName($data[0]['first_name'].' '.$data[0]['last_name']);
		
		
		$userName = $data[0]['user_name'];
		$fullName = $data[0]['first_name'].' '.$data[0]['last_name'];
		$compName = $data[0]['company_name'];
		$phone = $data[0]['phone'];
		$email = $data[0]['email'];
	}else{
		$userName = $staticData[0]['Ship To Number'];
		$fullName = $staticData[0]['Primary Customer Contact'];
		$compName = $staticData[0]['Name'];
		$phone = $staticData[0]['Phone Number'];
		$email = $staticData[0]['Primary email'];
	}
	
	
	
}else $inputLog.= ' - Password NOT Match';

if($_POST['inputUsername'] == 'guest' && $_POST['inputPassword'] == 'guest'){
	$check=1;
	$userName = 'Guest001';
	$fullName = 'John Smith';
	$compName = 'ACME';
	$phone = '972-123-4567';
	$email = 'guest@guest.com';
	
	$inputLog.= ' [GUEST LOGIN]';
}

if($_POST['submit']=='1')
file_put_contents(get_template_directory().'/LOGIN_LOG/'.date('Ymd').'.log',$inputLog."\n",FILE_APPEND);

?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span12 clearfix" role="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>
			role="article">
			
		<!-- end article header --> <section class="post_content">

		<?php if($check){?>
			<form action="/calculate" method="post" class="form-horizontal" role="form">
			<input type="hidden" name="inputName" value="<?php echo $fullName.' - '.$userName; ?>">
			<input type="hidden" name="inputAcc" value="<?php echo $userName; ?>">
			<input type="hidden" name="inputCompany" value="<?php echo $compName; ?>">
			<input type="hidden" name="inputPhone" value="<?php echo $phone; ?>">
			<input type="hidden" name="inputEmail" value="<?php echo $email; ?>">
			<!-- <h1><strong>Login Successful</strong></h1>  -->
			<p><button type="submit" class="btn btn-large btn-primary" name="fromLogin" value="1">Click Here</button> to create a new quote project.</p>
			</form>
			<?php 
			
			$previousProjects = getProjectList($mysqli_data, $userName);
			
			echo '<hr/><h3>Project Log</h3>';
			echo printProjectTable($previousProjects);
			
			?>
		<?php }else{ ?>
			<div>
			<form action="/login/" method="post" class="form-horizontal" role="form">
				<div class="control-group">
					<label class="control-label" for="inputUsername">Username</label>
					<div class="controls">
						<input type="text" id="inputUsername" name="inputUsername" required autofocus>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">Password</label>
					<div class="controls">
						<input type="password" id="inputPassword" name="inputPassword"  required>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<!-- <label class="checkbox"> <input type="checkbox"> Remember me </label>  -->
						<button type="submit" class="btn" name="submit" value="1">Sign in</button>
					</div>
				</div>
			</form>
			</div>
		<?php } ?>

		</section> <!-- end article section --> <footer>

		<p class="clearfix">
		<?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?>
		</p>

		</footer> <!-- end article footer --> </article>
		<!-- end article -->

		<?php endwhile; ?>

		<?php else : ?>

		<article id="post-not-found"> <header>
		<h1>
		<?php _e("Not Found", "bonestheme"); ?>
		</h1>
		</header> <section class="post_content">
		<p>
		<?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?>
		</p>
		</section> <footer> </footer> </article>

		<?php endif; ?>

	</div>
	<!-- end #main -->

	<?php //get_sidebar(); // sidebar 1 ?>

</div>
<!-- end #content -->

<?php 
if($mysqli)
$mysqli->close();

if($mysqli_data)
$mysqli_data->close();

get_footer();
?>