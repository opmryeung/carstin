<?php
/*
Template Name: Full Width Page - Order Form - Process Post
*/
?>
<?php

include_once 'core-functions.php';
$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
$next = $_GET['next'];
$from = $_GET['from'];
$order_session = $_GET['session'];
$insert = isset($_GET['insert'])?$_GET['insert']:NULL;
$data = $_POST;

unset($data['submit']);

if($from == '/dealer-info/'){
	
	$keys = array_keys($data);
	
	$sql = 'INSERT INTO `ORDER` (ORDER_GROUP,TYPE,'.implode(',', $keys).') VALUES (\''.$order_session.'\',\'countertop\',\''.implode('\',\'', $data).'\')';
	
	executeSQL($mysqli, $sql);
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session.'&QuoteID='.$data['QuoteID']);
	
}

if($from == '/customer-info/'){
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= $k.' = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	executeSQL($mysqli, $sql);
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/material-color/'){
	
	if($data['Slab'] == 1 && ( empty($data['SlabLocation']) || empty($data['SlabDate']) )){
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Slab selected but locaiton or date not set; please set a slab location and date&'.http_build_query($data));
		exit();
	}
	
	if($data['Thickness'] == 'Other' && $data['OtherThickness']=='' ){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Thickness set to Other but Other Thickness not set; please set Other Thickness&'.http_build_query($data));
		exit();
	}
	
	//If order_session doesn't exist, means it's new and means it's an insert
	$orderExists = readSQL($mysqli, 'SELECT COUNT(ORDER_GROUP) as count FROM `ORDER` WHERE ORDER_GROUP = \''.$order_session.'\'','count');
	$orderExists = $orderExists[0];
	
	$keys = array_keys($data);
	if($orderExists){
		$sql = 'UPDATE `ORDER` SET ';
		foreach ($data as $k=>$d){
			
			if($k == 'SlabDate' && empty($d))
			$sql.= $k.' = \'0000-00-00\',';
			else
			$sql.= $k.' = \''.$d.'\',';
			
		}
		$sql = substr_replace($sql, '', -1);
		$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	}else{
		$sql = 'INSERT INTO `ORDER` (ORDER_GROUP,TYPE,'.implode(',', $keys).') VALUES (\''.$order_session.'\',\'ex-countertop\',\''.implode('\',\'', $data).'\')';
	}
	
	executeSQL($mysqli, $sql);
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/edge-profile/'){
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= $k.' = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	executeSQL($mysqli, $sql);
	
	/*echo '<pre>';
	var_dump($next);
	var_dump($from);
	var_dump($order_session);
	var_dump($data);
	
	echo $sql."\n";
	
	echo '</pre>';*/
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/sink-info/'){
	
	/*if($data['Sink'] == 'Yes' && ( $data['SinkBaseCabinetWidth']=='' )){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Base cabinet width and/or sink faucet hole count cannot be empty&'.http_build_query($data));
		exit();
	}*/
	
	if($data['Sink'] == 'Yes' && $data['SinkMaterial']=='Other' && $data['OtherSinkMaterial']=='' ){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Sink Material set to Other but Other Sink Material is empty&'.http_build_query($data));
		exit();
	}
	
	if($data['Sink'] == 'Yes' && $data['SinkType']=='Other' && $data['OtherSinkType']=='' ){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Sink Type set to Other but Other Sink Type is empty&'.http_build_query($data));
		exit();
	}
	
	if($data['Sink'] == 'Yes' && $data['SinkMakeModel']=='Other' && $data['OtherSinkMakeModel']=='' ){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Sink Make and Model set to Other but Other Sink Make and Model is empty&'.http_build_query($data));
		exit();
	}
	
	/*if($data['Sink'] == 'Yes' && $data['SinkBaseCabinetWidth']=='' ){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Has sink but sink base cabinet width is empty&'.http_build_query($data));
		exit();
	}*/
	
	/*if($data['Sink'] == 'Yes' && $data['SinkFaucetHoles']=='' ){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Has sink but sink faucet hole is empty&'.http_build_query($data));
		exit();
	}*/
	
	if($data['Sink'] == 'No (Jump to next step)'){
		$sql = 'UPDATE `ORDER` SET `Sink` = \'No (Jump to next step)\'';
		$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
		executeSQL($mysqli, $sql);
		
		header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
		exit();
	}
	
	$keys = array_keys($data);
	
	if($insert!=1){
	
		$sql = 'UPDATE `ORDER` SET ';
		foreach ($data as $k=>$d){
			
			$sql.= $k.' = \''.$d.'\',';
			
		}
		$sql = substr_replace($sql, '', -1);
		$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
		
		executeSQL($mysqli, $sql);
		
	}else{
		
		$sql = 'INSERT INTO `ORDER` (ORDER_GROUP,TYPE,'.implode(',', $keys).') VALUES (\''.$order_session.'\',\'ex-sink\',\''.implode('\',\'', $data).'\')';
		
		executeSQL($mysqli, $sql);
		
	}
	
	if($data['AdditionalSink'] == '1'){
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?insert=1&session='.$order_session.'&message=Adding a new sink to order');
		exit();
	}
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/range-info/'){

	/*if($data['Range'] == 'Yes' && ( $data['RangeMake']=='' || $data['RangeModel']=='' )){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Range Make and Model cannot be empty when range exists&'.http_build_query($data));
		exit();
	}*/
	
	if(($data['RangeStyle'] == 'Cooktop' || $data['RangeStyle'] == 'Slide-In Range') && ( $data['RangeMake']=='' || $data['RangeModel']=='' ) && $data['Range'] == 'Yes'){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Range Make and Model cannot be empty when range style is Cootop or Slide-In Range&'.http_build_query($data));
		exit();
	}
	
	if($data['Range'] == 'No (Jump to next step)'){
		
		$sql = 'UPDATE `ORDER` SET `Range` = \'No (Jump to next step)\'';
		$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
		executeSQL($mysqli, $sql);
		
		header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
		
		exit();
	}
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= '`'.$k.'` = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	executeSQL($mysqli, $sql);
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

/*if($from == '/backsplash-info/'){

	if($data['Range'] == 'Yes' && ( $data['RangeMake']=='' || $data['RangeModel']=='' )){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Range Make and Model cannot be empty when range exists&'.http_build_query($data));
		exit();
	}
	
	if($data['Range'] == 'No (Jump to next step)'){
		
		$sql = 'UPDATE `ORDER` SET `Range` = \'No (Jump to next step)\'';
		$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
		executeSQL($mysqli, $sql);
		
		header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
		
		exit();
	}
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= '`'.$k.'` = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	executeSQL($mysqli, $sql);
	
	/*echo '<pre>';
	var_dump($next);
	var_dump($from);
	var_dump($order_session);
	var_dump($data);
	
	echo $sql."\n";
	
	echo '</pre>';
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}*/

if($from == '/backsplash-info/'){

	/*if($data['Backsplash'] == 'Standard Height (4in, 3cm)' && $data['BacksplashOutlets']==''){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Backsplash outlet count is empty!&'.http_build_query($data));
		exit();
	}*/
	
	if($data['Backsplash'] == 'Custom Height/Size' && ($data['CustomBacksplash']=='')){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Custom backsplash cannot be empty when Custom Height/Size is selected&'.http_build_query($data));
		exit();
	}
	
	if($data['Backsplash'] != 'No Backsplash' && $data['BacksplashOutlets']==''){
		//echo 'Thickness =  other; OtherThickness empty'."\n";
		header('Location: http://'.$_SERVER['SERVER_NAME'].$from.'?session='.$order_session.'&message=Backsplash outlet count is empty when backsplash is present&'.http_build_query($data));
		exit();
	}
	
	if($data['Backsplash'] == 'No Backsplash'){
		
		$sql = 'UPDATE `ORDER` SET `Backsplash` = \'No Backsplash\'';
		$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
		executeSQL($mysqli, $sql);
		
		header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
		
		exit();
	}
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= '`'.$k.'` = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	executeSQL($mysqli, $sql);
	
	/*echo '<pre>';
	var_dump($next);
	var_dump($from);
	var_dump($order_session);
	var_dump($data);
	
	echo $sql."\n";
	
	echo '</pre>';*/
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/options-info/'){
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= $k.' = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	executeSQL($mysqli, $sql);
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/additional-countertop/'){
	
	$keys = array_keys($data);
	
	$sql = 'UPDATE `ORDER` SET ';
	foreach ($data as $k=>$d){
		
		$sql.= $k.' = \''.$d.'\',';
		
	}
	$sql = substr_replace($sql, '', -1);
	$sql.= ' WHERE ORDER_GROUP = \''.$order_session.'\'';
	
	//echo $sql;
	
	executeSQL($mysqli, $sql);
	
	if($data['AdditionalCountertop'] == 'Yes'){
		
		//Make copy of current order_session and add -# to end to indicate this is an additional top
		
		$parts = explode('-', $order_session);
		
		$countOfItems = readSQL($mysqli,'SELECT COUNT(ORDER_GROUP) as count FROM `ORDER` WHERE ORDER_GROUP LIKE \''.$parts[0].'%\'','count');
		
		$countOfItems = $countOfItems[0];
		
		//$dealer_customerInfo = readSQL($mysqli, 'SELECT DealerName, DealerContact, DealerEmail, DealerPhone, DealerFax, DealerPO, CustomerFirstName');
		
		header('Location: http://'.$_SERVER['SERVER_NAME'].'/material-color/'.'?session='.$parts[0].'-'.$countOfItems.'&message=Adding additional countertop to order&add=1');
		exit();
	}
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session);
	
}

if($from == '/review-info/'){
	
	$keys = array_keys($data);
	
	$parts = explode('-', $order_session);
	
	if($data['Submit'] == 1){
		//Continue to complete page
		//Copy order group to confirmed_order
		
		$sql ='INSERT INTO `CONFIRMED_ORDER` (`ORDER_GROUP`) VALUES ('.$parts[0].')';
		executeSQL($mysqli, $sql);
		
		$quoteId = $_POST['QuoteID'];
		
		$send = readSQL($mysqli, 'SELECT ProjectName, AccountName, ContactEmail FROM ORDER_LOG WHERE id = \''.$quoteId.'\'');
		
		$send = $send[0];
		
		header('Location: http://'.$_SERVER['SERVER_NAME'].$next.'?session='.$order_session.'&'.http_build_query($send));
		
	}elseif($data['Submit'] == 0){
		
		$sql = 'DELETE FROM `ORDER` WHERE ORDER_GROUP LIKE \''.$order_session.'%\'';
		executeSQL($mysqli, $sql);
		
		$quoteId = $_POST['QuoteID'];
		
		$send = readSQL($mysqli, 'SELECT ProjectName, AccountName, ContactEmail FROM ORDER_LOG WHERE id = \''.$quoteId.'\'');
		
		$send = $send[0];
		
		$location = 'Location: http://'.$_SERVER['SERVER_NAME'].'/dealer-info/'.'?'.http_build_query($send);
		
		header($location);
		
		//Go to first page with the following information attached
	}
	
	exit();
	
}

//header('Location: http://'.$_SERVER['SERVER_NAME'].$next);

if($mysqli)
$mysqli->close();

?>