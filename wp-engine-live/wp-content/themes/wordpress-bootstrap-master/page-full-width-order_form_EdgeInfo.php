<?php
/*
Template Name: Full Width Page - Order Form - Edge Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
include_once 'config.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							echo '<form action="/process-post/?next=/sink-info/&from=/edge-profile/&session='.$order_session.'" method="post"  role="form">';
							echo '<field>';
							echo '<legend>Edge Profile</legend>';
							
							DisplayOrderForm($mysqli, 'EdgeInfo','EdgeProfile');
							
							DisplayOrderForm($mysqli, 'EdgeInfo','EdgeNotes');
							
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Sink Info</button>';
							echo '</field>';
							echo '</form>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>