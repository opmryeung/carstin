<?php
/*
Template Name: Full Width Page - Order Form - Option Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							echo '<form action="/process-post/?next=/additional-countertop/&from=/options-info/&session='.$order_session.'" method="post"  role="form">';
							echo '<field>';
							echo '<legend>Options Info</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'OptionInfo','optionCornerRadius');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'OptionInfo','optionInsideRadius');
							echo '</div>';
							echo '<p style="clear:both;"><em><strong>**Note - Minimum for Inside Radius\', for all edges, is 3-1/4" </strong></em></p>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'OptionInfo','optionGeneralNotes');
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Additional Countertop</button>';
							echo '</field>';
							echo '</form>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>