<?php
/*
Template Name: Full Width Page - Order Form - Export Excel
*/
?>
<?php

function getNameAndStuff(mysqli $mysqli, $id){
	
	$sql = 'SELECT CONCAT(CustomerLastName,\' - \',DealerName) AS value FROM `ORDER` WHERE id = \''.$id.'\'';
	
	//echo $sql;
	
	$data = readSQL($mysqli, $sql,'value');
	
	return $data[0];
	
}

function getAttachment(mysqli $mysqli, $quote_id){
	
	$sql = 'SELECT ProjectName FROM ORDER_LOG WHERE id = \''.$quote_id.'\'';
	
	$data = readSQL($mysqli, $sql,'ProjectName');
	
	$sql = 'SELECT FileName FROM ORDER_LOG WHERE ProjectName = \''.$data[0].'\'';
	
	$output = '';
	
	foreach(readSQL($mysqli, $sql) as $file){
		if(!empty($file['FileName']))
		$output.= ('http://dealers.carstinbrands.com/wp-content/themes/wordpress-bootstrap-master/UPLOADED_FILES/'.$file['FileName'].',');
		else 
		$output.=' ';
	}
	
	$output = substr_replace($output, '', -1);
	
	return $output;
	
}

function getNameFromNumber($num) {
	$numeric = $num % 26;
	$letter = chr(65 + $numeric);
	$num2 = intval($num / 26);
	if ($num2 > 0)
		return getNameFromNumber($num2 - 1) . $letter;
	else
		return $letter;
}

function getColumns(mysqli $mysqli){
	
	$sql = 'SELECT `name` FROM  `ORDER_FORM` GROUP BY  `name` ORDER BY  `order` ';
	$columns = readSQL($mysqli, $sql,'name');
	$count = count($columns);
	
	$column_with_name = array();
	for($i=0; $i<$count; $i++){
		array_push($column_with_name,array(getNameFromNumber($i),$columns[$i]));
	}
	
	return $column_with_name;
	
}

function getRowData(mysqli $mysqli, $id, array $columns = NULL){
	
	$sql = 'SELECT 1 as `COLUMN`, 1 as `ATTACHMENT`, `'.implode('`,`', $columns).'` FROM `ORDER` WHERE id = '.$id;
	
	$data = readSQL($mysqli, $sql);
	
	$type = readSQL($mysqli, 'SELECT `TYPE` FROM `ORDER` WHERE id = '.$id);
	
	if($type[0]['TYPE'] == 'ex-sink'){
		unset($data[0]['Range']);
		unset($data[0]['RangeStyle']);
		unset($data[0]['RangeMake']);
		unset($data[0]['RAngeModel']);
		unset($data[0]['RangeNotes']);
		unset($data[0]['Backsplash']);
		unset($data[0]['CustomBacksplash']);
		unset($data[0]['BacksplashOutlets']);
		unset($data[0]['BacksplashNotes']);
		unset($data[0]['OptionCornerRadius']);
		unset($data[0]['OptionInsideRadius']);
		unset($data[0]['OptionGeneralNotes']);
		unset($data[0]['AdditionalCountertop']);
	}
	
	return $data[0];
}

function createColumnData(mysqli $mysqli){
	
	$sql = 'SELECT `name` FROM  `ORDER_FORM` GROUP BY  `name` ORDER BY  `order` ';
	
	$data = array();
	
	if ($results = $mysqli->query($sql)) {
		if($results->num_rows!=0){
			while ($row = $results->fetch_assoc()){
				array_push($data, $row['name']);
			}
		}
		$results->close();
	}else{
		echo "SQL: ".$sql."\n";
		throw new exception( "Execute Error: (" . $mysqli->errno . ") " . $mysqli->error . "\n");	
	}
	
	return $data;
	
}

function buildDataArray(mysqli $mysqli, $startDate, $endDate){
	
	$dataArray = array();
	
	$sql = 'SELECT id,QuoteID, ORDER_GROUP,TYPE FROM `ORDER` WHERE `OrderDate` >= \''.$startDate.'\' AND `OrderDate` <= \''.$endDate.'\'';
	$countertops = readSQL($mysqli, $sql);
	
	$columnData = createColumnData($mysqli);
	
	$display_column_data = createColumnData($mysqli);
	array_unshift($display_column_data,'Attachment');
	array_unshift($display_column_data,'Column');
	
	$display_column_data = array_replace($display_column_data,array(3=>'CarstinSalesRep'));
	
	$confirmed_orders = readSQL($mysqli, 'SELECT `ORDER_GROUP` FROM `CONFIRMED_ORDER`','ORDER_GROUP');
	
	foreach($countertops as $countertop){
		
		$parts = explode('-', $countertop['ORDER_GROUP']);
		
		if(in_array($parts[0], $confirmed_orders)){
			
			$rowData = getRowData($mysqli, $countertop['id'],$columnData);
			
			if(!empty($countertop['QuoteID']))
			$quoteId = explode('-', $countertop['QuoteID']);
			else 
			$quoteId = array(0,0);
			
			array_shift($rowData);
			array_shift($rowData);
			array_unshift($rowData, getAttachment($mysqli,$quoteId[1]));
			array_unshift($rowData, getNameAndStuff($mysqli, $countertop['id']));
			
			
			array_push($dataArray, $display_column_data);
			array_push($dataArray, $rowData);
			
			$sql = 'SELECT id, ORDER_GROUP,TYPE FROM `ORDER` WHERE ORDER_GROUP LIKE \''.$countertop['ORDER_GROUP'].'%\' AND `TYPE` != \'countertop\'';
			$extra = readSQL($mysqli,$sql);
			
			foreach($extra as $ex){
				
				$rowData = getRowData($mysqli, $ex['id'],$columnData);
				array_shift($rowData);
				array_shift($rowData);
				array_unshift($rowData, NULL);
				array_unshift($rowData, NULL);
				
				array_push($dataArray, $rowData);
			}
			
		}
	}
	
	return $dataArray;
	
}

function getRowCount(mysqli $mysqli, $startDate, $endDate){
	$sql = 'SELECT ORDER_GROUP FROM `ORDER` WHERE `OrderDate` >= \''.$startDate.'\' AND `OrderDate` <= \''.$endDate.'\'';
	$countertops = readSQL($mysqli, $sql);
	
	$row = count($countertops);
	$row = $row *2;
	
	foreach($countertops as $top){
		
		$sql = 'SELECT ORDER_GROUP FROM `ORDER` WHERE ORDER_GROUP LIKE \''.$top['ORDER_GROUP'].'%\' AND `TYPE` != \'countertop\'';
		$row+= count(readSQL($mysqli, $sql));
		
	}
	
	return $row;
}

function findHeaderColumns(mysqli $mysqli, $startDate, $endDate){
	
	$row = array();
	$rowCount = 1;
	
	$sql = 'SELECT id, ORDER_GROUP,TYPE FROM `ORDER` WHERE `OrderDate` >= \''.$startDate.'\' AND `OrderDate` <= \''.$endDate.'\'';
	$countertops = readSQL($mysqli, $sql);
	
	$confirmed_orders = readSQL($mysqli, 'SELECT `ORDER_GROUP` FROM `CONFIRMED_ORDER`','ORDER_GROUP');
	
	foreach($countertops as $countertop){
		
		$parts = explode('-', $countertop['ORDER_GROUP']);
		
		if(in_array($parts[0], $confirmed_orders)){
		
			array_push($row, $rowCount);
			$rowCount++;
			$rowCount++;
			$sql = 'SELECT id, ORDER_GROUP,TYPE FROM `ORDER` WHERE ORDER_GROUP LIKE \''.$countertop['ORDER_GROUP'].'%\' AND `TYPE` != \'countertop\'';
			$extra = readSQL($mysqli,$sql);
			foreach($extra as $ex){
				$rowCount++;
			}
		
		}
		
	}
	
	return $row;
	
}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Chicago');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/PHPExcel.php';
include_once 'core-functions.php';
$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
$baseStyle = new PHPExcel_Style();
$baseStyle->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
												 'color' => array('argb' => 'C0D9D9FF')),
								 'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM))));

$objPHPExcel = new PHPExcel();
$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

$data = buildDataArray($mysqli,$_POST['startDate'],$_POST['endDate']);

$rows = findHeaderColumns($mysqli, $_POST['startDate'],$_POST['endDate']);

$end = getNameFromNumber(count(getColumns($mysqli)) +1 );

$objPHPExcel->getActiveSheet()->fromArray($data,NULL,'A1');

$objPHPExcel->getActiveSheet()->setTitle('Sheet');

$objPHPExcel->setActiveSheetIndex(0);

foreach ($rows as $row){
	
	$objPHPExcel->getActiveSheet()->setSharedStyle($baseStyle, 'A'.$row.':'.$end.$row);
	
}



// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.date('Y-m-d').' Orders.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');



if($mysqli)
$mysqli->close();

exit;

?>