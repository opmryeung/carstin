<?php
/*
 Template Name: Full Width Page - Order Form - Color Selector From Material
 */
?>
<?php

//page: http://dealers.carstinbrands.com/material-color-selector/
//info: http://stackoverflow.com/questions/20857764/php-mysql-dynamic-select-box

include_once 'core-functions.php';

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');

if(isset($_GET['option']))
{
	$option = $_GET['option'];
	
	$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
	$data = readSQL($mysqli, 'SELECT value FROM ORDER_FORM_Color WHERE type = \''.$option.'\' ORDER BY value ASC','value');

	$reply = array('data' => $data, 'error' => false);
}
else
{
	$reply = array('error' => true);
}

$json = json_encode($reply);
echo $json;
?>