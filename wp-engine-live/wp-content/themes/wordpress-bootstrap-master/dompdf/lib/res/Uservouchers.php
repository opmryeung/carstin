<?php

class User_Uservouchers extends Zend_Db_Table{

	/**
     * The table name.
     *
     * @var string
     */
    protected $_name = 'ek_vouchers';

    protected $hrcalender;
	
    const TYPE_INT = 'Integer';
    const TYPE_MIX = 'Mix';
	const TYPE_STRING = 'typeString';
	const TYPE_FLOAT = 'typeFloat';
	const TYPE_BOOLEAN = 'typeBoolean';
	
    protected $fields = array(
    	'ek_voucherid' => self::TYPE_INT,
    	'ek_voucher' => self::TYPE_INT,
    	'voucher_userid' => self::TYPE_INT,
    	'voucher_hrid' => self::TYPE_INT,
		'voucher_month' => self::TYPE_INT,
		'voucher_year' => self::TYPE_INT
    );

    protected $_values = array();

    protected $_children = array();

    public function __construct($users=null) {
    	parent::__construct();
    }
	public function retreivevouchers($userid,$presentnum,$presentyear)
	{
	
	$select = $this->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
		$select->setIntegrityCheck(false)		
					->where('voucher_userid = ?', $userid)
					->where('voucher_month = ?', $presentnum)
					->where('voucher_year = ?', $presentyear);
		$row = $this->fetchRow($select);	

	
	
	
	
		/*$row = $this->fetchRow(
					$this->select()
					->where('voucher_userid = ?', $userid)
					->where('voucher_month = ?', $presentnum)
					->where('voucher_year = ?', $presentyear)
					
				);*/
		$rowCount = count($row);
		
		if ($rowCount > 0) 
		{
			return $row->toArray();
		} 
		else 
		{
			return false;
		}	
	}
	public function selectvouchers($post)
	{
			//$userid=$post$postuserid);
		$count=$post['countvalues'];
		for($i=0;$i<$count;$i++)
		{
			$this->insertvouchers($post,$i);
		}
	}
	public function insertvouchers($post,$i)
	{
		$userid="userid".$i;
		$vouchercount="totcount".$i;
		
		
		$userpicturecard="userpicturecard".$i;
		$picturecardcost="picturecardcost".$i;
		$standardcardcost="standardcardcost".$i;
		$montlyfee="montlyfee".$i;
		$picturestatus="picturestatus".$i;
		$ek_user_vouchervalue="ek_user_vouchervalue".$i;
		
		$userpicturecard=$post[$userpicturecard];
		$picturecardcost=$post[$picturecardcost];
		$standardcardcost=$post[$standardcardcost];
		$montlyfee=$post[$montlyfee];
		$picturestatus=$post[$picturestatus];
		$ek_user_vouchervalue=$post[$ek_user_vouchervalue];
		
		
		
		$dat['userpicturecard'] =  $userpicturecard;
		$dat['picturecardcost'] =  $picturecardcost;
		$dat['standardcardcost'] =  $standardcardcost;
		$dat['montlyfee'] =  $montlyfee;
		$dat['picturestatus'] =  $picturestatus;
		$dat['ek_user_vouchervalue'] =  $ek_user_vouchervalue;
		
		
		
		$hrid=$post['hruserid'];
		$month=$post['month'];
		$companyid=$post['companyid'];
		$year=$post['year'];
		$voucheruser= $post[$userid];
		$totvouchercount=$post[$vouchercount];
		$dat['ek_voucher'] =  $totvouchercount;
		$dat['voucher_userid'] =  $voucheruser;
		$dat['voucher_hrid'] =  $hrid;
		$dat['voucher_month'] =  $month;
		$dat['voucher_year'] =  $year;
		$dat['voucher_cmpid'] =  $companyid;
		
		
		if($totvouchercount!='0' && $totvouchercount!="")
		{
			$countval=$this->retreivevouchers($voucheruser,$month,$year,$hrid);
			if(count($countval)>1)
			{
				$where=array();
				$where[] = $this->getAdapter()->quoteInto('voucher_userid = ?', $voucheruser);
				$where[] = $this->getAdapter()->quoteInto('voucher_month = ?', $month);
				$where[] = $this->getAdapter()->quoteInto('voucher_year = ?', $year);
			//	$where[] = $this->getAdapter()->quoteInto('voucher_hrid = ?', $hrid);
				$row = $this->update($dat,$where);
			}else{
				$row = $this->insert($dat);
			}
		}
	}
	public function selectvouchersbyhr($userid,$presentnum,$presentyear,$companyid)
	{
		
/*		$select = $this->select(Zend_Db_Table::SELECT_WITH_FROM_PART);
			$select->setIntegrityCheck(false)		
					->where('voucher_hrid 	 = ?', $userid)
					->where('voucher_month = ?', $presentnum)
					->where('voucher_year = ?', $presentyear);
					//echo $select;
			$row = $this->fetchAll($select);	
*/		
if(strlen($presentnum)>=2)
{
	$month=$presentnum;
}else{
$month="0".$presentnum;
}
		$select=$this->select()
             ->from(array('p' => 'ek_vouchers'),
                    array('sum(ek_voucher) as countvoucher'))
			//->where('voucher_hrid 	 = ?', $userid)
			->where('voucher_cmpid 	 = ?', $companyid)
			->where('voucher_month = ?', $month)
			->where('voucher_year = ?', $presentyear);
		
		$row = $this->fetchAll($select);
		
		$rowCount = count($row);
		if ($rowCount > 0) 
		{
			return $row->toArray();
		} 
		else 
		{
			return false;
		}	
	}
	
	
	public function selectvoucherscount($userid,$presentnum,$presentyear,$companyid)
	{
		if(strlen($presentnum)>=2)
{
	$month=$presentnum;
}else{
$month="0".$presentnum;
}
			$select=$this->select()
				//->where('voucher_hrid 	 = ?', $userid)
				->where('voucher_cmpid 	 = ?', $companyid)
				->where('voucher_month = ?', $month)
				->where('voucher_year = ?', $presentyear);
				//exit;
			$row = $this->fetchAll($select);
			
			$rowCount = count($row);
			if ($rowCount > 0) 
			{
				return $row->toArray();
			} 
			else 
			{
				return false;
			}	
	}
	
	public function retrieveusers($totvouchers)
	{
		$users = new User_Users();
		//print_r($totvouchers);
			for($i=0;$i<count($totvouchers);$i++)
			{
				$vouchers=$totvouchers[$i]['ek_voucher'];
				$userid=$totvouchers[$i]['voucher_userid'];
				//$UserRecord=$users->userlistbyid($userid,'','');
				//if($UserRecord['ek_voucher']!="")
				//{
					//$voucherate[]=$UserRecord['ek_voucher'];
					//$valuesrecord=str_replace('.','',$UserRecord['ek_voucher']);
					$valuesrecord=str_replace('.','',$totvouchers[$i]['ek_user_vouchervalue']);
					$valuesrecordmul=str_replace(',','.',$valuesrecord);
					//$vouchervalue[]=$vouchers*$UserRecord['ek_voucher'];
					$vouchervalue[]=$vouchers*$valuesrecordmul;
				//}
				
			}
			
		if(count($vouchervalue)>0)
			{
				$totvoucheamt=array_sum($vouchervalue);
			 }
		 return $totvoucheamt;
		//print_r($voucherate);
	}
	
	//public function displaymonthconfirm($presentmonthname,$presentyear,$totusers,$totvoucher,$totvoucheramt,$lang,$validate="",$companyid)
	public function displaymonthconfirm($presentmonthname,$presentyear,$totusers,$totvoucher,$totvoucheramt,$lang,$validate="",$companyid,$monthdata_res = "") {
	$t = Zend_Registry::get('Zend_Translate');
		if($totvoucher!="")
		{
			$totvoucher=$totvoucher;
		}else{
			$totvoucher=0;
		}
		$html .='<div id="monthvalidate"><div class="Commondiv2">
		   		<div class="hrmonth-transaction">
					<div class="hrmonth-transaction-head">
						<div class="hrmonth-head-left float-left">'.$presentmonthname .'&nbsp;'.$presentyear.'</div>
						<div class="hrmonth-head-right float-right">&nbsp;</div>
					</div>
					<div class="hrmonth-row">
						<div class="hrmonth-row-left float-left">Aantal werknemers</div>
						<div class="hrmonth-row-right float-right">'.$totusers.'</div>
					</div>
					<div class="hrmonth-row">
						<div class="hrmonth-row-left float-left">Aantal maaltijdcheques</div>
						<div class="hrmonth-row-right float-right">'. $totvoucher.'</div>
					</div>
					<div class="hrmonth-row">
						<div class="hrmonth-row-left float-left">Totale waarde van deze maand</div>
						<div class="hrmonth-row-right float-right">'.number_format($totvoucheramt,2).' &euro;<input type="hidden" name="urlpostcode" id="urlpostcode" value="/'.$lang.'/calender/monthconfirm"/></div>
					</div>
				</div>
		   </div>';
		  if($validate!=1)
		  { 
		$html .='<div class="Commondiv">
		   		<a href="calender_month_management" class="maand">Maand aanpassen</a>
		   </div>
			
		  


			<div class="Commondiv2">
              <div class="Threeblocktop2">
                <div class="Transactcontentdiv ">
					<div class="hrcalender">

						<p>'.$t->_('monthconfirmcontent').'</p>
						
					</div>
					<div class="Commondiv">
						<div class="hrmonth-password">
							<div class="hrmonth-label float-left">
								Paswoord :
							</div>
							<div class="hrmonth-field float-left">
								<input type="password" name="password" value="" id="password" class="regtextbox2n" />
								<input type="hidden" name="invoicevalue" value="'.$monthdata['invoicevalue'].'" id="invoicevalue" />
								<input type="hidden" name="servicevalue" value="'.$monthdata['servicevalue'].'" id="servicevalue" />
								<input type="hidden" name="invoicenumber" value="'.$monthdata['invoicenumber'].'" id="invoicenumber" />
								<input type="hidden" name="inumber" value="'.$monthdata['inumber'].'" id="inumber" />
								<input type="hidden" name="modval" value="'.$monthdata['modval'].'" id="modval" />
								<input type="hidden" name="totvoucheramt" value="'.$totvoucheramt.'" id="totvoucheramt" />
							</div>
						</div>
						<div class="hrmonth-password">
							<div class="hrmonth-label float-left">
								&nbsp;
							</div>
							<div class="hrmonth-field float-left">
								<input type="submit" class="maandbutton" value="submit" />
							</div>
						</div>
					</div>
					
                </div>
              </div>
          </div>';  
		  } 
		  $html .='</div>';
		return $html;
	}
	
	//public function displaymonthnotconfirm($presentmonthname,$presentyear,$totusers,$totvoucher,$totvoucheramt,$lang,$validate="",$companyid) {
	public function displaymonthnotconfirm($presentmonthname,$presentyear,$totusers,$totvoucher,$totvoucheramt,$lang,$validate="",$companyid,$monthdata = "") {
	$t = Zend_Registry::get('Zend_Translate');
		if($totvoucher!="")
		{
			$totvoucher=$totvoucher;
		}else{
			$totvoucher=0;
		}
		$html .='<div id="monthvalidate"><div class="Commondiv2">
		   		<div class="hrmonth-transaction">
					<div class="hrmonth-transaction-head">
						<div class="hrmonth-head-left float-left">'.$presentmonthname .'&nbsp;'.$presentyear.'</div>
						<div class="hrmonth-head-right float-right">&nbsp;</div>
					</div>
					<div class="hrmonth-row">
						<div class="hrmonth-row-left float-left">Aantal werknemers</div>
						<div class="hrmonth-row-right float-right">'.$totusers.'</div>
					</div>
					<div class="hrmonth-row">
						<div class="hrmonth-row-left float-left">Aantal maaltijdcheques</div>
						<div class="hrmonth-row-right float-right">'. $totvoucher.'</div>
					</div>
					<div class="hrmonth-row">
						<div class="hrmonth-row-left float-left">Totale waarde van deze maand</div>
						<div class="hrmonth-row-right float-right">'.number_format($totvoucheramt,2).' &euro;<input type="hidden" name="urlpostcode" id="urlpostcode" value="/'.$lang.'/calender/monthconfirm"/></div>
					</div>
				</div>
		   </div>';
		    if($validate!=1)
		  { 
		$html .='<div class="Commondiv">
		   		<a href="calender_month_management" class="maand">Maand aanpassen</a>
		   </div>
			
		  


			<div class="Commondiv2">
              <div class="Threeblocktop2">
                <div class="Transactcontentdiv ">
					<div class="hrcalender">

						<p>'.$t->_('monthconfirmcontent').'</p>
						
					</div>
					<div class="Commondiv">
						<div class="hrmonth-password">
							<div class="hrmonth-label float-left">
								Paswoord :
							</div>
							<div class="hrmonth-field float-left">
								<input type="password" name="password" value="" id="password" class="regtextbox2n" />
								<input type="hidden" name="invoicevalue" value="'.$monthdata['invoicevalue'].'" id="invoicevalue" />
								<input type="hidden" name="servicevalue" value="'.$monthdata['servicevalue'].'" id="servicevalue" />
								<input type="hidden" name="invoicenumber" value="'.$monthdata['invoicenumber'].'" id="invoicenumber" />
								<input type="hidden" name="inumber" value="'.$monthdata['inumber'].'" id="inumber" />
								<input type="hidden" name="modval" value="'.$monthdata['modval'].'" id="modval" />
								<input type="hidden" name="totvoucheramt" value="'.$totvoucheramt.'" id="totvoucheramt" />
							</div>
						</div>
						<div class="hrmonth-password">
							<div class="hrmonth-label float-left">
								&nbsp;
							</div>
							<div class="hrmonth-field float-left">
								<input type="submit" class="maandbutton" value="submit" />
							</div>
						</div>
					</div>
					
                </div>
              </div>
          </div>';  
		  } 
		return $html;
	}
	public function updatevalidate($month,$year,$companyid,$date)
	{
		$dat['voucher_validate']=1;
		$dat['validatedate']=$date;
		$where = array();
		$where[] = $this->getAdapter()->quoteInto('voucher_cmpid = ?', $companyid);
		$where[] = $this->getAdapter()->quoteInto('voucher_month = ?', $month);
		$where[] = $this->getAdapter()->quoteInto('voucher_year = ?', $year);
		$this->update($dat, $where);
	}
	
public function validatevouchermonth($month,$year)
	{
		
		$db = Pimcore_Resource_Mysql::get();
		//$query = "SELECT EUE.ek_voucher as voucherprice,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' GROUP BY EUE.ek_voucher ";
		 $query = "SELECT validatedate FROM ek_vouchers  WHERE  voucher_month='" . $month . "' AND voucher_year='" . $year . "'  GROUP BY voucher_month";
		$data = $db->fetchAll($query);
		return $data;
		//print_r($data);
		//exit;
		
		
	}
	
	public function voucherlist($month,$year,$companyid)
	{
		
		$db = Pimcore_Resource_Mysql::get();
		//$query = "SELECT EUE.ek_voucher as voucherprice,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' GROUP BY EUE.ek_voucher ";
		$query = "SELECT EV.ek_user_vouchervalue as voucherprice,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' GROUP BY EV.ek_user_vouchervalue ";
		$data = $db->fetchAll($query);
		return $data;
		//print_r($data);
		//exit;
		
		
	}
	
	/* Mani Kanta R.K. 
		Function to know the Standard Card Users for the select month in the Invoice
	*/
	
	public function standardcardlist($month,$year,$companyid)
	{
		//echo "1";
		//exit;
		$db = Pimcore_Resource_Mysql::get();
		//$query = "SELECT count(*) as standardusers,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' AND EUE.ek_picture_status ='0'";
		
		$query = "SELECT count(*) as standardusers,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' AND picturestatus ='0'";
		$data = $db->fetchAll($query);
		return $data;	
	}
	
	/* Mani Kanta R.K. 
		Function to know the Picture Card Users for the select month in the Invoice
	*/
	
	public function picturecardlist($month,$year,$companyid)
	{
		//echo "1";
		//exit;
		$db = Pimcore_Resource_Mysql::get();
	//	$query = "SELECT count(*) as picturecardusers,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' AND EUE.ek_picture_status ='1'";
		$query = "SELECT count(*) as picturecardusers,sum(EV.ek_voucher) as vouchercount FROM ekena_user_employee as EUE, ek_vouchers as EV WHERE  EV.voucher_userid=EUE.ek_user_id  AND voucher_month='" . $month . "' AND voucher_year='" . $year . "' AND voucher_cmpid='" . $companyid . "' AND picturestatus ='1'";
		
		//echo $query;
		//exit;
		$data = $db->fetchAll($query);
		return $data;	
	}	
	
public function Hrhtmlinvoice($voucherlist,$month,$year,$companyid,$hrid,$companyinfo,$inumber,$invoicenumber,$monthdate)
	{
	$splitarraydate=explode("-",$monthdate);
	
		$monthdate=$splitarraydate[1];
			 $confirmmonthname=date( 'F', mktime(0, 0, 0, $monthdate) );
			$presentmonthname=date( 'F', mktime(0, 0, 0, $month) );
			//print_r($voucherlist);
			$str='<table width="660" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
				<tr>
					<td>
						<table width="90%" cellpadding="1" cellspacing="0" border="0">
							<tr>
							
								<Td width="50%" align="left" valign="top">
								<font face="Arial, Helvetica, sans-serif" size="+2">FACTUUR nr 120'.$invoicenumber.'</font>
								</Td>
								<td  width="20%" align="right" valign="top">
								<font face="Arial, Helvetica, sans-serif" size="+1">Afzender:</font>
								</td>
								<td  width="30%" align="left" valign="top">
									<table width="100%" cellpadding="1" cellspacing="0" border="0" style="border:#000000 2px solid;">
										<tr>
											<td align="left">
											<font face="Arial, Helvetica, sans-serif" size="-1">
											E-ve (dept. Of HighCo-Scan ID) -<br />Kruiskouter 1 - 1730 Asse - Belgi�<br />BTW - BE 0475.109.067
											</font>
											</td>
										</tr>
									 </table>
											
								</td>
							 </tr>
							 <tr>
								<Td  align="left" valign="top">
								<font face="Arial, Helvetica, sans-serif" size="+1">*** KOPIE ***</font><br />
								<font face="Arial, Helvetica, sans-serif" size="-1">Deze factuur werd overgemaakt <br />aan de financi�le instelling die <br />de betaling uitvoert per domici�ring </font>
								</Td>
								<td   align="right" valign="top">
								<font face="Arial, Helvetica, sans-serif" size="+1">Begunstigde:</font>
								</td>
								<td  align="left" valign="top">
									<table width="100%" cellpadding="1" cellspacing="0" border="0" style="border:#000000 2px solid;">
										<tr>
											<td align="left">
											<font face="Arial, Helvetica, sans-serif" size="+1">' . $companyinfo["contact_person_name"] .'</font><br />
											<font face="Arial, Helvetica, sans-serif" size="-1">T.a.v. '. $hrid . '<br />' . $companyinfo["streetname"] .' '. $companyinfo["streetnumber"] .' '. $companyinfo["street_box"] . ' <br />B - ' . $companyinfo["postcode"] .' ' . $companyinfo["city"] .'<br />BTW-nr. BE '.$companyinfo["vatnumber"].'
											</font>
											</td>
										</tr>
									 </table>
											
								</td>
							 </tr>
							 <Tr>
								<td colspan="3">
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:#000000 2px solid;">
										 <tr>
											<td width="50%" align="left" valign="top">
											 <font face="Arial, Helvetica, sans-serif" size="+1">TE VERMELDEN OP DE BETALING:</font><br /><br />
											 <font face="Arial, Helvetica, sans-serif" size="-1">Mededeling&nbsp;&nbsp;&nbsp;+++ ' . $inumber.' +++</font>
											</td>
											<Td width="50%" valign="top">
												<table width="100%" cellpadding="0" cellspacing="0" border="0" >
													<Tr>
														<td width="20%" align="left"><font face="Arial, Helvetica, sans-serif" size="+1">Datum</font></td>
														<td width="80%" align="center"><font face="Arial, Helvetica, sans-serif" size="+1">'. $splitarraydate[2]." ".$confirmmonthname." ".$splitarraydate[0] . '</font></td>
													</Tr>
													<Tr>
														<td width="20%"  align="left"><font face="Arial, Helvetica, sans-serif" size="+1">Uw Ref.</font></td>
														<td width="80%" align="center"><font face="Arial, Helvetica, sans-serif" size="+1">'. $companyinfo["name"] .'</font></td>
													</Tr>
													<Tr>
														<td width="20%" align="left"><font face="Arial, Helvetica, sans-serif" size="+1">Onze Ref.</font></td>
														<td width="80%" align="center"><font face="Arial, Helvetica, sans-serif" size="-1">' . $companyinfo["name"] .' - +++ ' . $inumber.' +++</font></td>
													</Tr>
												</table>
											</Td>
										 </tr>
										 <tr>
											<td >
												<table width="100%" cellpadding="1" cellspacing="0" border="0" >
													<tr>
														<td><font face="Arial, Helvetica, sans-serif" size="+1">Job:</font></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td><font face="Arial, Helvetica, sans-serif" size="+1">Servicekost maaltijdcheques</font></td>
														<td>&nbsp;</td>
													</tr>
												</table>
											</td>
											<td ><font face="Arial, Helvetica, sans-serif" size="+1">
											' . $presentmonthname . ' ' . $year . '</font>
											</td>
										</tr>
										<Tr><td colspan="2" height="2"></td></Tr>
										<tr>
											<Td colspan="2">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" >
													<Tr>
														<td width="55%" align="left" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+2">Omschrijving</font></td>
														<td width="10%" align="right" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+2">Aantal</font></td>
														<td width="15%" align="right" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+2">Eenh. Prijs</font></td>
														<td width="15%" align="right" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+2">Bedrag</font></td>
														<td width="5%" align="center" style="border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+2">�</font></td>
													</Tr>
													<Tr>
														<td  colspan="5" align="left" style=" border-bottom:#000000 1px solid;" height="5"></td>
													   
													</Tr>
													<Tr>
														<td   align="left" height="10" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="center" ></td>
													</Tr>
													';
													$grandtotal=0;
													foreach($voucherlist as $vlist)
													{
																$vprice = str_replace(",",".",$vlist["voucherprice"]);
																
																$total= $vprice * $vlist["vouchercount"];
																$grandtotal+=$total;
																$str.='    <Tr>
																<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="+1">Cheques waarde � '. $vlist["voucherprice"] . '</font></td>
																<td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="+1">'. $vlist["vouchercount"] .'</font></td>
																<td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="+1">'. number_format($vprice,3) .'</font></td>
																<td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="+1">' . number_format($total,2) .' �</font></td>
																<td  align="center" ></td>
															</Tr>
															<Tr>
																<td   align="left" height="20" style="border-right:#000000 1px dotted; "></td>
																<td  align="right" style="border-right:#000000 1px dotted; "></td>
																<td  align="right" style="border-right:#000000 1px dotted; "></td>
																<td  align="right" style="border-right:#000000 1px dotted; "></td>
																<td  align="center" ></td>
															</Tr>';
															
															}
													
													
													
													$str.='
													<Tr>
														<td   align="left" height="10" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="center" ></td>
													</Tr>
													<Tr>
														<td   align="left" height="10" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="right" style="border-right:#000000 1px dotted; "></td>
														<td  align="center" ></td>
													</Tr>
													
													
												</table>
											</Td>
									   </tr>
									  
									 </table>
								  </td>
							   </Tr>
								<tr>
											<td colspan="3">
												<table width="100%" cellpadding="1" cellspacing="0" border="0" >
													<Tr>
														
														<td  align="right" width="80%" ><font face="Arial, Helvetica, sans-serif" size="-1">Netto bedrag </font></td>
														<td  align="right" width="15%" ><font face="Arial, Helvetica, sans-serif" size="-1">'. number_format($grandtotal,2) . '</font></td>
														<td  align="left" width="5%"  ><font face="Arial, Helvetica, sans-serif" size="-1">�</font></td>
													</Tr>
													<Tr>
														
														<td  align="right"  > </td>
														<td  align="right"  > </td>
														<td  align="left"  > </td>
													</Tr>
													<Tr>
														
														<td  align="right" width="80%"  ><font face="Arial, Helvetica, sans-serif" size="-1">Te betalen  </font></td>
														<td  align="right" width="15%" ><font face="Arial, Helvetica, sans-serif" size="+1">'. number_format($grandtotal,2)  . '</font> </td>
														<td  align="left" width="5%" ><font face="Arial, Helvetica, sans-serif" size="+1">�</font> </td>
													</Tr>
												</table>
											</td>
									   </tr>
							   <tr>
								<td colspan="2" align="left"><font face="Arial, Helvetica, sans-serif" size="-2">Over te schrijven op een van onderstaande rekeningen met de gestructureerde medeling</font></td>
								<td  align="left"><font face="Arial, Helvetica, sans-serif" size="-2">+++ ' . $inumber .'+++</font></td>
							   </tr>
							   
							   <tr>
								<td colspan="3">
										<table width="90%" cellpadding="0" cellspacing="0" border="0" style="border:#000000 2px solid;">
										<tr>
											<td align="left" width="30%" valign="top" style="border-right:#000000 2px solid;">
											<font face="Arial, Helvetica, sans-serif" size="+1">
											Tunz.com inzake E-Kena/E-ve<br />IBAN : BE76732014253795<br />BIC : CREGBEBB
											</font>
											</td>
										</tr>
									 </table>
								</td>
							   </tr>
							   <tr>
								<td colspan="3" align="center"><font face="Arial, Helvetica, sans-serif" size="+1">Wij danken u voor het vertrouwen !</font></td>
							   </tr>
						  </table>
						
					</td>
				</tr>
			</table>';
		return $str;	
	}	
	
	public function ekenainvoice($companyinfo,$standardcardinfo,$picturecardinfo,$hrid,$month,$year,$inumber,$invoicenumber,$monthdate)
	{
	$splitarraydate=explode("-",$monthdate);
	
		$monthdate=$splitarraydate[1];
			 $confirmmonthname=date( 'F', mktime(0, 0, 0, $monthdate) );
		$presentmonthname=date( 'F', mktime(0, 0, 0, $month) );
		
				$onemonthafter= $splitarraydate[0]."-".$splitarraydate[1]."-".$splitarraydate[2];
		
		$dateOneMonthAdded = strtotime(date($onemonthafter, strtotime($onemonthafter)) . "+1 month");

		$exactday = date('d  F Y', $dateOneMonthAdded);

		
		
		$picusers = $picturecardinfo[0]["picturecardusers"];
		$piccost = $companyinfo["cardcost"];
		$standardusers = $standardcardinfo[0]["standardusers"];
		$standardcost = $companyinfo["standardcardcost"];
		
		$picusers = str_replace(",",".",$picusers);
		$piccost = str_replace(",",".",$piccost);
		$standardusers = str_replace(",",".",$standardusers);
		$standardcost = str_replace(",",".",$standardcost);
		
		$pictotal = $picusers * $piccost;
		$standardtotal = $standardusers * $standardcost;
		
		$total = $pictotal + $standardtotal;
		$userstotal = $picusers + $standardusers;
		
		$totalvouchers = $standardcardinfo[0]["vouchercount"] + $picturecardinfo[0]["vouchercount"];
		//$monthlyfee = $companyinfo["montlyfee"];
		$monthlyfee = str_replace(",",'.',$companyinfo["montlyfee"]);
		$totalvouchersamt = $totalvouchers * $monthlyfee;
		
		$totalamount = $total + $totalvouchersamt;
		$vat = ($totalamount * 21) / 100;
		$grandtotal = $totalamount + $vat;
		/*$picturecardinfo[0]["picturecardusers"]*$companyinfo["cardcost"]
		$picturecardinfo[0]["standardusers"]*$companyinfo["standardcardcost"]*/
		
		$str='<table width="660" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
	<tr>
    	<td>
        	<table width="90%" cellpadding="1" cellspacing="0" border="0">
            	<tr>
                	<Td width="50%" align="left" valign="top">
                    <font face="Arial, Helvetica, sans-serif" size="+2">FACTUUR nr 121'.$invoicenumber.'</font>
                    </Td>
                    <td  width="20%" align="right" valign="top">
                    <font face="Arial, Helvetica, sans-serif" size="+1">Afzender:</font>
                    </td>
                    <td  width="30%" align="left" valign="top">
                    	<table width="100%" cellpadding="1" cellspacing="0" border="0" style="border:#000000 2px solid;">
                        	<tr>
                            	<td align="left">
                                <font face="Arial, Helvetica, sans-serif" size="-1">
                                E-ve (dept. Of HighCo-Scan ID) -<br />Kruiskouter 1 - 1730 Asse - Belgi�<br />BTW - BE 0475.109.067
                                </font>
                                </td>
                            </tr>
                         </table>
                                
                    </td>
                 </tr>
                 <tr>
                	<Td  align="left" valign="top">
                    <font face="Arial, Helvetica, sans-serif" size="+1">*** KOPIE ***</font><br />
                    <font face="Arial, Helvetica, sans-serif" size="-1">Deze factuur werd overgemaakt <br />aan de financi�le instelling die <br />de betaling uitvoert per domici�ring </font>
                    </Td>
                    <td   align="right" valign="top">
                    <font face="Arial, Helvetica, sans-serif" size="+1">Begunstigde:</font>
                    </td>
                    <td  align="left" valign="top">
                    	<table width="100%" cellpadding="1" cellspacing="0" border="0" style="border:#000000 2px solid;">
                        	<tr>
                            	<td align="left">
                                <font face="Arial, Helvetica, sans-serif" size="+1">' . $companyinfo["contact_person_name"].'</font><br />
                                <font face="Arial, Helvetica, sans-serif" size="-1">T.a.v. '. $hrid .'<br />' . $companyinfo["streetname"].' ' . $companyinfo["streetnumber"] .' ' . $companyinfo["street_box"].'<br />B - ' .  $companyinfo["potcode"]. ' ' . $companyinfo["city"]. '<br />BTW-nr. BE '.$companyinfo["vatnumber"].'
                                </font>
                                </td>
                            </tr>
                         </table>
                                
                    </td>
                 </tr>
                 <Tr>
                 	<td colspan="3">
                    	<table width="100%" cellpadding="2" cellspacing="0" border="0" style="border:#000000 2px solid;">
                             <tr>
                                <td width="50%" align="left" valign="top">
                                 <font face="Arial, Helvetica, sans-serif" size="+1">TE VERMELDEN OP DE BETALING:</font><br /><br />
                                 <font face="Arial, Helvetica, sans-serif" size="-1">Mededeling&nbsp;&nbsp;&nbsp;+++ ' . $inumber.' +++</font>
                                </td>
                                <Td width="50%" valign="top">
                                	<table width="100%" cellpadding="1" cellspacing="0" border="0" >
                                    	<Tr>
                                        	<td width="30%" align="left"><font face="Arial, Helvetica, sans-serif" size="+1">Datum</font></td>
                                            <td width="70%" align="left"><font face="Arial, Helvetica, sans-serif" size="-1">'. $splitarraydate[2]." ".$confirmmonthname." ".$splitarraydate[0] . '</font></td>
                                        </Tr>
                                        <Tr>
                                        	<td   align="left"><font face="Arial, Helvetica, sans-serif" size="+1">Uw Ref.</font></td>
                                            <td  align="left"><font face="Arial, Helvetica, sans-serif" size="-1">' . $companyinfo["name"].'</font></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left"><font face="Arial, Helvetica, sans-serif" size="+1">Onze Ref.</font></td>
                                            <td  align="left"><font face="Arial, Helvetica, sans-serif" size="-1">' . $companyinfo["name"].' - +++ '. $inumber. ' +++</font></td>
                                        </Tr>
                                    </table>
                                </Td>
                             </tr>
                             <tr>
                             	<td >
                                	<table width="100%" cellpadding="1" cellspacing="0" border="0" >
                                    	<tr>
                                        	<td><font face="Arial, Helvetica, sans-serif" size="-1">Job:</font></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td><font face="Arial, Helvetica, sans-serif" size="-1">Servicekost maaltijdcheques</font></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td ><font face="Arial, Helvetica, sans-serif" size="-1">
                               ' . $presentmonthname .' ' .$year .' </font>
                                </td>
                            </tr>
                            <Tr><td colspan="2" height="2"></td></Tr>
                            <tr>
                            	<Td colspan="2">
                                	<table width="100%" cellpadding="2" cellspacing="0" border="0" >
                                    	<Tr>
                                        	<td width="55%" align="left" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+1">Omschrijving</font></td>
                                            <td width="10%" align="right" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+1">Aantal</font></td>
                                            <td width="15%" align="right" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+1">Eenh. Prijs</font></td>
                                            <td width="15%" align="right" style="border-right:#000000 1px dotted; border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+1">Bedrag</font></td>
                                            <td width="5%" align="center" style="border-top:#000000 1px solid; border-bottom:#000000 1px solid;"><font face="Arial, Helvetica, sans-serif" size="+1">�</font></td>
                                        </Tr>
                                        <Tr>
                                        	<td  colspan="5" align="left" style=" border-bottom:#000000 1px solid;" height="5"></td>
                                           
                                        </Tr>
                                        <Tr>
                                        	<td   align="left" height="4" style="border-right:#000000 1px dotted; "></td>
                                           	<td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">Servicekost maaltijdcheques</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' . $presentmonthname .' ' .$year .'</font> </td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td   align="left" height="4" style="border-right:#000000 1px dotted; "></td>
                                           	<td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="+1">Nieuwe gebruikers</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">AANMAKEN NIEUWE KAARTEN</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">- Picturecards</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' .$picturecardinfo[0]["picturecardusers"] .'</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' . number_format($companyinfo["cardcost"],2).'</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">'. number_format($pictotal,2)  .'</font></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">- Standaard kaarten</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' . $standardcardinfo[0]["standardusers"] . '</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' . number_format($companyinfo["standardcardcost"],2).'</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">'. number_format($standardtotal,2)  .'</font></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td   align="left" height="4" style="border-right:#000000 1px dotted; "></td>
                                           	<td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                         <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; ">
                                            	<table width="100%" cellpadding="0" cellspacing="0" border="0">	
                                                	<tr>
                                                    	<td align="right" width="45%"><font face="Arial, Helvetica, sans-serif" size="-1">Bestelmaand</font></td>
                                                        <td align="right" width="5%"></td>
                                                        <td align="left" width="45%"><font face="Arial, Helvetica, sans-serif" size="-1">' . $presentmonthname . ' ' . $year .'</font></td>
                                                    </tr>
                                                </table>
                                                        
                                            </td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' . $userstotal .'</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">'.number_format($total,2). '</font></td>
                                            <td  align="center" >�</td>
                                        </Tr>
                                        <Tr>
                                        	<td   align="left" height="4" style="border-right:#000000 1px dotted; "></td>
                                           	<td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="+1">Maaltijdcheques</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">SERVICEKOST E-KENA</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "></td>
                                            <td  align="center" ></td>
                                        </Tr>
                                        <Tr>
                                        	<td  align="left" style="border-right:#000000 1px dotted; ">
                                            	<table width="100%" cellpadding="0" cellspacing="0" border="0">	
                                                	<tr>
                                                    	<td align="right" width="45%"><font face="Arial, Helvetica, sans-serif" size="-1">Bestelmaand</font></td>
                                                        <td align="right" width="5%"></td>
                                                        <td align="left" width="45%"><font face="Arial, Helvetica, sans-serif" size="-1">' . $presentmonthname . ' ' . $year .'</font></td>
                                                    </tr>
                                                </table>
                                                        
                                            </td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' .$userstotal .'</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">'.number_format($monthlyfee,2).'</font></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; "><font face="Arial, Helvetica, sans-serif" size="-1">' . number_format($totalvouchersamt,2) .'</font></td>
                                            <td  align="center" >�</td>
                                        </Tr>
                                        <Tr>
                                        	<td   align="left" height="4" style="border-right:#000000 1px dotted; border-bottom:#000000 1px dotted; "></td>
                                           	<td  align="right" style="border-right:#000000 1px dotted; border-bottom:#000000 1px dotted;"></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; border-bottom:#000000 1px dotted;"></td>
                                            <td  align="right" style="border-right:#000000 1px dotted; border-bottom:#000000 1px dotted;"></td>
                                            <td  align="center" style="border-bottom:#000000 1px dotted;" ></td>
                                        </Tr>
                                        <Tr>
                                        	
                                            <td  align="right" colspan="5" height="2" ></td>
                                        </Tr>

                                        <Tr>
                                        	
                                            <td  align="right" colspan="3" ><font face="Arial, Helvetica, sans-serif" size="-1">Netto bedrag </font></td>
                                            <td  align="right" ><font face="Arial, Helvetica, sans-serif" size="-1">' .number_format($totalamount,2). '</font></td>
                                            <td  align="left"  ><font face="Arial, Helvetica, sans-serif" size="-1">�</font></td>
                                        </Tr>
                                        <Tr>
                                        	
                                            <td  align="right" colspan="5" height="2" ></td>
                                        </Tr>

                                        <Tr>
                                        	
                                            <td  align="right" colspan="3" ><font face="Arial, Helvetica, sans-serif" size="-1"> 21% BTW </font></td>
                                            <td  align="right" ><font face="Arial, Helvetica, sans-serif" size="-1">'.number_format($vat,2) . '</font></td>
                                            <td  align="left"  ></td>
                                        </Tr>
                                        <Tr>
                                        	
                                            <td  align="right" colspan="5" height="2" ></td>
                                        </Tr>

                                        <Tr>
                                        	
                                            <td  align="right" colspan="3" ><font face="Arial, Helvetica, sans-serif" size="-1">Te betalen </font></td>
                                            <td  align="right" ><font face="Arial, Helvetica, sans-serif" size="-1">' .number_format($grandtotal,2). '</font></td>
                                            <td  align="left"  ><font face="Arial, Helvetica, sans-serif" size="-1">�</font></td>
                                        </Tr>
                                        <Tr>
                                        	
                                            <td  align="right" colspan="5" height="2" ></td>
                                        </Tr>

                                        <Tr>
                                        	
                                            <td  align="right" colspan="3" ><font face="Arial, Helvetica, sans-serif" size="-1">Vervaldatum </font></td>
                                            <td  align="center" colspan="2" ><font face="Arial, Helvetica, sans-serif" size="-1">'.$exactday.'</font></td>
                                        </Tr>
                                        <Tr>
                                        	
                                            <td  align="right" colspan="5" height="2" ></td>
                                        </Tr>
                                    </table>
                                </Td>
                           </tr>
                         </table>
                      </td>
                   </Tr>
                   <tr>
                   	<td colspan="2" align="left"><font face="Arial, Helvetica, sans-serif" size="-2">Over te schrijven op een van onderstaande rekeningen met de gestructureerde medeling</font></td>
                    <td  align="left"><font face="Arial, Helvetica, sans-serif" size="-2">+++ '. $inumber. '+++</font></td>
                   </tr>
                   <tr>
                   	<td colspan="3" align="left"><font face="Arial, Helvetica, sans-serif" size="-2">FACTUURVOORWAARDEN:  De betalingstermijn is 30 dagen einde maand.  Bij laattijdige betaling wordt het bedrag van de factuur vermeerderd met 3%, met een minimum van 100 EUR.</font></td>
                   </tr>
                   <tr>
                   	<td colspan="3" align="left"><font face="Arial, Helvetica, sans-serif" size="-2">De interest is van rechtswege en zonder enige ingebrekestelling verschuldigd vanaf  het verstrijken van de betalingstermijn.</font></td>
                   </tr>
                   <tr>
                   	<td colspan="3">
                    		<table width="90%" cellpadding="1" cellspacing="0" border="0" style="border:#000000 2px solid;">
                        	<tr>
											<td align="left" width="30%" valign="top" style="border-right:#000000 2px solid;">
											<font face="Arial, Helvetica, sans-serif" size="+1">
											Tunz.com inzake E-Kena/E-ve<br />IBAN : BE76732014253795<br />BIC : CREGBEBB
											</font>
											</td>
										</tr>
                         </table>
                    </td>
                   </tr>
                   <tr>
                   	<td colspan="3" align="center"><font face="Arial, Helvetica, sans-serif" size="-1">Wij danken u voor het vertrouwen !</font></td>
                   </tr>
              </table>
            
        </td>
    </tr>
</table>
';
		return $str;
	}
	
	public function invoiceformat($confirminfo,$itype="")
	{
		
		//print_r($confirminfo);
		$invoicenumber=$confirminfo[0]["invoicenumber"];
		
		
		if($itype==""){		
			$invoice = "103" . $confirminfo[0]["invoicevalue"] . $invoicenumber;
			$inumber = "103 - " . $confirminfo[0]["invoicevalue"] .$invoicenumber;
		} else{
			$invoice = "104" .  $confirminfo[0]["servicevalue"] . $invoicenumber;
			$inumber = "104 - " .  $confirminfo[0]["servicevalue"] . $invoicenumber;
		}
		

		//echo $invoice;
		$result = (98 - ($invoice * (100 %97) % 97));
		//echo $result . "<br>";
		$inumber.= "  - ". $result;
		//echo $inumber . "<Br>";
		return $inumber;
	}

}

?>