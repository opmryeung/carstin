<?php
@session_start();

$debug_String = '';

function myErrorHandler($errno, $errstr, $errfile, $errline) {
	if ( E_RECOVERABLE_ERROR===$errno ) {
		echo "An error has occured; please contact the development team ryeung@opm.us.com<br/>";
		echo $errstr."<br>";
		return true;
	}
	return false;
}

set_error_handler('myErrorHandler');

function printTable($data){
	
	if(empty($data))
	return 'TABLE IS EMPTY RIGHT NOW';
	
	$table = '<table class="table table-striped table-condensed">';
	$table.= '<thead>';
	$table.= '<tr>';
	$table.= '<th>ID #</th>';
	$table.= '<th>Date</th>';
	$table.= '<th>Project Name</th>';
	$table.= '<th>Contact Info</th>';
	$table.= '<th>Item Data</th>';
	$table.= '<th>Comment</th>';
	$table.= '<th>Image</th>';
	$table.= '</tr>';
	$table.= '</thead>';
	$table.= '<tbody>';
	foreach ($data as $d){
		
		$decoded_item = $dataArray = json_decode(stripslashes($d['DATA']),TRUE);

		//var_dump($decoded_item);
		
		$table.= '<tr>';
		$table.= '<td>'.$d['id'].'</td>';
		$table.= '<td>'.$d['add_date'].'<br/>';
		
		$table.= '<form action="/view-item/" method="post" role="form" style ="float: left; padding: 5px;">';
		$table.= '<input name="item_id" type="hidden" value="'.$d['id'].'">';
		$table.= '<button class="btn btn-mini btn-info" type="submit">Load Item</button> ';
		$table.= '</form>';
		
		$table.= '<form action="/results/" method="post"  role="form" style ="float: left; padding: 5px;">';
		$table.= '<input name="Account" type="hidden" value="'.$d['AccountName'].'">';
		$table.= '<input name="ProjectName" type="hidden" value="'.$d['ProjectName'].'">';
		$table.= '<input name="ContactEmail" type="hidden" value="'.$d['ContactEmail'].'">';
		$table.= '<button class="btn btn-mini btn-inverse" type="submit" name="LoadProject" value="1">Load Project</button>';
		$table.= '</form>';
		
		$table.= '</td>';
		$table.= '<td>'.$d['ProjectName'].'</td>';
		$table.= '<td>'.$d['ContactName'].'<br/>'.$d['CompanyName'].'<br/>'.$d['ContactPhone'].'<br/>'.$d['ContactEmail'].'</td>';
		$table.= '<td>';
		if(is_array($decoded_item)){
			$table.= 'Total Sq.Ft.: '.number_format(getSqFt($decoded_item),2).'<br/>';
			$table.= 'Total Ln.Ft.: '.number_format(getLnFt($decoded_item),2).'<br/>';
		}else{
			$table.= "Can't Calculate Sq.Ft. and Ln.Ft. <br/>";
			//$table.= "<!-- ".var_export($d['DATA'],TRUE)." -->";
		}
		$table.= 'Upscale: '.$d['UpScale'];
		$table.= '</td>';
		$table.= '<td>'.$d['Comment'].'</td>';
		if(!empty($d['FileName']))
		$table.= '<td><a href="'.get_bloginfo('template_url').'/UPLOADED_FILES/'.$d['FileName'].'" target="_blank"><i class="icon-file"></i></a></td>';
		else
		$table.= '<td>N/A</td>';
		$table.= '</tr>';

	}
	$table.= '</tbody>';
	$table.= '</table>';
	
	return $table;
	
}

function updatePricing(mysqli $mysqli, array $data){
	
	foreach ($data as $k=>$d){	
              
		if($k == 'Table' || $k == 'UpdateData'){
			//Do nothing
		}else{
			if($data['Table']=='Settings')
			$sql = 'UPDATE `'.$data['Table'].'` SET value = \''.$d.'\' WHERE id = '.$k;
			elseif($data['Table']=='DEALERS')
			$sql = 'UPDATE `'.$data['Table'].'` SET `Stone/Solid Surface Discount` = \''.$d.'\' WHERE id = '.$k;
			else 
			$sql = 'UPDATE `'.$data['Table'].'` SET cost = \''.$d.'\' WHERE id = '.$k;
			
			executeSQL($mysqli, $sql);
		}
	}
	
	echo 'Prices Updated!';
}

function printSettingsTable($data, $type){
	
	if(empty($data))
	return 'TABLE IS EMPTY RIGHT NOW';
	
	if($type=='MaterialCost'){
		$table = '<table class="table table-striped table-hover table-condensed">';
		$table.= '<thead>';
		$table.= '<tr>';
		$table.= '<th>ID #</th>';
		$table.= '<th>Name</th>';
		$table.= '<th>Description</th>';
		$table.= '<th>Unit</th>';
		$table.= '<th>Cost</th>';
		$table.= '</tr>';
		$table.= '</thead>';
		$table.= '<tbody>';
		$table.= '<input name="Table" type="hidden" value="Pricing">';
		foreach ($data as $d){
			$table.= '<tr>';
			$table.= '<td>'.$d['id'].'</td>';
			$table.= '<td>'.$d['name'].'</td>';
			$table.= '<td>'.$d['description'].'</td>';
			$table.= '<td>'.$d['unit'].'</td>';
			$table.= '<td>';
			$table.= '<input name="'.$d['id'].'" type="text" value="'.$d['cost'].'" class="input-small text-right">';
			$table.= '</td>';
			$table.= '</tr>';
		}
		$table.= '</tbody>';
		$table.= '</table>';
	}elseif($type=='StoneOption'){
		$table = '<table class="table table-striped table-hover table-condensed">';
		$table.= '<thead>';
		$table.= '<tr>';
		$table.= '<th>ID #</th>';
		$table.= '<th>Type</th>';
		$table.= '<th>Description</th>';
		$table.= '<th>Unit</th>';
		$table.= '<th>Cost</th>';
		$table.= '</tr>';
		$table.= '</thead>';
		$table.= '<tbody>';
		$table.= '<input name="Table" type="hidden" value="Pricing">';
		foreach ($data as $d){
			$table.= '<tr>';
			$table.= '<td>'.$d['id'].'</td>';
			$table.= '<td>'.$d['type'].'</td>';
			$table.= '<td>'.$d['description'].'</td>';
			$table.= '<td>'.$d['unit'].'</td>';
			$table.= '<td>';
			$table.= '<input name="'.$d['id'].'" type="text" value="'.$d['cost'].'" class="input-small text-right">';
			$table.= '</td>';
			$table.= '</tr>';
		}
		$table.= '</tbody>';
		$table.= '</table>';
	}elseif($type=='CollectionOption'){
		$table = '<table class="table table-striped table-hover table-condensed">';
		$table.= '<thead>';
		$table.= '<tr>';
		$table.= '<th>ID #</th>';
		$table.= '<th>Name</th>';
		$table.= '<th>Ogee</th>';
		$table.= '<th>Description</th>';
		$table.= '<th>Cost</th>';
		$table.= '</tr>';
		$table.= '</thead>';
		$table.= '<tbody>';
		$table.= '<input name="Table" type="hidden" value="Pricing_Backend">';
		foreach ($data as $d){
			$table.= '<tr>';
			$table.= '<td>'.$d['id'].'</td>';
			$table.= '<td>'.$d['name'].'</td>';
			$table.= '<td>'.$d['ogee'].'</td>';
			$table.= '<td>'.$d['description'].'</td>';
			$table.= '<td>';
			$table.= '<input name="'.$d['id'].'" type="text" value="'.$d['cost'].'" class="input-small text-right">';
			$table.= '</td>';
			$table.= '</tr>';
		}
		$table.= '</tbody>';
		$table.= '</table>';		
	}elseif($type=='Other'){
		$table = '<table class="table table-striped table-hover table-condensed">';
		$table.= '<thead>';
		$table.= '<tr>';
		$table.= '<th>ID #</th>';
		$table.= '<th>Option</th>';
		$table.= '<th>Modifier</th>';
		$table.= '<th>Value</th>';
		$table.= '</tr>';
		$table.= '</thead>';
		$table.= '<tbody>';
		$table.= '<input name="Table" type="hidden" value="Settings">';
		foreach ($data as $d){
                     
			$table.= '<tr>';
			$table.= '<td>'.$d['id'].'</td>';
			$table.= '<td>'.$d['option'].'</td>';
			$table.= '<td>'.$d['modifier'].'</td>';
			$table.= '<td>';
			$table.= '<input name="'.$d['id'].'" type="text" value="'.$d['value'].'" class="input-small text-right">';
			$table.= '</td>';
			
			$table.= '</tr>';
		}
		$table.= '</tbody>';
		$table.= '</table>';
	}elseif($type=='Dealers'){
		
		//"SELECT `Ship To Number`, `Name`, `Primary Customer Contact`, `Primary email`, `Phone Number`, `Stone/Solid Surface Discount` FROM DEALERS ORDER BY id ASC";
		
		$table = '<table class="table table-striped table-hover table-condensed">';
		$table.= '<thead>';
		$table.= '<tr>';
		$table.= '<th>Login Name</th>';
		$table.= '<th>Company Name</th>';
		$table.= '<th>Contact</th>';
		$table.= '<th>Email</th>';
		$table.= '<th>Phone</th>';
		$table.= '<th>Stone Discount</th>';
		$table.= '</tr>';
		$table.= '</thead>';
		$table.= '<tbody>';
		$table.= '<input name="Table" type="hidden" value="DEALERS">';
		foreach ($data as $d){
			$table.= '<tr>';
			
			$table.= '<td>'.$d['Ship To Number'].'</td>';
			$table.= '<td>'.$d['Name'].'</td>';
			$table.= '<td>'.$d['Primary Customer Contact'].'</td>';
			$table.= '<td>'.$d['Primary email'].'</td>';
			$table.= '<td>'.$d['Phone Number'].'</td>';
			$table.= '<td>';
			$table.= '<input name="'.$d['id'].'" type="text" value="'.$d['Stone/Solid Surface Discount'].'" class="input-small text-right">';
			$table.= '</td>';
			
			$table.= '</tr>';
		}
		$table.= '</tbody>';
		$table.= '</table>';
	}else{
		echo "No Bacon Here...";
	}
	
	$table.= '<input type="submit" class="btn" name="UpdateData" value="Update Data">';
	
	return $table;
}

function printProjectTable($data){
	if(empty($data))
	return 'TABLE IS EMPTY RIGHT NOW';
	
	$table = '<table class="table table-striped table-hover table-condensed" style="background: #fff;-moz-border-radius: 15px;
border-radius: 15px;">';
	$table.= '<thead>';
	$table.= '<tr>';
	$table.= '<th>ID #</th>';
	$table.= '<th>Mod. Date</th>';
	$table.= '<th>Project Name</th>';
	$table.= '<th>Contact Info</th>';
	$table.= '<th>Item Count</th>';
	$table.= '</tr>';
	$table.= '</thead>';
	$table.= '<tbody>';
	foreach ($data as $d){
		
		//$decoded_item = $dataArray = json_decode(stripslashes($d['DATA']),TRUE);

		//echo '<!-- '.var_export($d,TRUE).' -->';
		
		$table.= '<tr>';
		$table.= '<td>'.$d['id'].'</td>';
		$table.= '<td>'.$d['ModDate'];
		$table.= '</td>';
		$table.= '<td>'.$d['ProjectName'].'<br/>';
		$table.= '<form action="/results/" method="post" role="form" style="float: left; padding: 5px;">';
		$table.= '<input name="Account" type="hidden" value="'.$d['AccountName'].'">';
		$table.= '<input name="ProjectName" type="hidden" value="'.$d['ProjectName'].'">';
		$table.= '<input name="ContactEmail" type="hidden" value="'.$d['ContactEmail'].'">';
		$table.= '<button class="btn btn-mini btn-inverse" type="submit" name="LoadProject" value="1">Review Project and Order</button>';
		$table.= '</form>';
		
		//?a=1&p='.$ProjectName.'&n='.$ContactName.'&c='.$CompanyName.'&cp='.$ContactPhone.'&ce='.$ContactEmail.'&acc='.$AccountName.'&f='.$FileName.'&d='.$Deliver);
		
		$table.= '<form action="/calculate/" method="get" role="form" style="float: left; padding: 5px;">';
		$table.= '<input name="a" type="hidden" value="1">';
		$table.= '<input name="p" type="hidden" value="'.$d['ProjectName'].'">';
		$table.= '<input name="n" type="hidden" value="'.$d['ContactName'].'">';
		$table.= '<input name="c" type="hidden" value="'.$d['CompanyName'].'">';
		$table.= '<input name="cp" type="hidden" value="'.$d['ContactPhone'].'">';
		$table.= '<input name="ce" type="hidden" value="'.$d['ContactEmail'].'">';
		$table.= '<input name="acc" type="hidden" value="'.$d['AccountName'].'">';
		$table.= '<input name="d" type="hidden" value="">';
		$table.= '<button class="btn btn-mini" type="submit" name="LoadProject" value="1">Add to this Project</button>';
		$table.= '</form>';
		$table.= '</td>';
		$table.= '<td>'.$d['ContactName'].'<br/>'.$d['CompanyName'].'<br/>'.$d['ContactPhone'].'<br/>'.$d['ContactEmail'].'</td>';
		$table.= '<td>'.$d['DataCount'].'</td>';
		$table.= '</tr>';

	}
	$table.= '</tbody>';
	$table.= '</table>';
	
	return $table;
}

function printItemDetails(mysqli $mysqli, array $data){
	
	if(empty($data))
	return "ITEM DOESN'T EXIST";
	
	echo '<dl class="dl-horizontal">';
	echo '<dt>Account: </dt><dd>'.$data['Account'].'</dd>';
	echo '<dt>Project Name: </dt><dd>'.$data['ProjectName'].'</dd>';
	echo '<dt>Contact Name: </dt><dd>'.$data['ContactName'].'</dd>';
	echo '<dt>Company Name: </dt><dd>'.$data['CompanyName'].'</dd>';
	echo '<dt>Contact Phone: </dt><dd>'.$data['ContactPhone'].'</dd>';
	echo '<dt>Contact Email: </dt><dd>'.$data['ContactEmail'].'</dd>';
	echo '</dl>';
	
	//echo '<h5><strong>Shape: '.$data['shape'].'</strong></h5>';
	echo '<div class="row-fluid">';
	echo '<div class="span4">';
	echo '<img class="pull-right" src="'.get_bloginfo('template_url').'/images/'.str_replace(' ','_',$data['shape']).'.png" alt="An image of the table should be shown here; please report to ryeung@opm.us.com if you see this message; Thanks">';
	echo '</div>';
	echo '<div class="span8">';
	
	echo '<table class="table table-striped table-hover table-condensed">';
	echo '<thead>';
	echo '<tr>';
	echo '<th>Edge</th>';
	echo '<th>Length</th>';
	echo '<th>BS - Length</th>';
	echo '<th>BS - Height</th>';
	echo '<th>LF - Edge</th>';
	echo '<th>Edge Grp</th>';
	echo '</tr>';
	echo '</thead>';
	echo '<tbody>';
	
	foreach ($data[$data['shape']] as $key=>$edge){
		if($key!='Corner'){
			echo '<tr>';
			echo '<td>'.$key.'</td>';
			foreach($edge as $k=>$e){
				echo '<td>';
				if($k == 'Edge-Grp')
				echo lookupOption($mysqli, $e);
				else
				echo $e;
				echo '</td>';
			}
			echo '<tr>';
		}
	}
	
	echo '</tbody>';
	echo '</table>';
	
	/*foreach($data[$data['shape']]['Corner'] as $key=>$corner){
		//echo $corner.'<br/>';
		if($corner==0){
			echo '- '.$key.' : NONE<br/>';
		}else{
			//var_dump($corner);
			if(is_int((int)$corner))
			echo '- '.$key.' : '.lookupOption($mysqli, $corner).'<br/>';
		}
	}*/
	//echo $data[$data['shape']]['Corner'].'<br/>';
	echo '</div>';
	echo '</div>';
	
	echo '<strong><h3>Options</h3></strong>';
	foreach($data['option'] as $k=>$option){
		
		if(!empty($option))
		echo lookupOption($mysqli, $k).' : '.$option.' ('.lookupOption($mysqli, $k,'unit').')<br/>';
		
	}
	
	if(!isset($data['option'][46]))
	echo lookupOption($mysqli, 46).' : 0 ('.lookupOption($mysqli, 46,'unit').')<br/>';
	
	//echo '<pre>';
	//var_dump($data['option']);
	//echo '</pre>';
	
}

function removeItem(mysqli $mysqli, $id){
	$sql = "INSERT INTO ORDER_LOG_TRASH SELECT * FROM ORDER_LOG WHERE id = $id";
	executeSQL($mysqli, $sql);
	$sql = "DELETE FROM ORDER_LOG WHERE id = $id";
	executeSQL($mysqli, $sql);
}

function lookupOption(mysqli $mysqli, $id, $other=NULL){
	$sql = 'SELECT * FROM Pricing WHERE id = '.$id;
	//echo '<pre>'.$sql.'</pre>';
	$data = readSQL($mysqli, $sql);
	$data = $data[0];
	if(!is_null($other))
	return $data[$other];
	return $data['description'];
}

function executeSQL(mysqli $mysqli, $sql, $supress=TRUE){
	
	if (!$mysqli->query($sql)){
		echo "SQL: ".$sql."\n";
		throw new exception( "Execute Error: (" . $mysqli->errno . ") " . $mysqli->error . "\n");	
	}else{
		if($supress){
			
			if (mysqli_warning_count($mysqli)) {
				$e = mysqli_get_warnings($mysqli);
				do {
					echo "Warning: $e->errno: $e->message\n";
					echo "SQL: ".$sql."\n";
				} while ($e->next());
			}
			
			return TRUE;
		}else{
			echo "\tStatus       : ".$mysqli->info."\n";
			echo "\tAffected Rows: ".$mysqli->affected_rows."\n";
			
			if (mysqli_warning_count($mysqli)) {
				$e = mysqli_get_warnings($mysqli);
				do {
					echo "Warning: $e->errno: $e->message\n";
					sleep(10);
				} while ($e->next());
			}		
			return TRUE;
		}
	}
}

function readSQL(mysqli $mysqli, $sql, $col=NULL){
	
//echo $sql;

	$data = array();
	//return $results;
	//exit;
	if ($results = $mysqli->query($sql)) {

//echo $results;
		if($results->num_rows!=0){
			while ($row = $results->fetch_assoc()){

//print_r($row);

				if(!is_null($col) && key_exists($col, $row))
				array_push($data, $row[$col]);
				else 
				array_push($data, $row);
			}
		}
		$results->close();
	}else{
		echo "SQL: ".$sql."\n";
		// throw new Exception( "Execute Error: (" . $mysqli->errno . ") ". $mysqli->error . "\n");	
	}
	
	//echo $sql."<br/>";
	
	return $data;
}

function getProjectList(mysqli $mysqli, $account){

	$data = readSQL($mysqli, "SELECT id, 
									 AccountName,
							  		 MAX(add_date) as ModDate,
							  		 MIN(add_date) as CreateDate,
							  		 ProjectName,
							  		 ContactName,
							  		 CompanyName,
							  		 ContactPhone,
							  		 ContactEmail,
							  		 COUNT(DATA) AS DataCount
							  FROM ORDER_LOG
							  WHERE UpScale = 0 AND AccountName = '$account'
							  GROUP BY ProjectName
							  ORDER BY ProjectName ASC");
	
	if(!empty($data))
	return $data;
	else
	return FALSE;
}

function getItemList(mysqli $mysqli, $projectName=NULL, $accountName=NULL, $contactEmail=NULL){
	
	if(empty($projectName) || empty($projectName) || empty($projectName))
		$data = readSQL($mysqli, "SELECT * FROM ORDER_LOG WHERE UpScale IS NOT NULL ORDER BY add_date DESC LIMIT 500");
	else
		$data = readSQL($mysqli, "SELECT * FROM ORDER_LOG WHERE ProjectName = '$projectName' AND AccountName='$accountName' AND ContactEmail = '$contactEmail' AND UpScale = 0 LIMIT 500");
	
	 //echo '<pre>';
	 //print_r($data);
	 //echo '</pre>';

	 //exit;
	if(!empty($data))
		return $data;
	else
		return FALSE;
	
}

function printItemList(array $itemList, $target=NULL){
	
	if(empty($itemList))
	return 'ITEM LIST IS EMPTY RIGHT NOW';
	
	
	echo '<table class="table">';
	echo '<thead><tr>';
	if($_SERVER['REQUEST_URI']!='/quote-submission/')
	//echo '<th>Action</th>';
	echo '<th>Shape</th>';
	echo '<th>Sq. Ft.</th>';
	echo '<th>Ln. Ft.</th>';
	echo '<th>Comment</th>';
	if($_SERVER['REQUEST_URI']!='/quote-submission/')
	echo '<th>File</th>';
	echo '</tr></thead>';
	echo '<tbody>';
	foreach($itemList as $item){
		
		$dataArray = json_decode(stripslashes($item['DATA']),TRUE);		
		
		echo '<tr>';
		if($_SERVER['REQUEST_URI']!='/quote-submission/'){
			//echo '<td>';
			//echo '<!-- Edit Form Here -->';
			//echo '<form action="/results/" method="post" role="form" style="float: left; padding: 5px;">';
			//echo '<input name="Account" type="hidden" value="'.$item['AccountName'].'">';
			//echo '<input name="ProjectName" type="hidden" value="'.$item['ProjectName'].'">';
			//echo '<input name="ContactEmail" type="hidden" value="'.$item['ContactEmail'].'">';
			//echo '<button class="btn btn-mini btn-primary " disabled type="submit" name="EditItem" value="'.$item['id'].'">Edit</button> ';
			//echo '</form>';
			//echo '<form action="/results/" method="post" role="form" style="float: left; padding: 5px;">';
			//echo '<input name="Account" type="hidden" value="'.$item['AccountName'].'">';
			//echo '<input name="ProjectName" type="hidden" value="'.$item['ProjectName'].'">';
			//echo '<input name="ContactEmail" type="hidden" value="'.$item['ContactEmail'].'">';
			//echo '<button class="btn btn-mini btn-danger " disabled type="submit" name="RemoveItem" value="'.$item['id'].'">Remove</button>';
			//echo '</form>';
			//echo '</td>';
		}
		echo '<td>'.$dataArray['shape'].'</td>';
		echo '<td>';
		echo number_format(getSqFt($dataArray),2);
		echo '</td>';
		echo '<td>';
		echo number_format(getLnFt($dataArray),2);
		echo '</td>';
		echo '<td>'.$item['Comment'].'</td>';
		if($_SERVER['REQUEST_URI']!='/quote-submission/')
		echo '<td><a href="'.get_bloginfo('template_url').'/UPLOADED_FILES/'.$item['FileName'].'" target="_blank">'.$item['FileName'].'</a></td>';
		echo '</tr>';
	}
	echo '</tbody>';
	echo '</table>';
}

function getShape(array $data){
	return $data['shape'];
}

function getSqFt(array $data, array $FullBackMod=NULL, $return=NULL){
	
	$shape = $data['shape'];
	
	$modifier = '';
	$value = '';
	
	$CounterTops = 0;
	$StandardBack = 0;
	$FullBack = 0;

        $backsplash_value ='';
        $backsplash_modifier ='';
	
	
	
	if(is_null($FullBackMod)){
		$modifier = '*';
		$value = 1;
	}else{
		$modifier = $FullBackMod[0]['modifier'];
		$value = $FullBackMod[0]['value'];
	}
	
	if($shape==='Custom'){		
		$CounterTops = $data[$shape]['A']['Length'];
		
		foreach($data[$shape] as $edge){
			$BacksplashHeight = isset($edge['Backsplash-Height']) ? $edge['Backsplash-Height'] : '';
                        
                        /// new code //////////////
                        $BacksplashLength = isset($edge['Backsplash-Length']) ? $edge['Backsplash-Length'] : '';
			////////////////////////////////////////
			if($BacksplashHeight<6){
				 //$StandardBack+= ($BacksplashHeight * $BacksplashHeight)/144;
                                 $StandardBack+= $BacksplashLength * ($BacksplashHeight/12);
			}else{
				 //$FullBack+= ($BacksplashHeight * $BacksplashHeight)/144;
                                  $FullBack+= $BacksplashLength * ($BacksplashHeight/12);
			}
		}	

///echo $FullBack . "<br>";	
	}
	
	if($shape==='Straight_Top'){
		$length = $data[$shape]['A']['Length'];
		$width = $data[$shape]['B']['Length'];
		
		$CounterTops = ($length * $width)/144; // = length.1/12 * length.2/12
		
		foreach($data[$shape] as $edge){
			$BacksplashHeight = isset($edge['Backsplash-Height']) ? $edge['Backsplash-Height'] : '';

                        /// new code //////////////
                        $BacksplashLength = isset($edge['Backsplash-Length']) ? $edge['Backsplash-Length'] : '';
			////////////////////////////////////////
			
			if($BacksplashHeight<6){
				//$StandardBack+= ($BacksplashHeight * $BacksplashHeight)/144;
                                 $StandardBack+= ($BacksplashLength * $BacksplashHeight)/144;
			}else{
				//$FullBack+= ($BacksplashHeight * $BacksplashHeight)/144;
                                $FullBack+= ($BacksplashLength * $BacksplashHeight)/144;
			}
		}		
	}
	//echo $FullBack;
	if($shape==='L-Shaped_Top'){
		$length = $data[$shape]['A']['Length'];
		$width = $data[$shape]['B']['Length'];
		
		$s_length = $data[$shape]['D']['Length'];
		$s_width = $data[$shape]['E']['Length']; 
		
		$CounterTops1 = ($length * $width)/144; // = length.1/12 * length.2/12
		$CounterTops2 = ($s_length * $s_width)/144; // = length.1/12 * length.2/12
		
		$CounterTops = $CounterTops1 - $CounterTops2;
		
		foreach($data[$shape] as $edge){
			$BacksplashHeight = isset($edge['Backsplash-Height']) ? $edge['Backsplash-Height'] : '';
			
			if($BacksplashHeight<6){
				$StandardBack+= ($BacksplashHeight * $BacksplashHeight)/144;
			}else{
				$FullBack+= ($BacksplashHeight * $BacksplashHeight)/144;
			}
		}	
	}
	
	if($shape==='U-Shaped_Top'){
		
		$C = $data[$shape]['C']['Length'];
		$B = $data[$shape]['B']['Length'];
		
		$G = $data[$shape]['G']['Length'];
		$H = $data[$shape]['H']['Length'];
		
		$E = $data[$shape]['E']['Length'];
		$D = $data[$shape]['D']['Length'];
		
		$part1 = ($C * $B)/144;
		$part2 = ($G * $H)/144;
		$part3 = ($E * ($B-$D))/144;
		
		$CounterTops = $part1+$part2+$part3;
		
		foreach($data[$shape] as $edge){
			$BacksplashHeight = isset($edge['Backsplash-Height']) ? $edge['Backsplash-Height'] : '';
			
			if($BacksplashHeight<6){
				$StandardBack+= ($BacksplashHeight * $BacksplashHeight)/144;
			}else{
				$FullBack+= ($BacksplashHeight * $BacksplashHeight)/144;
			}
		}	
	}

        /* Code for adding the backsplash */
        /*$mysqli = new mysqli('localhost','opm_dealdevdata',')#BpEa-5F2!}','opm_dealdevdata');
        $BacksplashMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Backsplash\'');
	$backsplash_modifier =$BacksplashMod[0]['modifier'];
        $backsplash_value =$BacksplashMod[0]['value'];

        switch($backsplash_modifier){
		case '*':
			$FullBack = ($FullBack * $backsplash_value);
			break;
		case '/':
			$FullBack = ($FullBack / $backsplash_value);
			break;
		case '+':
			$FullBack = ($FullBack + $backsplash_value);
			break;
		case '-':
			$FullBack = ($FullBack - $backsplash_value);
			break; 
	}*/
       
//echo $CounterTops . " CT<br>";

	if(!is_null($return)){
		switch($return){
			case 'CounterTop':
				return $CounterTops;
				break;
			case 'StandardBack':
				return $StandardBack;
				break;
			case 'FullBack':
				switch($modifier){
					case '*':
						return ($FullBack * $value);
						break;
					case '/':
						return ($FullBack / $value);
						break;
					case '+':
						return ($FullBack + $value);
						break;
					case '-':
						return ($FullBack - $value);
						break; 
				}
				break;
		}
	}
	//echo $FullBack * $value;

//echo $modifier . " 3333<br>";
//echo $CounterTops + $StandardBack + ($FullBack * $value) . " 4444<br>";

	switch($modifier){
		case '*':
			return $CounterTops + $StandardBack + ($FullBack * $value);
			break;
		case '/':
			return $CounterTops + $StandardBack + ($FullBack / $value);
			break;
		case '+':
			return $CounterTops + $StandardBack + ($FullBack + $value);
			break;
		case '-':
			return $CounterTops + $StandardBack + ($FullBack - $value);
			break; 
	}
	
	return FALSE;
}

function getLnFt(array $data){

	$shape = $data['shape'];

	
	
	$lnft = 0;
	if($shape!=''){
	foreach ($data[$shape] as $edge){
		$lfEdge = isset($edge['LF-Edge']) ? $edge['LF-Edge'] : '';
		$lnft+=$lfEdge;
	}
        }
	
	if($shape=='Custom')
	return $lnft;
	return $lnft/12;
}

function getCollectionPrice(mysqli $mysqli, $material, $SquareFeet, $LinearFeet, $option_cost, $ogee, $install = 1){
	
	global $_POST;
	
	
	if($install){
         //echo "SELECT SUM(Cost) AS cost FROM Pricing_Backend WHERE name = '$material' AND ogee=$ogee AND type!='extra'";
//echo "<br>";
	$base_sum = readSQL($mysqli, "SELECT SUM(Cost) AS cost FROM Pricing_Backend WHERE name = '$material' AND ogee=$ogee AND type!='extra'", 'cost');
	}else{
	$base_sum = readSQL($mysqli, "SELECT SUM(Cost) AS cost FROM Pricing_Backend WHERE name = '$material' AND ogee=$ogee AND type!='extra' AND description!='Install'", 'cost');
}
	$earn = readSQL($mysqli, "SELECT value FROM Settings WHERE `option` = 'Sales Earnings (%)'",'value');

	$base_sum = $base_sum[0];
	$earn = $earn[0];
	
	if($ogee=='1')
	$extra_sum = $option_cost + ($LinearFeet * 15 * (1-($earn/100)));
	else
	$extra_sum = $option_cost;
	
	//if($_SERVER['REMOTE_ADDR']=='216.138.113.106')
	//echo "<pre>(($base_sum * $SquareFeet)+$extra_sum)/(1-($earn/100))</pre>";
	
	$COST = (($base_sum * $SquareFeet)+$extra_sum)/(1-($earn/100));
	
	if(!empty($_POST['up_scale']))		
	return '$ '.number_format( $COST + ($COST*($_POST['up_scale']/100)) , 2 );
	else
	return '$ '.number_format( $COST ,2 );
}

function getCollectionOptionCost(mysqli $mysqli, array $options, $sqft){
	
	$Cost = 0;
	
	foreach ($options as $key=>$count){
	        //echo $key;
	        //echo "<br>";
		
		if($key == 28 && !empty($count)){
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 73", 'cost');
			$Cost+=$tempCost[0];
		}elseif($key == 37 && !empty($count)){
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 73", 'cost');
			$Cost+=$tempCost[0];
		}elseif($key == 38 && !empty($count)){
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 73", 'cost');
			$Cost+=$tempCost[0];
		}elseif($key == 34 && !empty($count)){
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 74", 'cost');
			$Cost+=$tempCost[0];
		}elseif($key == 30 && !empty($count)){
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 75", 'cost');
			$Cost+=$tempCost[0];
		}elseif($key == 36 && !empty($count)){
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 78", 'cost');
			$Cost+=$tempCost[0];
		}elseif($key == 46 && !empty($count)){
		//echo "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 77";
			$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing_Backend WHERE id = 77", 'cost');
			//$tempCost = readSQL($mysqli, "SELECT ($count * cost) as cost FROM Pricing WHERE id = 46", 'cost');
			$Cost+=$tempCost[0];
		}/*elseif($key == 48){
			$tempCost = readSQL($mysqli, "SELECT ($sqft * $count * cost) as cost FROM Pricing WHERE id = 49", 'cost');
			$Cost+=$tempCost[0];
		}*/
		
		//if($_SERVER['REMOTE_ADDR']=='216.138.113.106')
		//if(!empty($count))
		//echo "<pre>".$key.'; '.$tempCost[0].'; '.$count."</pre>";
	}
	
	$cleaner = readSQL($mysqli, "SELECT cost FROM Pricing_Backend WHERE id = 76", 'cost');
	$Cost+= $cleaner[0];
	
	return $Cost;
}

function getMaterialCost(mysqli $mysqli, array $sqft, $material, $description, $backsplash=0, $install=1){
      //echo "SELECT cost FROM Pricing WHERE name = '$material' AND description = '$description'";
//echo "<br>";
//print_r($sqft);


	$cost = readSQL($mysqli,"SELECT cost FROM Pricing WHERE name = '$material' AND description = '$description'",'cost');


	
	$backsplash_mod = readSQL($mysqli, "SELECT * FROM Settings WHERE `option` = 'Backsplash'");
	
	$install_mod = readSQL($mysqli, "SELECT cost FROM Pricing WHERE id = 48", 'cost');
	
	 $cost = $cost[0];
	
	//print_r($backsplash);
      
	if(!$install)
        $cost-= $install_mod[0];
	
      
	
     $backsplash_mod = $backsplash_mod[0];
	
	$backsplash_mod_val = $backsplash_mod['value'];
	
	$backsplash_cost = 0;
	
	switch ($backsplash_mod['modifier']) {
		case '*':
			$backsplash_cost = ($backsplash_cost * $backsplash_mod_val);
		break;
		case '/':
			$backsplash_cost = ($backsplash_cost / $backsplash_mod_val);
		break;
		case '+':
			$backsplash_cost = ($backsplash_cost + $backsplash_mod_val);
		break;
		case '-':
			$backsplash_cost = ($backsplash_cost - $backsplash_mod_val);
		break;
	}
	
	if($material === 'Caesarstone' || $material === 'Staron Radianz'){
		if($backsplash)
		return 0;
		
                return (($sqft[0]+$sqft[1])*$cost)+($sqft[2]*$backsplash_cost);
              
	}else{		
		if($backsplash){
			
			//if($_SERVER['REMOTE_ADDR']=='216.138.113.106')
			//echo '<pre>'."$sqft[2] * ($cost + $backsplash_cost) -".'</pre>';
			//echo $sqft[2] * ($cost + $backsplash_cost) . "<br>";

			return $sqft[2] * ($cost + $backsplash_cost);
			
		}else{
			
			//if($_SERVER['REMOTE_ADDR']=='216.138.113.106')
			//echo '<pre>'."($sqft[0] + $sqft[1]) * $cost - ".var_export($install,TRUE).'</pre>';
			//echo "11111 entering";
//echo ($sqft[0] + $sqft[1]) * $cost ."<Br>";

			return ($sqft[0] + $sqft[1]) * $cost;
		}
	}
}

function getOptionsCost(mysqli $mysqli, array $cornerArray, array $optionArray, $sqft=0){
	
	$cost = 0;
	$options = ($cornerArray + $optionArray);
///	print_r($options);
//echo "<br>";
	foreach ($options as $key=>$opt){

		//echo "<br>";
		$optCost = 0;
		
		if(!empty($opt)){
			//echo "key-----".$key."opt-----".$opt;
			if($key=='46' && $opt <= 25){ //Mileage Charge; if less ot equal 25 miles don't charge
				
				$optCost = 0;
				
			}elseif($key=='48'){ //If not install, subtract certain amount
				
				/*if(empty($opt))
				$opt = 0;
				else
				$opt = 1; 
				
				$sql = "SELECT ($opt * $sqft * cost) AS cost FROM Pricing WHERE id = $key";
				$optCost = readSQL($mysqli, $sql,'cost');
				$optCost = $optCost[0];
				
				//echo '<pre>';
				//echo 'subtracting '.$optCost.' ( '.$opt.' | '.$sqft.' )';
				//echo '</pre>';
				
				$cost-=$optCost;*/
				
				//DO NOTHING; NOT INSTALL COST CALCULATED SOMEWHERE ELSE
				
			}else{
				
				$sql = "SELECT ($opt * cost) AS cost FROM Pricing WHERE id = $key";
				$optCost = readSQL($mysqli, $sql,'cost');
				$optCost = $optCost[0];
				//echo $key . " --- ". $optCost. "<Br>";
				$cost+=$optCost;
			}
			
			//if($_SERVER['REMOTE_ADDR']=='216.138.113.106')
			//if(!empty($optCost))
			//echo '<pre>'.$key.' - '.$optCost.'</pre>';
			
		}
	}
	
//echo $cost . "<br><Br><br>";
	return $cost;
	
}

function getCornerCount(array $data){
	$shape = $data['shape'];
	
	$group0 = 0;
	$group18 = 0;
	$group36 = 0;
	$group54 = 0;
	
	/*foreach ($data[$shape]['Corner'] as $corner){
		if($corner == 0)
		$group0++;
		if($corner == 18)
		$group18++;
		if($corner == 36)
		$group36++;
		if($corner == 54)
		$group54++;
	}*/
	
	return array(0=>$group0,23=>$group18,24=>$group36,25=>$group54);
}

function get_SQ_Eased_Edge($material,$backsplash,$option){
	
	global $_POST;
	
	$cost = ($material+$backsplash+$option)*0.4;

//echo $material. "---" . $backsplash . "---". $option . "--" . $cost . "<br>";
//echo $cost . "-- Cost<br>";

	
	//if($_SERVER['REMOTE_ADDR']=='216.138.113.106')
	//echo "<pre>($material+$backsplash+$option)*0.4 = $cost</pre>";
	
	if(isset($_POST['up_scale']))
	$cost = $cost + ($cost * ($_POST['up_scale'] / 100));
	
	return $cost;
}

function get_Bevel_Bullnose_Roundover_DBL_Radius($material,$backsplash,$edge,$option){
	if(empty($edge))
	return '0';
	
	global $_POST;
	
	$cost = ($material+$backsplash+$edge+$option)*0.4;
	
	//echo "<pre>($material+$backsplash+$edge+$option)*0.4 = $cost</pre><br/>";
	
	if(isset($_POST['up_scale']))
	$cost = $cost + ($cost * ($_POST['up_scale'] / 100));
	
	return $cost;
}

function getEdgeCost(mysqli $mysqli, array $edges, $override_group=NULL){
	
	$sum = 0;
	
	$edgeCount = count($edges);

//print_r($edges);

//echo "<br>";

	foreach($edges as $edge){
		
		if($edgeCount==1){
			$length=$edge[0];
			$group=$edge[1];
		//echo $length . "--". $group . "<br>";
			if(empty($length))
			$sum+= 0;
			else{
				if(empty($override_group))
				$cost = readSQL($mysqli, "SELECT cost FROM Pricing WHERE id = $group", 'cost');
				else
				$cost = readSQL($mysqli, "SELECT cost FROM Pricing WHERE id = $override_group", 'cost');
				
				$cost = $cost[0];
				$sum+= $cost * ($length);
			}
		}else{
			$length=$edge[0];
			$group=$edge[1];
		
			if(empty($length))
			$sum+= 0;
			else{
				if(empty($override_group))
				$cost = readSQL($mysqli, "SELECT cost FROM Pricing WHERE id = $group", 'cost');
				else
				$cost = readSQL($mysqli, "SELECT cost FROM Pricing WHERE id = $override_group", 'cost');
				
				$cost = $cost[0];
				$sum+= $cost * ($length/12);
			}
		}
		
	}
	
//echo $sum  . "<br><Br>";
	return $sum;
	
}

function getEdges(array $data){
	$shape = $data['shape'];
	
	$array = array();
	
	foreach ($data[$shape] as $key=>$edge){
		$LF_edge = isset($edge['LF-Edge']) ? $edge['LF-Edge'] : '';
		$edge_Grp = isset($edge['Edge-Grp']) ? $edge['Edge-Grp'] : '';
		
		if($key!='Corner'){
			$tmp = array($LF_edge,$edge_Grp);
			array_push($array, $tmp);
		}
	}
	
//print_r($array);

	return $array;
}

function processUploadedFile(){
	
	global $_FILES;
	
	if(empty($_FILES["FileUpload"]["name"]))
	return NULL;
	
	$newFileName = date('YmdHis').'_'.$_FILES["FileUpload"]["name"];
	
	if ($_FILES["FileUpload"]["error"] > 0){
		//echo "Return Code: " . $_FILES["FileUpload"]["error"] . "<br>";
	}else{
		if (file_exists(get_template_directory().'/UPLOADED_FILES/'.$newFileName)){
			echo $newFileName . " already exists. ";
		}else{
   			move_uploaded_file($_FILES["FileUpload"]["tmp_name"],get_template_directory().'/UPLOADED_FILES/' . $newFileName);
		}
	}

	
	return $newFileName;
}

function processUploadedOrderForm(){
	
	global $_FILES;
	
	if(empty($_FILES["file"]["name"])){
		echo 'An error has occured; No order form was submitted (Filename: ['.$_FILES['file']['name'].']';
		echo '<br/>Program stopped at EMPTY FILE';
		exit();
		return NULL;
	}
	
	$newFileName = date('YmdHis').'_'.$_FILES["file"]["name"];
	
	if ($_FILES["file"]["error"] > 0){
		echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
		exit();
	}else{
		if (file_exists(get_template_directory().'/UPLOADED_FILES/'.$newFileName)){
			echo $newFileName . " already exists. ";
		}else{
   			move_uploaded_file($_FILES["file"]["tmp_name"],get_template_directory().'/UPLOADED_FILES/' . $newFileName);
		}
	}

	
	return $newFileName;
}

function storeData(mysqli $mysqli, $data, $file_Name){
	
	global $_POST;
	global $_FILES;
	
	$sizeCheck = getSqFt($data);
	
	
	$ProjectName= isset($_POST['ProjectName']) ? $_POST['ProjectName'] : '';
	$ContactName= isset($_POST['ContactName']) ? $_POST['ContactName'] : '';
	$CompanyName= isset($_POST['CompanyName']) ? $_POST['CompanyName'] : '';
	$ContactPhone= isset($_POST['ContactPhone']) ? $_POST['ContactPhone'] : '';
	$ContactEmail= isset($_POST['ContactEmail']) ? $_POST['ContactEmail'] : '';
	$Comment= isset($_POST['Comment']) ? $_POST['Comment'] : '';
	$Salesperson= isset($_POST['Salesperson']) ? $_POST['Salesperson'] : '';
	$FileName=$file_Name;
	$DATA=json_encode($data);
	$AccountName= isset($_POST['Account']) ? $_POST['Account'] : '';
	$Deliver= isset($_POST['option'][46]) ? $_POST['option'][46] : '';
	$Install= isset($_POST['option'][48]) ? $_POST['option'][48] : '';
	
	if(isset($_POST['up_scale'])){
		if($_POST['up_scale']==0)
		$upscale = 'NULL';
		else
		$upscale = $_POST['up_scale'];
	}else
	$upscale = 0;
	
	$sql = "INSERT INTO ORDER_LOG (ProjectName,ContactName,CompanyName,ContactPhone,ContactEmail,FileName,DATA,AccountName,IP,UpScale,Comment,Salesperson) VALUES";
	$sql.= '("'.addslashes($ProjectName).'",';
	$sql.= '"'.addslashes($ContactName).'",';
	$sql.= '"'.addslashes($CompanyName).'",';
	$sql.= '"'.preg_replace("/[^0-9]/","",$ContactPhone).'",';
	$sql.= '"'.addslashes($ContactEmail).'",';
	$sql.= '"'.addslashes($FileName).'",';
	$sql.= '"'.addslashes($DATA).'",';
	$sql.= '"'.addslashes($AccountName).'",';
	$sql.= '"'.$_SERVER['REMOTE_ADDR'].'",';
	$sql.= ''.$upscale.',';
	$sql.= '"'.addslashes($Comment).'",';
	$sql.= '"'.addslashes($Salesperson).'")';
	
	//echo $sql;
	//exit;

	
	if(isset($_POST['shape']) || isset($_POST['up_scale']))
	if(!empty($sizeCheck))
	executeSQL($mysqli, $sql);
	
	
	if(!isset($_POST['up_scale']))
	if(isset($_POST['StoreSubmit']) || ( isset($_POST['CalculationSubmit']) && !isset($_POST['shape']) ) || empty($sizeCheck) )
        
	header('Location: http://dealers.carstindev.onlinepositioning.com/calculate/?a=1&p='.$ProjectName.'&n='.$ContactName.'&c='.$CompanyName.'&cp='.$ContactPhone.'&ce='.$ContactEmail.'&acc='.$AccountName.'&f='.$FileName.'&d='.$Deliver.'&sp='.$Salesperson.'&i='.$Install);
	
	return TRUE;
	
}

function SQFT_LNFT_DATA(mysqli $mysqli, array $data, $returnArray=0){
	
	global $debug_string;
	$debug_string.= '['.__FUNCTION__.']'."\n";
	
	$totalSQFT = 0;
	$totalLNFT = 0;
	
	$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'');
          //$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Backsplash\'');
	
	foreach($data as $object){
		$item = json_decode(stripslashes($object['DATA']),TRUE);
		
		$totalSQFT+= getSqFt($item,$FullBackMod);
		$totalLNFT+= getLnFt($item);
		
		$debug_string.= '$totalSQFT: '.var_export($totalSQFT,TRUE)."\n";
		$debug_string.= '$totalLNFT: '.var_export($totalLNFT,TRUE)."\n";
		$debug_string.= '$FullBackMod: '.var_export($FullBackMod,TRUE)."\n";

	}
	
	$table = '<table class="table table-bordered">';
	$table.= '<thead>';
	$table.= '<tr>';
	$table.= '<th class="span2">Total Sq./Ft.</th>';
	$table.= '<td>'.number_format($totalSQFT,2).'</td>';
	$table.= '<th class="span2">Total Ln./Ft.</th>';
	$table.= '<td>'.number_format($totalLNFT,2).'</td>';
	$table.= '</tr>';
	$table.= '</thead>';
	$table.= '</table>';
	
	if($returnArray)
	return array($totalSQFT,$totalLNFT);
	else
	return $table;
}

function array_add($a1, $a2) {  // ...
  // adds the values at identical keys together
  $aRes = $a1;
  foreach (array_slice(func_get_args(), 1) as $aRay) {
    foreach (array_intersect_key($aRay, $aRes) as $key => $val) $aRes[$key] += $val;
    $aRes += $aRay; }
  return $aRes; }

function COLLECTION_TABLE(mysqli $mysqli, array $data){
	
	global $debug_string;
	$sqft ='';
	$debug_string.= '['.__FUNCTION__.']'."\n";
	
	$measurements = SQFT_LNFT_DATA($mysqli, $data, 1);
	
	$option_cost = 0;
	
	$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'');
	
	$totalOptions = array();
	//print_r($data);
	
	// NEW OPTION CALCULATION METHOD
	foreach($data as $object){
		$item = json_decode(stripslashes($object['DATA']),TRUE);
		
		$sqft+= getSqFt($item, $FullBackMod);
		
		$totalOptions = array_add($item['option'],$totalOptions);
	}
	
	if(empty($totalOptions[48]))
	$install = 1;
	else
	$install = 0;
	
	$option_cost = getCollectionOptionCost($mysqli, $totalOptions, $sqft);
	
	
	$materialArray = array('01-06','07-13','14-18','19-24');
	
	$table = '<table class="table table-bordered table-hover">
	<thead>
	<tr>
	
	<th class="span3">Material</th>
	<th>SQ Eased Edge</th>
	<th>Bevel, Bullnose, Roundover, DBL Radius</th>
	<th>Ogee Edge</th>
	</tr>
	</thead>
	<tbody>';
	
	foreach($materialArray as $material){
		$table.= '<tr onclick="selectRow(this)">';
		//$table.= '<td><input type="checkbox"></td>';
		$table.= '<td>'.$material.'</td>';
		$table.= '<td>'.getCollectionPrice($mysqli, $material, $measurements[0], $measurements[1], $option_cost, 0, $install).'</td>';
		$table.= '<td>'.getCollectionPrice($mysqli, $material, $measurements[0], $measurements[1], $option_cost, 0, $install).'</td>';
		$table.= '<td>'.getCollectionPrice($mysqli, $material, $measurements[0], $measurements[1], $option_cost, 1, $install).'</td>';
		$table.= '</tr>';
		
		$debug_string.= '$material: '.var_export($material,TRUE)."\n";
		$debug_string.= '$measurement[0]: '.var_export($measurements[0],TRUE)."\n";
		$debug_string.= '$measurement[1]: '.var_export($measurements[1],TRUE)."\n";
	}	

	$table.= '</tbody></table>';
	
	return $table;
	
}

function STONES_TABLE(mysqli $mysqli, array $data, $stoneName=NULL){

//print_r($data);
	
	global $debug_string;
	$debug_string.= '['.__FUNCTION__.']'."\n";
	
	$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'');

///echo 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'' .' 1<br>';
//	
	if(is_null($stoneName))
	$stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' GROUP BY name ORDER BY field(name,\'Collection\',\'Caesarstone\',\'Granite\',\'Hanstone\',\'Silestone\',\'SoapStone\',\'Staron Radianz\',\'Viatera\',\'Zodiaq\')', 'name');
        //$stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' GROUP BY name', 'name');
	else
        $stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' AND name = \''.$stoneName.'\' GROUP BY name ORDER BY field(name,\'Collection\',\'Caesarstone\',\'Granite\',\'Hanstone\',\'Silestone\',\'SoapStone\',\'Staron Radianz\',\'Viatera\',\'Zodiaq\')', 'name');
	//$stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' AND name = \''.$stoneName.'\' GROUP BY name', 'name');
	

	$table = '';
	
	$count=0;
		
	foreach($stones as $stone){
		$colors = readSQL($mysqli,'SELECT description FROM Pricing WHERE hide!=1 AND name="'.$stone.'"','description');

//echo 'SELECT description FROM Pricing WHERE hide!=1 AND name="'.$stone.'"' . '<br>';

		
		if($_SERVER['REQUEST_URI']=='/quote-submission/'){
			
			if($count%2==0){
                          if($stone =='Staron Radianz'){
			     $table.= '<div style="float: right;">';
                           }else{
                             $table.= '<div style="float: left;">';
                           }
                        }
			else{
                          if($stone =='Viatera'){
			     $table.= '<div style="float: left;">';
                           }else{
                             $table.= '<div style="float: right;">';
                           }
			  
                        }
		}
if($stone =='Collection'){
  $table.= '<h3>Carstin Brands Granite/Quartz Collection</h3>';	
}
else {
		$table.= '<h3>'.$stone.'</h3>';	
}	
		$table.= '<table class="table table-bordered table-hover">';
		$table.= '<thead>
				  <tr>
				  
				  <th class="span3">Material</th>
				  <th>SQ Eased Edge</th> 
				  <th>Bevel, Bullnose, Roundover, DBL Radius</th>
				  <th>Ogee Edge</th>
				  </tr>
				  </thead>';
		$table.= '<tbody>';
		
		foreach($colors as $color){
			
			$materialCost = $materialCost_ogee = $optionCost = $edgeCost_39 = $edgeCost_40 = 0;
			
			$totalSqFt = 0;
			
			foreach($data as $object){
				
				$item = json_decode(stripslashes($object['DATA']),TRUE);
				
				//echo "<br>";
				if(empty($item['option'][48])){
				   
					$install = 1;
                                       
                         
				}else{
				
				       $install = 0;
                                      
				    }

				
//echo "<br>";
				
				//echo '<pre>'.var_export($item['option'][48],TRUE).' - '.$install.'</pre>';
				
				$edge_array = getEdges($item);
				
				$counterTop_SqFt = getSqFt($item,$FullBackMod,'CounterTop');
				$stdBack_SqFt = getSqFt($item,$FullBackMod,'StandardBack'); 
				$fullBack_SqFt = getSqFt($item,$FullBackMod,'FullBack');
				
				$sqft_array = array($counterTop_SqFt,$stdBack_SqFt,$fullBack_SqFt);
				
//print_r($sqft_array);

				$totalSqFt+=array_sum($sqft_array);

//echo $totalSqFt . "<br><Br>";

				
				$materialCost+= getMaterialCost($mysqli, $sqft_array, $stone, $color,0,$install);
				$materialCost_ogee+= getMaterialCost($mysqli, $sqft_array, $stone, $color,1,$install);

//echo $materialCost . "-- " . $materialCost_ogee . "<Br>";

				$optionCost+= getOptionsCost($mysqli, getCornerCount($item), $item['option'], array_sum($sqft_array));

//echo $optionCost . "--". $edgeCost_39 ." Option Cost<br>";

				$edgeCost_39+= getEdgeCost($mysqli,$edge_array,39);
				$edgeCost_40+= getEdgeCost($mysqli,$edge_array,40);
				
			}
			
			if($totalSqFt<20){
				$materialCost = getMaterialCost($mysqli, array(20,0,0), $stone, $color,0,0);
				$materialCost_ogee = getMaterialCost($mysqli, array(20,0,0), $stone, $color,1,0);
			}
			
			if($totalSqFt<28 && ($stone=='Caesarstone' || $stone == 'Staron Radianz')){
				$materialCost = getMaterialCost($mysqli, array(28,0,0), $stone, $color,0,$install);
				$materialCost_ogee = getMaterialCost($mysqli, array(28,0,0), $stone, $color,1,$install);
			}
                   
                        //if($stone =='Collection'){
                        //echo "Material Cose--------".$materialCost;
                        //echo "<br>";
                        //echo "Material Cose Ogee--------".$materialCost_ogee;
                        //echo "<br>";
                        //echo "Option Cost--------".$optionCost;
                        //echo "<br>";
                       // }
                        //echo $materialCost;
                        
                        
                        
			
			$price1 = get_SQ_Eased_Edge($materialCost,
										$materialCost_ogee,
										$optionCost);
				
			$price2 = get_Bevel_Bullnose_Roundover_DBL_Radius($materialCost,
										  					  $materialCost_ogee,
										  					  $edgeCost_39,
										  					  $optionCost);
										  					  
			$price3 = get_Bevel_Bullnose_Roundover_DBL_Radius($materialCost,
										  					  $materialCost_ogee,
										  					  $edgeCost_40,
										  					  $optionCost);

//echo "--". $price1 . "---<br>";

                        if($install){
                          $trip_milage= readSQL($mysqli,"SELECT cost FROM Pricing WHERE id = 46",'cost');
                          if($price1 > 0){
                             $price1 =($price1+$trip_milage[0]);
if(isset($_POST['up_scale']))
	$price1 = $price1 + ($trip_milage[0] * ($_POST['up_scale'] / 100));
                          }
                          if($price2 > 0){ 
                             $price2 =$price2+$trip_milage[0];
if(isset($_POST['up_scale']))
	$price2 = $price2 + ($trip_milage[0] * ($_POST['up_scale'] / 100));
                           }
                          if($price3 > 0){
                             $price3 =$price3+$trip_milage[0];
if(isset($_POST['up_scale']))
	$price3 = $price3 + ($trip_milage[0] * ($_POST['up_scale'] / 100));
                           }
                         }

//echo "---". $price1 . "--<br>";

 
                        // 21-12-2015 /////////////
                          //if(!$install)
                          //$price1 = $price1-100;
                        //////////// ///////////////////
										  					  
			//echo "Price1 - ".var_export($price1,TRUE);
			//echo "Price2 - ".var_export($price2,TRUE);
			//echo "Price3 - ".var_export($price3,TRUE);
			
			
			$table.= '<tr onclick="selectRow(this)">';
			//$table.= '<td><input type="checkbox"></td>';
			$table.= '<td>'.$stone.' '.$color.'</td>';
			$table.= '<td>'.'$ '.number_format($price1,2).'</td>';
			$table.= '<td>'.'$ '.number_format($price2,2).'</td>';
			$table.= '<td>'.'$ '.number_format($price3,2).'</td>';
			$table.= '</tr>';
			
		}

		$table.= '</tbody>';
		$table.= '</table>';
		//$table.= '<hr style="page-break-before:always;" />';
		if($_SERVER['REQUEST_URI']=='/quote-submission/'){
			$table.= '</div>';
		}
		$count++;
		
	}
	
	//echo '<!-- '.$debug_string.' -->';
	
	return $table;
}

function moveToFinal(mysqli $mysqli, array $POST_DATA){
	
	$sql = 'INSERT INTO ORDER_LOG_SUBMIT SELECT * FROM ORDER_LOG WHERE ProjectName = "'.$POST_DATA['ProjectName'].'" AND AccountName = "'.$POST_DATA['Account'].'" AND UpScale = 0';
	executeSQL($mysqli, $sql);
	return TRUE;
	
}

function DisplayOrderForm(mysqli $mysqli, $group, $name=NULL, $value=NULL, $line=NULL){
	
	if(!is_null($name)){
		
		$data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE name = \''.$name.'\'');
		
		if(count($data)!=0){
			foreach($data as $d)
			if($d['type'] == 'text' || $d['type'] == 'email' || $d['type'] == 'tel' || $d['type'] == 'date'){
				echo '<div style="">';
				echo '<label for="'.$d['id'].'">';
				echo $d['label'].' ';
				if($line==0)
				echo '</label>';
				echo '<input id = "'.$d['id'].'" name = "'.$d['name'].'" type = "'.$d['type'].'" style="margin-right: 3px;" '.($d['required']==1?'required':'').' value="'.$value.'">';
				echo '</div>';
				if($line==1)
				echo '</label>';
			}
			
			if($data[0]['type']=='hidden'){
				foreach($data as $hd){
					echo '<input id = "'.$hd['id'].'" name = "'.$hd['name'].'" type = "'.$hd['type'].'" value="'.$value.'"  >';
				}
			}
			
			if($data[0]['type']=='checkbox'){
				foreach($data as $cd){
					echo '<label for="'.$cd['id'].'" style="">';
					echo '<!-- '.$value.' -->';
					echo '<input id = "'.$cd['id'].'" name = "'.$cd['name'].'" type = "'.$cd['type'].'" value="1" '.($value==1?'checked':NULL).' >';
					echo ' '.$cd['label'];
					echo '</label>';
				}
			}
			
			if($data[0]['type']=='textarea'){
				foreach($data as $tad){
					echo '<label for="'.$tad['id'].'" >';
					echo $tad['label'].'<br/>';
					echo '<'.$tad['type'].' id = "'.$tad['id'].'" name = "'.$tad['name'].'" rows="4" style="width: 500px;" >';
					echo $value;
					echo '</'.$tad['type'].'>';
					echo '</label>';
				}
			}
			
			if($data[0]['type']=='radio'){
				$rg = $data[0];
				echo '<div style="">';
				echo $rg['label']."<br/>";
				foreach($data as $rd){
					echo '<label for="'.$rd['id'].'" >';
					echo '<input id = "'.$rd['id'].'" name = "'.$rd['name'].'" type = "'.$rd['type'].'" value="'.$rd['value'].'" '.($rd['required']==1?'required':'').' '.($value==$rd['value']?'checked':NULL).' >';
					echo ' '.$rd['value'];
					echo '</label>';
				}
				echo '</div>';
			}
			
			if($data[0]['type']=='select'){
				
				$select_data = $data[0];
				
				$sql = 'SELECT * FROM `ORDER_FORM_'.$select_data['name'].'`';
				echo '<label for="'.$select_data['id'].'" style="">';
				echo ' '.$select_data['label']." ";
				echo '<select id = "'.$select_data['id'].'" name = "'.$select_data['name'].'" '.($select_data['required']==1?'required':'').'>';
				foreach(readSQL($mysqli, $sql) as $opt){
					echo '<option value="'.$opt['value'].'" '.($value==$opt['value']?'selected':NULL).'>'.$opt['value'].'</option>';
				}	
				echo '</select>';
				echo '</label>';
				
			}
			
		}
		
	}else{
		
		echo "DEAD FUNCTION!\n";
		
		/*$data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE `group` = \''.$group.'\'');
		$checkbox_data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE `group` = \''.$group.'\' AND `type` = \'checkbox\'');
		$radio_group = readSQL($mysqli, 'SELECT name, label FROM `ORDER_FORM` WHERE `group` = \''.$group.'\' AND `type` = \'radio\' GROUP BY `name`');
		$radio_data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE `group` = \''.$group.'\' AND `type` = \'radio\'');
		$textarea_data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE `group` = \''.$group.'\' AND `type` = \'textarea\'');
		$legend = readSQL($mysqli,'SELECT display FROM `ORDER_FORM_GROUP` WHERE `group` = \''.$group.'\' LIMIT 0,1','display');
		$select_data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE `group` = \''.$group.'\' AND `type` = \'select\'');
		
		echo '<field>';
		echo '<legend>'.$legend[0].'</legend>';
		echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
		
		echo '<div style="clear:both;">';
		foreach($data as $d){
			if($d['type'] == 'text' || $d['type'] == 'email' || $d['type'] == 'tel' || $d['type'] == 'date'){
				echo '<div style="float:left; margin-right:10px;">';
				echo '<label for="'.$d['id'].'">';
				echo $d['label'].'</label>';
				echo '<input id = "'.$d['id'].'" name = "'.$d['name'].'" type = "'.$d['type'].'" style="margin-right: 3px;" '.($d['required']==1?'required':'').'>';
				echo '</div>';
			}
		}
		echo '</div>'."\n";
		
		echo '<div style="clear:both;">';
		foreach($checkbox_data as $cd){
			echo '<label for="'.$cd['id'].'" style="float:left; margin-right: 10px">';
			echo '<input id = "'.$cd['id'].'" name = "'.$cd['name'].'" type = "'.$cd['type'].'" value = "1">';
			echo ' '.$cd['label'];
			echo '</label>';
		}
		echo '</div>'."\n";
		
		echo '<div style="clear:both;">';
		foreach($select_data as $sd){
			$sql = 'SELECT * FROM `ORDER_FORM_'.$sd['name'].'`';
			echo '<label for="'.$sd['id'].'" style="">';
			echo ' '.$sd['label']." ";
			echo '<select id = "'.$sd['id'].'" name = "'.$sd['name'].'" '.($sd['required']==1?'required':'').'>';
			foreach(readSQL($mysqli, $sql) as $opt){
				echo '<option value="'.$opt['value'].'">'.$opt['value'].'</option>';
			}
			echo '</select>';
			echo '</label>';
		}
		echo '</div>'."\n";
		
		echo '<div style="clear:both;">';
		foreach($radio_group as $rg){
			$radio_data = readSQL($mysqli, 'SELECT * FROM `ORDER_FORM` WHERE `group` = \''.$group.'\' AND `type` = \'radio\' AND name = \''.$rg['name'].'\'');
			echo '<div style="float:left; padding: 5px;margin-right:10px;background-color: #DDDDDD;">';
			echo $rg['label']."<br/>";
			foreach($radio_data as $rd){
				echo '<label for="'.$rd['id'].'" >';
				echo '<input id = "'.$rd['id'].'" name = "'.$rd['name'].'" type = "'.$rd['type'].'" value="'.$rd['value'].'" '.($rd['required']==1?'required':'').' >';
				echo ' '.$rd['value'];
				echo '</label>';
			}
			echo '</div>';
		}
		echo '</div>'."\n";
		
		echo '<div style="clear:both;">';
		foreach($textarea_data as $tad){
			echo '<label for="'.$tad['id'].'" >';
			echo $tad['label'].'<br/>';
			echo '<'.$tad['type'].' id = "'.$tad['id'].'" name = "'.$tad['name'].'" rows="4" style="width: 500px;">';
			echo '</'.$tad['type'].'>';
			echo '</label>';
		}
		echo '</div>'."\n";
		
		echo '</div>';
		echo '</field>';
		*/
	}
}


?>