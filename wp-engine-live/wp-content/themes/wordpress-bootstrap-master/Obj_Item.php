<?php

class Item {
	
	private $id;
	private $name;
	private $notes;
	private $measurements;
	private $options;
	
	function __construct($id = NULL, $name=NULL, $notes=NULL, Measurement $measurements = NULL, Options $options = NULL) {
		$this->id = $id;
		$this->name = $name;
		$this->notes = $notes;
		$this->measurements = $measurements;
		$this->options = $options;
	}
	
	function importItem(mysqli $mysqli, $id){
		
	}
	
	function exportItem(mysqli $mysqli){
		
	}
	
	
	
	function getId(){return $this->id;}

	function getName(){return $this->name;}
	
	function getNotes(){return $this->notes;}
	
	function getMeasurements(){return $this->measurements;}
	
	function getOptions(){return $this->options;}
	
	function setId($id){$this->id = $id;}
	
	function setName($name){$this->name = $name;}
	
	function setNotes($notes){$this->notes = $notes;}
	
	function setMeasurements(Measurements $measurements){$this->measurements = $measurements;}
	
	function setOptions(Options $options){$this->options = $options;}
	
}

?>