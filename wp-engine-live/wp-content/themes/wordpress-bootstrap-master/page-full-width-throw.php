<?php
/*
Template Name: Full Width Page - Throw
*/
?>
<?php 

require_once("dompdf-master/dompdf_config.inc.php");

require 'PHPMailer/PHPMailerAutoload.php';

ob_start();

date_default_timezone_set('America/Chicago');

include_once 'core-functions.php';
include_once 'config.php';

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');

$stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' GROUP BY name', 'name');

$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'');

if(isset($_GET['ProjectName']))
$ITEMS = getItemList($mysqli, $_GET['ProjectName'], $_GET['AccountName'], $_GET['ContactEmail']);
else
$ITEMS = getItemList($mysqli, $_POST['ProjectName'], $_POST['Account'], $_POST['ContactEmail']);

?>

<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><?php wp_title( '|', true, 'right' ); ?></title>
				
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
		<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php //wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- theme options from options panel -->
		<?php get_wpbs_theme_options(); ?>
		
		<style type="text/css">
		
		body{
			font-size: 10px;
		}
		
		table {
			width: 325px;
		    border-collapse: collapse;
		    border: 1px solid #000;
		}
		td {
			padding: 2px;
		    border: 1px solid #000;
		}
		th {
			padding: 2px;
		    border: 1px solid #000;
		}
		
		.noborder {
			border: none;
		}
		
		</style>
				
	</head>
	<body style="padding-top: 0px; width: 680px;">

			<div id="content" class="clearfix row-fluid" >
			
				<div id="main" class="span12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
						<table style="width: 100%;" class="noborder"><tr>
						<td class="noborder">
						<h1>
						<img src="<?php echo of_get_option('branding_logo'); ?>" alt="<?php echo get_bloginfo('description'); ?>" width="88" height="50" style="vertical-align:middle;">
						<strong><?php echo (empty($_POST['ProjectName'])?$_GET['ProjectName']:$_POST['ProjectName']); ?></strong> Pricing Quote
						</h1>
						</td>
						<td class="noborder"><p style="text-align: right;"><?php echo date('Y-m-d H:i:s');?></p></td>
						</tr></table>
						<hr style="clear: both;"/>
						</header> <!-- end article header -->
						
						<section class="post_content" >
						<table style="width: 100%;" class="noborder"><tr>
						<td class="noborder" style="width: 50%; vertical-align:top;">
						<?php 
								
							//echo '<h3>Carstin Brands Granite/Quartz Collection</h3>';
							//echo '<div>'.COLLECTION_TABLE($mysqli, $ITEMS).'</div>';
                                                        echo STONES_TABLE($mysqli, $ITEMS, 'Collection');
							echo STONES_TABLE($mysqli, $ITEMS, 'Caesarstone');
							echo STONES_TABLE($mysqli, $ITEMS, 'Hanstone'); 
							echo STONES_TABLE($mysqli, $ITEMS, 'SoapStone'); 
							echo STONES_TABLE($mysqli, $ITEMS, 'Viatera');  
							 
						?>
						
						
						</td>

						<td class="noborder" style="width: 50%">
						<?php
							
							$measurements = SQFT_LNFT_DATA($mysqli, $ITEMS,1);
							
							//echo '<div>';
							echo '<strong>'.'Project ID:'.'</strong> W-'.$ITEMS[0]['id'].'<br/>';
							echo '<strong>'.'Project Name:'.'</strong> '.$ITEMS[0]['ProjectName'].'<br/>';
							echo '<strong>'.'Contact Name:'.'</strong> '.$ITEMS[0]['ContactName'].'<br/>';
							echo '<strong>'.'Company Name:'.'</strong> '.$ITEMS[0]['CompanyName'].'<br/>';
							echo '<strong>'.'Contact Phone:'.'</strong> '.$ITEMS[0]['ContactPhone'].'<br/>';
							echo '<strong>'.'Contact Email:'.'</strong> '.$ITEMS[0]['ContactEmail'].'<br/>';
							echo '<strong>'.'Account:'.'</strong> '.$ITEMS[0]['AccountName'].'<br/>'.'<br/>';
							echo SQFT_LNFT_DATA($mysqli, $ITEMS).'';
							
							echo STONES_TABLE($mysqli, $ITEMS, 'Granite');
							echo STONES_TABLE($mysqli, $ITEMS, 'Silestone'); 
							echo STONES_TABLE($mysqli, $ITEMS, 'Staron Radianz'); 
							echo STONES_TABLE($mysqli, $ITEMS, 'Zodiaq');  
							
						?></td>
						</tr></table>
						
						
							<!-- 
							<?php
							
							$measurements = SQFT_LNFT_DATA($mysqli, $ITEMS,1);
							
							echo '<div>';
							echo '<strong>'.'Project ID:'.'</strong> W-'.$ITEMS[0]['id'].'<br/>';
							echo '<strong>'.'Project Name:'.'</strong> '.$ITEMS[0]['ProjectName'].'<br/>';
							echo '<strong>'.'Contact Name:'.'</strong> '.$ITEMS[0]['ContactName'].'<br/>';
							echo '<strong>'.'Company Name:'.'</strong> '.$ITEMS[0]['CompanyName'].'<br/>';
							echo '<strong>'.'Contact Phone:'.'</strong> '.$ITEMS[0]['ContactPhone'].'<br/>';
							echo '<strong>'.'Contact Email:'.'</strong> '.$ITEMS[0]['ContactEmail'].'<br/>';
							echo '<strong>'.'Account:'.'</strong> '.$ITEMS[0]['AccountName'].'<br/>'.'<br/>';
							echo SQFT_LNFT_DATA($mysqli, $ITEMS).'</div>';

							?>
							<div>
								<?php 
								
								if(!empty($measurements[0])){
								echo '<h3>Carstin Brands Granite/Quartz Collection</h3>';
								echo '<div>'.COLLECTION_TABLE($mysqli, $ITEMS).'</div>';
								echo STONES_TABLE($mysqli, $ITEMS); 
								?>
							</div>

							 -->
							<div><hr/>All Quotes are based on information provided by Dealer. Quotes are subject to change after template is completed.</div>
							
							<?php
							
							}else{
								echo '<br/><hr/>';
								echo '<h3 class="text-error">Data was insufficient<br/>Please return to Calculation page and check your measurements.</h3>';	
							}
							
							?>
							
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php //comments_template(); ?>
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->
			
			<?php
			
			
			?>
</body>
</html>
<?php
$fileName = date('YmdHis').'-'.$ITEMS[0]['id'].'-'.$ITEMS[0]['ProjectName'].'-'.$ITEMS[0]['AccountName'].'.pdf';

$html = ob_get_clean();
$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
//$dompdf->stream($fileName);
$file = $dompdf->output();
file_put_contents(get_template_directory().'/QUOTE_SUBMISSIONS/'.$fileName, $file);


$message = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>'.'[Quote Submission] '.$ITEMS[0]['CompanyName'].' - '.$ITEMS[0]['ProjectName'].'</title></head><body>';
$message.= '<strong>'.'Project ID:'.'</strong> W-'.$ITEMS[0]['id'].'<br/>';
$message.= '<strong>'.'Project Name:'.'</strong> '.$ITEMS[0]['ProjectName'].'<br/>';
$message.= '<strong>'.'Contact Name:'.'</strong> '.$ITEMS[0]['ContactName'].'<br/>';
$message.= '<strong>'.'Company Name:'.'</strong> '.$ITEMS[0]['CompanyName'].'<br/>';
$message.= '<strong>'.'Contact Phone:'.'</strong> '.$ITEMS[0]['ContactPhone'].'<br/>';
$message.= '<strong>'.'Contact Email:'.'</strong> '.$ITEMS[0]['ContactEmail'].'<br/>';
$message.= '<strong>'.'Account:'.'</strong> '.$ITEMS[0]['AccountName'].'<br/>'.'<br/>';
$message.= SQFT_LNFT_DATA($mysqli, $ITEMS).''; 
$message.= '</body></html>';

$sql = 'SELECT * FROM Settings WHERE id = 5';
$toEmail = readSQL($mysqli, $sql);
$toEmail = $toEmail[0]['value'];

$email = new PHPMailer;

$email->From      = $toEmail;
$email->FromName  = 'Orders @ CarsinBrands';
$email->Subject   = '[Order Submission] '.$ITEMS[0]['CompanyName'].' - '.$ITEMS[0]['ProjectName'];
$email->Body      = $message;
$email->AddAddress( $toEmail );
$email->AddCC( $ITEMS[0]['ContactEmail'] );
$email->AddCC( 'ryeung@opm.us.com' );

$email->AddAttachment( get_template_directory().'/QUOTE_SUBMISSIONS/'.$fileName);

foreach($ITEMS as $i){
	if(!empty($i['FileName']))
	$email->AddAttachment( get_template_directory().'/UPLOADED_FILES/'.$i['FileName']);
}

if(isset($_POST['ProjectName'])){
	$OrderForm = processUploadedOrderForm();
	
	$email->AddAttachment( get_template_directory().'/UPLOADED_FILES/'.$OrderForm);
}

$email->isHTML(true); 

if($mysqli)
$mysqli->close();

//var_dump($OrderForm);

//if(empty($OrderForm))
//header('Location: http://dealers.carstinbrands.com/receiver/?Title=An Error Has Occured; Nothing was done.');
//else{
	if(!$email->send()) {
	   echo 'Message could not be sent.';
	   echo 'Mailer Error: ' . $email->ErrorInfo;
	   exit;
	}
	header('Location: http://dealers.carstinbrands.com/complete-order/?session='.$_GET['session']);
//}


?>