<?php
/*
Template Name: Full Width Page - Order Form - Backsplash Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
include_once 'config.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							if(isset($_GET['message'])){
								echo '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Notice!</h4>'.$_GET['message'].'</div>';
							}
							
							echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
							
							echo '<form action="/process-post/?next=/options-info/&from=/backsplash-info/&session='.$order_session.'" method="post" role="form">';
							echo '<field>';
							echo '<legend>Backsplash Info</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'BacksplashInfo','Backsplash',(isset($_GET['Backsplash'])?$_GET['Backsplash']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'BacksplashInfo','CustomBacksplash',(isset($_GET['CustomBacksplash'])?$_GET['CustomBacksplash']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'BacksplashInfo','BacksplashOutlets',(isset($_GET['BacksplashOutlets'])?$_GET['BacksplashOutlets']:NULL));
							echo '</div>';
							echo 'If no outlets are required, please enter "0". ';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'BacksplashInfo','BacksplashNotes',(isset($_GET['BacksplashNotes'])?$_GET['BacksplashNotes']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Options</button>';
							echo '</field>';
							echo '</form>';
							
							echo '</div>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>