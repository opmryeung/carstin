<?php
/*
 Template Name: Full Width Page - Calculate
 */
?>
<?php

if($_GET['a']==1 && empty($_GET['n']))
header('Location: http://dealers.carstindev.onlinepositioning.com/login/');

// echo 'Login page.';
// exit;
include_once 'core-functions.php';
include_once 'config.php';

//phpinfo();

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');

$shapeList = readSQL($mysqli, 'SELECT * FROM Shape');

$options = readSQL($mysqli,'SELECT type FROM Pricing WHERE hide != 1 AND type IS NOT NULL GROUP BY type ORDER BY id','type');

$optionSize = count($options);

$options_DATA = array();

foreach ($options as $opt){
	$array = readSQL($mysqli,"SELECT * FROM Pricing WHERE hide != 1 AND type = '$opt'");
	array_push($options_DATA, $array);
}

$edgeList = readSQL($mysqli, 'SELECT * FROM Pricing WHERE Type="edge"');

echo '<!-- ';
var_dump($_POST);
echo ' -->';

?>
<?php get_header(); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/JavaScript">

/*$(document).ready(function(){
	$('input').keypress(function(e) {
		var regex = new RegExp("^[a-zA-Z0-9 ]+$");
		var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (!regex.test(key)) {
			e.preventDefault ? e.preventDefault() : e.returnValue = false;
			console.log(key);
		}
	});
});*/

$(function()  {
	$('#shapeselector').change(function(){
		$('.SHAPE_CHOICE').hide();
		$(this).val().replace(/\s+/g, ' ');
		var name = $(this).val().split(' ').join('');
		$('#' + name).show();
    });
});

$(document).ready(function(){
    $('#debug').change(function(){
        if(this.checked)
        	$('#website').show();
        else
        	$('#website').hide();

    });
});

</script>
<script>
function validateForm()
{
var x=document.forms["mainForm"]["ProjectName"].value;
if (x==null || x=="")
  {
  alert("Project Name must be filled out");
  return false;
  }
}
</script>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span12 clearfix" role="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>
			role="article"> <header>

		<div class="page-header">
			<h1>
			<?php the_title(); ?>
			</h1>
		</div>

		</header> <!-- end article header --> <section class="post_content"> <?php //the_content(); ?>
		<?php 
		
		if($_GET['a']=='1'){
			$items = getItemList($mysqli, $_GET['p'], $_GET['acc'], $_GET['ce']);
			if($items){
				echo '<h4>Objects under <strong>'.$_GET['p'].'</strong>:</h4>';
				printItemList($items);
			}
		}
		
		?>		
		<form action="/results/" name="mainForm" method="post" class="form-horizontal" role="form" enctype="multipart/form-data" onsubmit="return validateForm()" >
		<!-- <div class="form-inline">
		<input id="debug" name="debug" type="checkbox" <?php echo ($_SERVER['REMOTE_ADDR']=='216.138.113.106')?('checked'):(''); ?>> <label for="debug" class="checkbox">Debug Mode</label>
		</div> -->
		<fieldset class="form-inline">
			<legend>Basic Contact Info</legend>
			
			<?php if($_GET['a']!='1'){ ?>
			<input name="Account" type="hidden" value="<?php echo $_POST['inputAcc']; ?>">
			
			<label for="ProjectName">Project Name</label>
			<input id="ProjectName" name="ProjectName" type="text" onkeyup='this.value=this.value.replace(/[^ \.A-Z0-9_-]/ig,"")' required autofocus>
			
			<label for="ContactName">Contact Name</label>
			<input id="ContactName" name="ContactName" type="text" value="<?php echo str_replace("\'", "", $_POST['inputName']); ?>" required>
			
			<label for="CompanyName">Company Name</label>
			<input id="CompanyName" name="CompanyName" type="text" value="<?php echo str_replace("\'", "", $_POST['inputCompany']); ?>" required><br/>
			
			<label for="ContactPhone">Contact Phone</label>
			<input id="ContactPhone" name="ContactPhone" type="tel" value="<?php echo $_POST['inputPhone']; ?>" required>
			
			<label for="ContactEmail">Contact Email</label>
			<input id="ContactEmail" name="ContactEmail" type="email" required value="<?php echo ($_SERVER['REMOTE_ADDR']=='216.138.113.106' && empty($_POST['inputEmail']))?('ryeung@opm.us.com'):($_POST['inputEmail']); ?>" >
			
			<input id="Salesperson" name="Salesperson" type="hidden" value="<?php echo $_POST['inputSales']; ?>">
			
			<?php }else{ ?>
			
			<input name="Account" type="hidden" value="<?php echo $_GET['acc']; ?>">
			
			<label for="ProjectName">Project Name</label>
			<input id="ProjectName" name="ProjectName" type="text" value="<?php echo $_GET['p']; ?>" readonly>
			
			<label for="ContactName">Contact Name</label>
			<input id="ContactName" name="ContactName" type="text" value="<?php echo $_GET['n']; ?>" readonly>
			
			<label for="CompanyName">Company Name</label>
			<input id="CompanyName" name="CompanyName" type="text" value="<?php echo $_GET['c']; ?>" readonly>
			
			<label for="ContactPhone">Contact Phone</label>
			<input id="ContactPhone" name="ContactPhone" type="tel" value="<?php echo $_GET['cp']; ?>" readonly>
			
			<label for="ContactEmail">Contact Email</label>
			<input id="ContactEmail" name="ContactEmail" type="email" value="<?php echo $_GET['ce']; ?>" readonly>
			
			<input id="salesperson" name="salesperson" type="hidden" value="<?php echo $_GET['sp']; ?>" >
			<?php } ?>
		</fieldset>
		<hr/>
		<fieldset>
			<legend>Shape</legend>
			<select id="shapeselector" name="shape" class="form-control">
				<option disabled selected value="0">Select a shape...</option>
				<?php 
				foreach ($shapeList as $shape)
				echo '<option value="'.implode('_',explode(' ',$shape['shape'])).'">'.$shape['shape'].'</option>';
				?>
			</select>
			<input type="text" name="Comment" placeholder="Notes..." class="span8">
		</fieldset>
		<hr/>
		<?php 
		foreach ($shapeList as $shape){
			
			if($shape['shape']=='Custom'){
				echo '<fieldset id="'.str_replace(' ','_',$shape['shape']).'" class="SHAPE_CHOICE" style="display:none">';
				echo '<legend>'.$shape['shape'].'</legend>';
				echo '<div class="row-fluid">';
				echo '<div class="span3">';
				echo '<img src="'.get_bloginfo('template_url').'/images/'.str_replace(' ','_',$shape['shape']).'.png" alt="A image of the table should be shown here; please report to ryeung@opm.us.com if you see this message; Thanks">';
				echo '</div>';
				echo '<div class="span9">';
				$range = range('A','Z');
				for($i=0; $i<$shape['edges']; $i++){
					echo 'Total Measurements: <br/>';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][Length]" placeholder="Area (Sq.Ft.)" type="text" class="span4">';
					echo '<br/>';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][Backsplash-Length]" placeholder="Backsplash Length (Ln.Ft.)" type="text" class="span4">';
					echo '<br/>';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][Backsplash-Height]" placeholder="Backsplash Height (in.)" type="text" class="span4">';
					echo '<br/>';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][LF-Edge]" placeholder="Edge Length (Ln.Ft.)" type="text" class="span4">';
					echo '<br/>';
					echo '<select id="edgeselector" name="'.$shape['shape'].'['.$range[$i].'][Edge-Grp]" class="form-control span4">';
					echo '<option disabled selected>Edge Group</option>';
					foreach ($edgeList as $edge)
					echo '<option value="'.$edge['id'].'">'.$edge['description'].'</option>';
					echo '<option value="0">NONE or Eased Edges</option>';
					echo '</select>';
					echo '<div class="form-inline">';
					//echo $range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].' - Radius, Clip Corners & Angles: ';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-0" value="0" checked>NONE</label><br/></div>';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-1" value="23" > up to 18"</label><br/></div>';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-2" value="24" > 18"-36"</label><br/></div>';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-3" value="25" > 36"-54"</label><br/></div>';
					echo '</div>';
				}
				echo '</div>';
				echo '</div>';
				echo '<hr/>';
				echo '</fieldset>';
			}else{
				echo '<fieldset id="'.str_replace(' ','_',$shape['shape']).'" class="SHAPE_CHOICE" style="display:none">';
				echo '<legend>'.$shape['shape'].'</legend>';
				echo '<div class="row-fluid">';
				echo '<div class="span3">';
				echo '<img src="'.get_bloginfo('template_url').'/images/'.str_replace(' ','_',$shape['shape']).'.png" alt="A image of the table should be shown here; please report to ryeung@opm.us.com if you see this message; Thanks">';
				echo '</div>';
				echo '<div class="span9">';
				$range = range('A','Z');
				for($i=0; $i<$shape['edges']; $i++){
					echo $range[$i].': ';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][Length]" placeholder="Length" type="text" class="span2">';
					//echo ' : ';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][Backsplash-Length]" placeholder="Backsplash (Length)" type="text" class="span3">';
					//echo ' : ';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][Backsplash-Height]" placeholder="Backsplash (Height)" type="text" class="span3">';
					//echo ' : ';
					echo '<input name="'.$shape['shape'].'['.$range[$i].'][LF-Edge]" placeholder="Edge (Length)" type="text" class="span2">';
					//echo ' : ';
					echo '<select id="edgeselector" name="'.$shape['shape'].'['.$range[$i].'][Edge-Grp]" class="form-control span2">';
					echo '<option disabled selected>Edge Group</option>';
					foreach ($edgeList as $edge)
					echo '<option value="'.$edge['id'].'">'.$edge['description'].'</option>';
					echo '<option value="0">NONE or Eased Edges</option>';
					echo '</select>';
					echo '<div class="form-inline">';
					//echo $range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].' - Radius, Clip Corners & Angles: ';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-0" value="0" checked>NONE</label><br/></div>';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-1" value="23" > up to 18"</label><br/></div>';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-2" value="24" > 18"-36"</label><br/></div>';
					//echo '<div class="radio inline"><label><input type="radio" name="'.$shape['shape'].'[Corner]['.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].']" id="'.$shape['shape'].'-'.$range[$i].$range[($i+1==$shape['edges']?'0':$i+1)].'-3" value="25" > 36"-54"</label><br/></div>';
					echo '</div>';
				}
				echo '</div>';
				echo '</div>';
				echo '<hr/>';
				echo '</fieldset>';
			}
		}
		// echo '<pre>';
		// print_r($options_DATA);
		// echo '</pre>';
		// exit;
		for($i=0; $i<$optionSize; $i++){
			echo '<fieldset>';
			echo "<legend>$options[$i]</legend>";
			foreach($options_DATA[$i] as $option){
			        
				echo '<div class="form-inline">';
				if($option['id']=='46'){
					if($_GET['d']!='')
					echo '<input id="option-'.$option['id'].'" name="option['.$option['id'].']" type="hidden" class="span1" disabled value="'.$_GET['d'].'"> ';	//46 = delivery; it's required
					else
					echo '<input id="option-'.$option['id'].'" name="option['.$option['id'].']" type="hidden" class="span1" value="1"> ';	//46 = delivery; it's required
				}elseif($option['id']=='48'){
					if(isset($_GET['i'])){
						echo '<label class="radio">';
						echo '<input type="radio" name="option['.$option['id'].']" id="option-'.$option['id'].'1" value="0"  '.($_GET['i']=='0'?'checked':'disabled').'>';
						echo 'Yes';
						echo '</label> ';
						echo '<label class="radio">';
						echo '<input type="radio" name="option['.$option['id'].']" id="option-'.$option['id'].'2" value="1"  '.($_GET['i']=='1'?'checked':'disabled').'>';
						echo 'No';
						echo '</label>';
					}else{
						echo '<label class="radio">';
						echo '<input type="radio" name="option['.$option['id'].']" id="option-'.$option['id'].'1" value="0" checked>';
						echo 'Yes';
						echo '</label> ';
						echo '<label class="radio">';
						echo '<input type="radio" name="option['.$option['id'].']" id="option-'.$option['id'].'2" value="1">';
						echo 'No';
						echo '</label>';	
					}
				}else{
					echo '<input id="option-'.$option['id'].'" name="option['.$option['id'].']" placeholder="'.$option['unit'].'" type="text" class="span1" > ';	//46 = delivery; it's required
				}
				if($option['id']=='46'){
				echo 'Mileage Included';
				}else{
                                  if($option['id']!='48'){
				    echo '<label for="option-'.$option['id'].'">'.$option['description'].'</label><br/>';
                                   }
				}
				echo '</div>';
			}
			echo '</fieldset><hr/>';
		}
		?>
		<fieldset>
		<legend>Upload Document</legend>
		<p>Document should be an image or sketch of the project</p>
		<input type="file" name="FileUpload">
		</fieldset>
		<input type="submit" class="btn btn-primary" name="CalculationSubmit" value="Submit">
		<?php 
		if($_GET['a']=='1' && $items)
		echo '<input type="submit" class="btn btn-info" name="DiscardSubmit" value="Discard this Object and Submit">';
		?>
		<input type="submit" class="btn btn-inverse" name="StoreSubmit" value="Add another countertop">
<!-- 
  ______          _            
 |  ____|        | |           
 | |__   __ _ ___| |_ ___ _ __ 
 |  __| / _` / __| __/ _ \ '__|
 | |___| (_| \__ \ ||  __/ |   
 |______\__,_|___/\__\___|_|   -->

		<a id="website" href="#myModal" role="button" class="btn btn-warning" data-toggle="modal" style="display:none">Recompute Base Encryption Key Hash</a>
		
		<!-- Modal1 -->
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    <h3 id="myModalLabel" class="text-center">CONFIRM CHANGE</h3>
		  </div>
		  <div class="modal-body">
		    <p class="text-center">Confirm you wish to make the following change:<br/>
		   	CHANGE_RECOMPUTE_BASE_ENCRYPTION_KEY_HASH<br/>
		   	Note: This requires all authentication tokens based<br/>
		   	on this Hash Key to be reset. Which will<br/>
		   	probably take all fXXking week. Have fun.</p>
		  </div>		  
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		    <a href="#myModal2" role="button" class="btn btn-danger" data-toggle="modal" >Confirm</a>
		  </div>
		</div>
		
		<!-- Modal2 -->
		<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true" style="cursor: progress;">
		  <div class="modal-header">
		    <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
		    <h3 id="myModal2Label" class="text-center">RECOMPUTING HASH<br/>DO NOT CLICK ANYTHING</h3>
		  </div>
		  <div class="modal-body">
		    <p class="text-center">
		    <img src="http://dealers.carstinbrands.com/wp-content/uploads/2013/09/UrbanDynamics_001700_27.gif">
		    This may take forever.
		  Do not interrupt the hash recompute or you may be required to re-license all your products.
		  Don't believe us? Try it.</p>
		  </div>  
		  <div class="modal-footer">
		    <button class="btn disabled" aria-hidden="true" style="cursor: progress;" disabled="disabled">Close</button>
		  </div>
		</div>
		
<!-- 
  ______            
 |  ____|           
 | |__   __ _  __ _ 
 |  __| / _` |/ _` |
 | |___| (_| | (_| |
 |______\__, |\__, |
         __/ | __/ |
        |___/ |___/ -->
		
		</form>

		</section> <!-- end article section --> <footer>

		<p class="clearfix">
		<?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?>
		</p>

		</footer> <!-- end article footer --> </article>
		<!-- end article -->

		<?php //comments_template(); ?>

		<?php endwhile; ?>

		<?php else : ?>

		<article id="post-not-found"> <header>
		<h1>
		<?php _e("Not Found", "bonestheme"); ?>
		</h1>
		</header> <section class="post_content">
		<p>
		<?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?>
		</p>
		</section> <footer> </footer> </article>

		<?php endif; ?>

	</div>
	<!-- end #main -->

	<?php //get_sidebar(); // sidebar 1 ?>

</div>

<?php 
if($mysqli)
$mysqli->close();
?>

<!-- end #content -->

	<?php get_footer(); ?>