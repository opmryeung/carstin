<?php
/*
Template Name: Full Width Page - Order Form - Material & Color
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
						
						
							<?php 
							if(isset($_GET['message'])){
								echo '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Notification!</h4>'.$_GET['message'].'</div>';
								//echo '<pre>';
								//var_dump(isset($_GET['OtherThickness'])?$_GET['OtherThickness']:NULL);
								//echo '</pre>';
							}
							
							echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
							echo '<form action="/process-post/?next=/edge-profile/&from=/material-color/&session='.$order_session.'" method="post" role="form">';
							
							echo '<field>';
							echo '<legend>Material & Color</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','RoomID',(isset($_GET['RoomID'])?$_GET['RoomID']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;display:none;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','Slab',(isset($_GET['Slab'])?$_GET['Slab']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;display:none;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','SlabLocation',(isset($_GET['SlabLocation'])?$_GET['SlabLocation']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;display:none;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','SlabDate',(isset($_GET['SlabDate'])?$_GET['SlabDate']:date('Y-m-d')));
							echo '</div>';
							//echo '<hr style="clear:both;">';
							if(isset($_GET['message']) && !isset($_GET['add'])){
								echo '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button>Please re-select material and color</div>';
							}
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','MaterialType',(isset($_GET['MaterialType'])?$_GET['MaterialType']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							//DisplayOrderForm($mysqli, 'MaterialAndColor','Color',(isset($_GET['Color'])?$_GET['Color']:NULL));
							echo '<label for="Color" style="">';
							echo ' Color ';
							echo '<select id="Color" name="Color"><option value="">Select Material Type First</option></select>';
							echo '</label>';
							echo '</div>';
							echo '<p style="clear:both;">If your desired color is not mentioned, please specify color in the Additional Material & Color Notes section below.</p>';
							echo '<hr style="clear:both;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','Thickness',(isset($_GET['Thickness'])?$_GET['Thickness']:NULL));
							DisplayOrderForm($mysqli, 'MaterialAndColor','OtherThickness',(isset($_GET['OtherThickness'])?$_GET['OtherThickness']:NULL));
							echo '<hr style="clear:both;">';
							DisplayOrderForm($mysqli, 'MaterialAndColor','MaterialNotes',(isset($_GET['MaterialNotes'])?$_GET['MaterialNotes']:NULL));
							echo 'If a Slab Selection is desired, please contact Carstin\'s customer service number at 1-800-765-9877.<br/>';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Edge Profile</button>';
							
							echo '</field>';
							echo '</form>';
							echo '</div>';
							
							?>
							
							<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
							<script type="text/javascript">   
							$('#MaterialType').change(createSelect2);
	
							function createSelect2(){
							    var option = $(this).find(':selected').val(),
							    dataString = "option="+option;
							    if(option != ''){
							    	console.log(option + " Selected");    
							        $.ajax({
							            type     : 'GET',
							            url      : 'http://dealers.carstinbrands.com/material-color-selector/',
							            data     : dataString,
							            dataType : 'JSON',
							            cache: false,
							            success  : function(data) {        
							                var output = '<option value="">('+option+')</option>';
	
							                $.each(data.data, function(i,s){
							                    var newOption = s;
	
							                    output += '<option value="' + newOption + '">' + newOption + '</option>';
							                });
	
							                $('#Color').empty().append(output);
							            },
							            error: function(){
							                console.log("Ajax failed");
							            }
							        }); 
							    }
							    else
							    {
							        console.log("You have to select at least sth");
							    }
							}
							
							
							</script>
							
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>