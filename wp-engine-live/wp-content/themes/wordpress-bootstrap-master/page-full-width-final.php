<?php
/*
Template Name: Full Width Page - Final
*/
?>
<?php 

include_once 'core-functions.php';

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';

$stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' GROUP BY name', 'name');

$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'');

$ITEMS = getItemList($mysqli, $_POST['ProjectName'], $_POST['Account'], $_POST['ContactEmail']);

?>

<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><?php wp_title( '|', true, 'right' ); ?></title>
				
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
		<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php //wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- theme options from options panel -->
		<?php get_wpbs_theme_options(); ?>
		
		<script>
		window.onload = function() { window.print(); }
		</script>
		
		<style>
		
			body{
			font-size: 10px;
			}
			
			.no-print, .no-print *
			{
				display: none !important;
			}
			
			table {
				width: 325px;
			    border-collapse: collapse;
			    border: 1px solid #000;
			}
			td {
				padding: 2px;
			    border: 1px solid #000;
			}
			th {
				padding: 2px;
			    border: 1px solid #000;
			}
			
		
		</style>
				
	</head>
	<body style="padding-top: 0px; width: 680px;">

			<div id="content" class="clearfix row-fluid" >
			
				<div id="main" class="span12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							<div style="float: right;"><?php echo date('Y-m-d H:i:s');?></div>
							<img src="<?php echo of_get_option('branding_logo'); ?>" alt="<?php echo get_bloginfo('description'); ?>" width="88" height="50" style="float: left; padding-right: 10px;">
							<div style="padding-top: 5px;"><h1><strong><?php echo $_POST['ProjectName']; ?></strong> Pricing Quote</h1></div>
							<hr style="clear: both;"/>
						
						</header> <!-- end article header -->
						
						<section class="post_content" >
						
							<?php
							
							$measurements = SQFT_LNFT_DATA($mysqli, $ITEMS,1);
							
							//if($ITEMS && !empty($measurements[0])){
							//	echo '<div style="float: left;">';
							//	printItemList($ITEMS);
							//	echo '</div>';
							//}
							//echo '<pre>'.var_export($ITEMS[0], TRUE).'</pre>';
							echo '<div style="float: right; margin-top: 0px;">';
							echo '<strong>'.'Project ID:'.'</strong> W-'.$ITEMS[0]['id'].'<br/>';
							echo '<strong>'.'Project Name:'.'</strong> '.$ITEMS[0]['ProjectName'].'<br/>';
							echo '<strong>'.'Contact Name:'.'</strong> '.$ITEMS[0]['ContactName'].'<br/>';
							echo '<strong>'.'Company Name:'.'</strong> '.$ITEMS[0]['CompanyName'].'<br/>';
							echo '<strong>'.'Contact Phone:'.'</strong> '.$ITEMS[0]['ContactPhone'].'<br/>';
							echo '<strong>'.'Contact Email:'.'</strong> '.$ITEMS[0]['ContactEmail'].'<br/>';
							echo '<strong>'.'Account:'.'</strong> '.$ITEMS[0]['AccountName'].'<br/>';
							echo '<strong>'.'Salesperson:'.'</strong> '.$ITEMS[0]['Salesperson'].'<br/>'.'<br/>';
							echo SQFT_LNFT_DATA($mysqli, $ITEMS).'</div>';

							?>
							<div>
								<?php 
								
								if(!empty($measurements[0])){
								//echo '<h3>Carstin Brands Granite/Quartz Collection</h3>';
								//echo '<div>'.COLLECTION_TABLE($mysqli, $ITEMS).'</div>';
	
								?>
							
								<?php
								echo STONES_TABLE($mysqli, $ITEMS); 
								?>
								</div>
								<?php //pdf24Plugin_link('PDF Version'); ?>
							
							<div style="float: left;"><hr/>All Quotes are based on information provided by Dealer. Quotes are subject to change after template is completed. <br/>Dealers agree not to share any pricing information with our competitors.</div>
							
							<?php
							
							}else{
								echo '<br/><hr/>';
								echo '<h3 class="text-error">Data was insufficient<br/>Please return to Calculation page and check your measurements.</h3>';	
							}
							
							?>
							
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php //comments_template(); ?>
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->
			
			<?php
			
			if($mysqli)
			$mysqli->close();
			
			?>
</body>
</html>