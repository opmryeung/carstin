<?php
/*
Template Name: Full Width Page - Order Form - Dealer Info
*/
?>
<?php 

/*session_start();
if(isset($_SESSION['ORDER']))
$order_session = $_SESSION['ORDER'];
else*/
//$order_session = $_SESSION['ORDER'] = date('YmdHis');

$order_session = date('YmdHis');

include_once 'core-functions.php';

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
if(isset($_GET['ProjectName']))
$quoteDetails = getItemList($mysqli, $_GET['ProjectName'], $_GET['AccountName'], $_GET['ContactEmail']);
else
$quoteDetails = getItemList($mysqli, $_POST['ProjectName'], $_POST['Account'], $_POST['ContactEmail']);

/*echo '<pre>';
var_dump($quoteDetails);
echo '</pre>';*/

?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							//echo '<pre>';
							//var_dump($quoteDetails[0]['id']);
							//echo '</pre>';
							
							$account = readSQL($mysqli,'SELECT DEALER,REP FROM `DEALER_REPS` WHERE DEALER LIKE (SELECT Name FROM DEALERS WHERE `Ship To Number` = \''.$_POST['Account'].'\' OR `Ship To Number` = \''.$_GET['AccountName'].'\')');
							$account = $account[0];
							
							echo '<form action="/process-post/?next=/customer-info/&from=/dealer-info/&session='.$order_session.'" method="post"  role="form">';
							echo '<field>';
							echo '<legend>Dealer Info</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','OrderDate',date('Y-m-d'));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','DealerName',$account['DEALER']);
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','DealerContact',$account['REP']);
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','DealerEmail');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','DealerPhone');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','DealerFax');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'DealerInfo','DealerPO');
							echo '</div>';
							echo '<input type="hidden" name="QuoteID" value="W-'.$quoteDetails[0]['id'].'">';
							echo '<hr style="clear:both;">';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Customer Info</button>';
							echo '</field>';
							echo '</form>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>