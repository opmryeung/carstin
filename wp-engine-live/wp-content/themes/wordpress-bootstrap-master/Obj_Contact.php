<?php

class Contact {

	private $id;
	private $account;
	private $company;
	private $name;
	private $email;
	private $phone;
	private $address;
	
	private $mysqli;

	function __construct(mysqli $mysqli, $id) {
		
		$sql = "SELECT * FROM DEALERS WHERE id = $id";
		$data = readSQL($mysqli, $sql);
		
		if(empty($data)){
			echo 'USER ID NOT FOUND!';
			return FALSE;
		}else{
			$this->id = $data[0]['id'];
			$this->account = $data[0]['Ship To Number'];
			$this->name = $data[0]['Primary Customer Contact'];
			$this->company = $data[0]['Name'];
			$this->email = $data[0]['Primary email'];
			$this->phone = $data[0]['Phone Number'];
			$this->address = $data[0]['Address Line 1'].'\n'.$data[0]['Address Line 2'].'\n'.$data[0]['City'].'\n'.$data[0]['State'].'\n'.$data[0]['Zip Code'];
			return TRUE;
		}
	}
	
	function importContact(mysqli $mysqli, $id){
		
	}
	
	function exportContact(mysqli $mysqli){
		
	}

	function getId(){return $this->id;}

	function getAccount(){return $this->account;}

	function getName(){return $this->name;}
	
	function getCompany(){return $this->company;}

	function getEmail(){return $this->email;}

	function getPhone(){return $this->phone;}

	function getAddress(){return $this->address;}

	function setId($id){$this->id = $id;}

	function setAccount($account){$this->account = $account;}

	function setName($name){$this->name = $name;}
	
	function setCompany($company){$this->company = $company;}
	
	function setEmail($email){$this->email = $email;}
	
	function setPhone($phone){$this->phone = $phone;}
	
	function setAddress($address){$this->address = $address;}
	
}

?>