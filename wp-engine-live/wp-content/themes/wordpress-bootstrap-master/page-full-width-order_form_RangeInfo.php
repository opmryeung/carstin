<?php
/*
Template Name: Full Width Page - Order Form - Range Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							if(isset($_GET['message'])){
								echo '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Notification!</h4>'.$_GET['message'].'</div>';
							}
							
							echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
							
							echo '<form action="/process-post/?next=/backsplash-info/&from=/range-info/&session='.$order_session.'" method="post" role="form">';
							echo '<field>';
							echo '<legend>Range Info</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'RangeInfo','Range',(isset($_GET['Range'])?$_GET['Range']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'RangeInfo','RangeStyle',(isset($_GET['RangeStyle'])?$_GET['RangeStyle']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'RangeInfo','RangeMake',(isset($_GET['RangeMake'])?$_GET['RangeMake']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'RangeInfo','RangeModel',(isset($_GET['RangeModel'])?$_GET['RangeModel']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'RangeInfo','RangeNotes',(isset($_GET['RangeNotes'])?$_GET['RangeNotes']:NULL));
							echo '</div>';
							echo '<p style="clear:both;"><strong><em>**Note - Templates must be provided for cooktops and slide-in ranges.  Cooktop and/or range must be at the job site at time of template.**</em></strong></p>';
							echo '<hr style="clear:both;">';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Backsplash Info</button>';
							echo '</field>';
							echo '</form>';
							
							echo '</div>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>