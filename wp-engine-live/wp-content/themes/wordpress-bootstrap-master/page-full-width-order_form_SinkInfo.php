<?php
/*
Template Name: Full Width Page - Order Form - Sink Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							if(isset($_GET['message'])){
								echo '<div class="alert alert-block"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Notification!</h4>'.$_GET['message'].'</div>';
							}
							
							echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
							
							echo '<form action="/process-post/?next=/range-info/&from=/sink-info/&session='.$order_session.'&insert='.$_GET['insert'].'" method="post" role="form">';
							echo '<field>';
							echo '<legend>Sink Info</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','Sink',(isset($_GET['Sink'])?$_GET['Sink']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','SinkMaterial',(isset($_GET['SinkMaterial'])?$_GET['SinkMaterial']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','OtherSinkMaterial',(isset($_GET['OtherSinkMaterial'])?$_GET['OtherSinkMaterial']:NULL),1);
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','SinkType',(isset($_GET['SinkType'])?$_GET['SinkType']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','OtherSinkType',(isset($_GET['OtherSinkType'])?$_GET['OtherSinkType']:NULL),1);
							echo '</div>';
							//echo '<p style="clear:both;"><strong><em>**Note - All Undermount sinks come standard with an overhang mount, unless the manufacture recommends another mounting type.**</em></strong></p>';
							echo '<p style="clear:both;"><strong><em>**Note - Sinks come standard with Overhang Mount. If you would like to Upgrade to a different mounting edge for an additional cost, please specify in Additional Sink notes section.**</em></strong></p>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','SinkMakeModel',(isset($_GET['SinkMakeModel'])?$_GET['SinkMakeModel']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','OtherSinkMakeModel',(isset($_GET['OtherSinkMakeModel'])?$_GET['OtherSinkMakeModel']:NULL),1);
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','CarstinSink',(isset($_GET['CarstinSink'])?$_GET['CarstinSink']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','SinkBaseCabinetWidth',(isset($_GET['SinkBaseCabinetWidth'])?$_GET['SinkBaseCabinetWidth']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','SinkFaucetHoles',(isset($_GET['SinkFaucetHoles'])?$_GET['SinkFaucetHoles']:NULL));
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','SinkNotes',(isset($_GET['SinkNotes'])?$_GET['SinkNotes']:NULL));
							echo '</div>';
							echo '<p style="clear:both;"><strong><em>**Note - Templates must be provided for all sinks, unless they are provided by Carstin Brands, Inc.  All sinks and faucets must be at the job site at time of template.**</em></strong></p>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','AdditionalSink',(isset($_GET['AdditionalSink'])?$_GET['AdditionalSink']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'SinkInfo','AdditionalSinkLocation',(isset($_GET['AdditionalSinkLocation'])?$_GET['AdditionalSinkLocation']:NULL),1);
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Range Info</button>';
							echo '</field>';
							echo '</form>';
							
							echo '</div>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>