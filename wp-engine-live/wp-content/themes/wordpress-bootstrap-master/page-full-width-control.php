<?php
session_start();
/*
Template Name: Full Width Page - Control
*/
?>
<?php 
// error_reporting(E_ALL);
// ini_set('display_errors','On');

include_once 'core-functions.php';
include_once 'config.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');

?>

<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section class="post_content">
						
						<?php 
						
						$dataArray = $_POST;
						
						if(!empty($_POST['UpdateData'])){
							echo '<pre>';
							updatePricing($mysqli, $dataArray);
							echo '</pre>';
						}
						
						if(!empty($_POST['UpdateColor'])){
							echo '<pre>';
							if ($_FILES["file"]["error"] > 0){
								echo "Error: " . $_FILES["file"]["error"] . "\n";
							}else{
								echo "Upload: " . $_FILES["file"]["name"] . "\n";
								echo "Type: " . $_FILES["file"]["type"] . "\n";
								echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB\n";
								echo "Stored in: " . get_template_directory()."/ColorGraph/" . $_FILES["file"]["name"]."\n";
								
								if(move_uploaded_file($_FILES["file"]["tmp_name"],get_template_directory()."/ColorGraph/" . $_FILES["file"]["name"])){
									echo 'Color Graph Update Successful'."\n";
									if(file_put_contents(get_template_directory().'/ColorFile.ini', $_FILES['file']['name']))
									echo 'Updated '.get_template_directory().'/ColorFile.ini'."\n";
								}else{
									echo 'AN ERROR HAS OCCURED!';
								}
							}
							echo '</pre>';
						}
						
						
						
						?>
						
						<div class="tabbable">
						
							<?php 
							
							$getData = (!empty($_GET['tab'])) ? $_GET['tab'] : '';
							
							echo '<ul class="nav nav-tabs">';
							echo '<li '.($getData==''?('class="active"'):('')).'><a href="#tab1" aria-controls="tab1" "data-toggle="tab">Order List</a></li>';
							echo '<li '.($getData=='Price'?('class="active"'):('')).'><a href="#tab2" aria-controls="tab2" data-toggle="tab">Price (Stone)</a></li>';
							echo '<li '.($getData=='StoneOption'?('class="active"'):('')).'><a href="#tab3" aria-controls="tab3" data-toggle="tab">Price (Stone Options)</a></li>';
							/*echo '<li '.($getData=='CollectionOption'?('class="active"'):('')).'><a href="#tab4" aria-controls="tab4" data-toggle="tab">Price (Collection Options)</a></li>';*/
							echo '<li '.($getData=='Other'?('class="active"'):('')).'><a href="#tab5" aria-controls="tab5" data-toggle="tab">Other</a></li>';
							echo '<li '.($getData=='Dealers' || isset($_GET['search']) ?('class="active"'):('')).'><a href="#tab6" data-toggle="tab">Dealers</a></li>';
							echo '<li '.($getData=='ColorGraph'?('class="active"'):('')).'><a href="#tab7" aria-controls="tab7" data-toggle="tab">Color Graph</a></li>';
							echo '<li '.($getData=='OrderExcel'?('class="active"'):('')).'><a href="#tab8" aria-controls="tab8" data-toggle="tab">Order Excel File</a></li>';
							echo '</ul>';
							
							?>
							<!--<div id="preloader">
								<div id="status">&nbsp;</div>
							</div>-->
							<div class="tab-content">
								<div class="tab-pane <?php echo ($getData==''?('active'):(''));?>" id="tab1">					
								<?php 
								$adminUsername = (!empty($_POST['adminUsername']) ? $_POST['adminUsername'] : '');
								$adminPassword = (!empty($_POST['adminPassword']) ? $_POST['adminPassword'] : '');
								
								$inputLog = date('Y-m-d H:i:s')."\t".$_SERVER['REMOTE_ADDR']."\t".$adminUsername.' : '.$adminPassword."";
								$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
								
								if($submit=='1')
									file_put_contents(get_template_directory().'/LOGIN_LOG/admin.log',$inputLog."\n",FILE_APPEND);
									
								// if($submit=='1'){
					
									// file_put_contents(get_template_directory().'/LOGIN_LOG/admin.log',$inputLog."\n",FILE_APPEND);
									
									// $_SESSION['CARSTIN_ADMIN_LOGIN'] = 'ok';
								// }else{
									
									// $_SESSION['CARSTIN_ADMIN_LOGIN'] = '';
								// }
								
						if(($adminUsername==='admin' && $adminPassword==='Worldcontrol' && 'http://carstinstonequotes.com/control-page/' == $_SERVER['HTTP_REFERER']) || $_SESSION['CARSTIN_ADMIN_LOGIN']==1){
									// echo '<p>you have control.</p>';
									
									$_SESSION['CARSTIN_ADMIN_LOGIN'] = 1;
									
									$data = getItemList($mysqli);
									
//print_r($data);
									echo printTable($data);
									
									?>
									</div>
									
									
									<div class="tab-pane <?php echo ($getData=='ColorGraph'?('active'):(''));?>" id="tab7">
									<?php 
									
									/*
									 * Color graph setter
									 */
									
									echo '<form action="/control-page/?tab=ColorGraph" method="post" enctype="multipart/form-data">';
									echo '<label for="file">Color Graph File:</label>
									<input type="file" name="file" id="file"><br>
									<input type="submit" class="btn" name="UpdateColor" value="Update Color Graph">';
									echo '</form>';
									
									?>
									</div>
									<div class="tab-pane <?php echo ($getData=='OrderExcel'?('active'):(''));?>" id="tab8">
									<?php 
									
									/*
									 * Order Excel Page
									 */
									
									echo '<form action="/export-excel/" method="post">';
									echo '<label for="startDate">Start Date:</label>
									<input type="date" name="startDate" id="startDate"><br>
									<label for="endDate">End Date:</label>
									<input type="date" name="endDate" id="endDate"><br>
									<input type="submit" class="btn" name="Download" value="Download">';
									echo '</form>';
									
									?>
									</div>
									<div class="tab-pane <?php echo ($getData=='Price'?('active'):(''));?>" id="tab2">
									<?php 
									
									/*
									 * Price (Stone)
									 */
									
									$sql = "SELECT id, name, description, unit, cost FROM Pricing WHERE name!='*' ORDER BY name ASC";
									
									$pricings = readSQL($mysqli, $sql);
									
									echo '<form action="/control-page/?tab=Price" method="post" class="form-horizontal" role="form">';
									echo printSettingsTable($pricings,'MaterialCost');
									echo '</form>';
									
									?>
									</div>
									
									<div class="tab-pane <?php echo ($getData=='Other'?('active'):(''));?>" id="tab5">
									<?php 
									
									/*
									 * Other
									 */
									
									$sql = "SELECT id, `option`, modifier, value FROM Settings ORDER BY id ASC";
									
									$pricings = readSQL($mysqli, $sql);
									
									echo '<form action="/control-page/?tab=Other" method="post" class="form-horizontal" role="form">';
									echo printSettingsTable($pricings,'Other');
									echo '</form>';
									
									?>
									</div>
									<div class="tab-pane <?php echo ($getData=='StoneOption'?('active'):(''));?>" id="tab3">
									<?php 
									
									/*
									 * Price (Stone Option)
									 */
									
									$sql = "SELECT id, type, description, unit, cost FROM Pricing WHERE type IS NOT NULL ORDER BY name ASC";
									
									$pricings = readSQL($mysqli, $sql);
									
									echo '<form action="/control-page/?tab=StoneOption" method="post" class="form-horizontal" role="form">';
									echo printSettingsTable($pricings, 'StoneOption');
									echo '</form>';
									
									?>
									</div>
									<div class="tab-pane <?php echo ($getData=='CollectionOption'?('active'):(''));?>" id="tab4">
									<?php 
									
									/*
									 * Price (Collection Option)
									 */
									// $getSearch = isset($_GET['search']) ? $_GET['search'] : '';
									
									$sql = "SELECT id, name, ogee, description, cost FROM Pricing_Backend ORDER BY id ASC";
									
									$pricings = readSQL($mysqli, $sql);
									
									echo '<form action="/control-page/?tab=CollectionOption" method="post" class="form-horizontal" role="form">';
									echo printSettingsTable($pricings, 'CollectionOption');
									echo '</form>';
									
									?>
									</div>
									<div class="tab-pane <?php echo ( ($getData=='Dealers' || isset($_GET['search']) )?('active'):(''));?>" id="tab6">
									<?php 
									
									/*
									 * Dealers
									 */
									
									echo '<form class="form-search" action="/control-page/" method="get">';
									echo '<input name="search" type="text" class="input-medium search-query">';
									echo '<button type="submit" class="btn">Search</button>';
									echo '</form>';
									
									echo '<form action="/control-page/" method="get">';
									echo '<input type="hidden" value="single" name="searchSingle">';
									echo '<div class="btn-toolbar" style="margin: 0;" >';
									echo '<div class="btn-group">';
									echo '<button name="search" value="#" type="submit" class="btn">#</button>'; 
									foreach(range('A','Z') as $char){
										echo '<button name="search" value="'.$char.'" type="submit" class="btn">'.$char.'</button>'; 
									}
									echo '</div>';
									echo '</div>';
									echo '</form>';
									
									if(empty($_GET['searchSingle'])){
										$sql = "SELECT `id`,`Ship To Number`, `Name`, `Primary Customer Contact`, `Primary email`, `Phone Number`, `Stone/Solid Surface Discount` 
											FROM DEALERS WHERE `Name` LIKE '%".$_GET['search']."%' ORDER BY id ASC";
									}else{
										if($_GET['search'] == '#'){
											$sql = "SELECT `id`,`Ship To Number`, `Name`, `Primary Customer Contact`, `Primary email`, `Phone Number`, `Stone/Solid Surface Discount` 
												FROM DEALERS WHERE `Name` regexp '^[0-9]+' ORDER BY id ASC";
										}else{
											$sql = "SELECT `id`,`Ship To Number`, `Name`, `Primary Customer Contact`, `Primary email`, `Phone Number`, `Stone/Solid Surface Discount` 
												FROM DEALERS WHERE `Name` LIKE '".$_GET['search']."%' ORDER BY id ASC";
										}
									}
									
									//echo '<pre>'.$_SERVER['QUERY_STRING'].'</pre>';
									
									if(!empty($_GET['search'])){									
										$dealers = readSQL($mysqli, $sql);
										
										echo '<form action="/control-page/?tab=Dealers&'.$_SERVER['QUERY_STRING'].'" method="post" class="form-horizontal" role="form">';
										echo printSettingsTable($dealers, 'Dealers');
										echo '</form>';
									}else{
										
										echo 'Search on your keywords.';
									}
									
									?>
								</div>
							</div>
							
						</div>
						<?php 
								
						}else{
							$_SESSION['CARSTIN_ADMIN_LOGIN'] = 0;
						?>
						
						<?php 
						
						/*

						.##.......####....####...######..##..##..........######...####...#####...##...##.
						.##......##..##..##........##....###.##..........##......##..##..##..##..###.###.
						.##......##..##..##.###....##....##.###..........####....##..##..#####...##.#.##.
						.##......##..##..##..##....##....##..##..........##......##..##..##..##..##...##.
						.######...####....####...######..##..##..........##.......####...##..##..##...##.
						 
						 */
						
						?>
							
							<div>
								<form action="/control-page/" method="post" class="form-horizontal" role="form">
									<div class="control-group">
										<label class="control-label" for="adminUsername">Username</label>
										<div class="controls">
											<input type="text" id="adminUsername" name="adminUsername" required autofocus>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="adminPassword">Password</label>
										<div class="controls">
											<input type="password" id="adminPassword" name="adminPassword"  required>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<button type="submit" class="btn" name="submit" value="1">LOGIN</button>
										</div>
									</div>
								</form>
							</div>
						
						<?php } ?>
					
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->
			
			<?php 
			
			if($mysqli)
			$mysqli->close();
			
			?>

<?php get_footer(); ?>