			<footer role="contentinfo">
			
				<div id="inner-footer" class="clearfix">
		          <hr />
		          <div id="widget-footer" class="clearfix row-fluid">
		            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1') ) : ?>
		            <?php endif; ?>
		            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2') ) : ?>
		            <?php endif; ?>
		            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3') ) : ?>
		            <?php endif; ?>
		          </div>
<!-- //////////////////////////////////////////// -->	

	<div class="no-print well well-small" style="width:40%; position:absolute;">
	<strong>Announcements:</strong><br />	
	<?php query_posts(array('post_type' => 'post', 'posts_per_page' => 2)); 
	while (have_posts()) { the_post();?>

			
			<?php the_date('m-d-Y'); ?> | 
				<a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
			<br />
			
	<?php } wp_reset_query(); ?>
	</div>

<!-- //////////////////////////////////////////// -->
					
					
					
					<?php $file = file_get_contents(get_template_directory().'/ColorFile.ini'); ?>
					<p class="attribution" style="float:right;"><a href="http://dealers.carstinbrands.com/wp-content/themes/wordpress-bootstrap-master/ColorGraph/<?php echo $file; ?>" target="_blank">View Color Menu</a> - Carstin Brands &copy; <?php echo date("Y"); ?></p>
				<br /><br /><br /><br /><br /><br />
				</div> <!-- end #inner-footer -->
				
			</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>