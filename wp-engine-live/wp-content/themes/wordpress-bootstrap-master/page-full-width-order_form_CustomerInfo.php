<?php
/*
Template Name: Full Width Page - Order Form - Customer Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
include_once 'config.php';
?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							echo '<div style="overflow: hidden;width: 100%; padding:1px;">';
							
							echo '<form action="/process-post/?next=/material-color/&from=/customer-info/&session='.$order_session.'" method="post" role="form">';
							echo '<field>';
							echo '<legend>Customer Info</legend>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerFirstName');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerLastName');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerAddress');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerCity');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerState');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerZip');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerPhone');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerAltPhone');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','QuoteID',(isset($_GET['QuoteID'])?$_GET['QuoteID']:NULL));
							echo '</div>';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','CustomerReadyDate');
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<div style="float:left; margin-right:10px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','HighRise');
							DisplayOrderForm($mysqli, 'CustomerInfo','Prior1978');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px; margin-left:20px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','Design');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px; margin-left:20px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','BuildType');
							echo '</div>';
							echo '<div style="float:left; margin-right:10px; margin-left:20px;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','Cabinets');
							echo '</div>';
							echo '<hr style="clear:both;">';
							echo '<strong><em>**Note - Cabinets must be set, secured, and leveled before templating appointment.**</em></strong>';
							echo '<hr style="clear:both;">';
							DisplayOrderForm($mysqli, 'CustomerInfo','OrderNotes');
							echo '<button type="submit" class="btn" name="submit" value="1">Next Step <i class="icon-arrow-right"></i> Material & Color</button>';
							echo '</field>';
							echo '</form>';
							
							echo '</div>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>