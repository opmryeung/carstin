<?php
/*
 Template Name: Full Width Page - Item View
 */
?>
<?php 

include_once 'core-functions.php';
include_once 'config.php';

//phpinfo();

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');

?>
<?php get_header(); ?>

<div id="content" class="clearfix row-fluid">

	<div id="main" class="span12 clearfix" role="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article"> <header>

		<div class="page-header">
			<h1>
			<?php the_title(); ?>
			</h1>
		</div>

		</header> <!-- end article header --> <section class="post_content"> <?php //the_content(); ?>

		<?php
		
		//echo '<pre>';
		if(isset($_POST['item_id']))
		$item = readSQL($mysqli, 'SELECT * FROM ORDER_LOG WHERE id = '.$_POST['item_id']);
		$shapeList = readSQL($mysqli, 'SELECT shape FROM Shape', 'shape');
		
		$data = json_decode(stripslashes($item[0]['DATA']),TRUE);
		
		$choosenShape = $data['shape'];
		
		$shapeList = str_replace(' ', '_', $shapeList);
		
		for($i=0; $i<count($shapeList); $i++){
			if($shapeList[$i] == $choosenShape)
			unset($shapeList[$i]);
		}		

		foreach ($shapeList as $s)
		unset($data[$s]);
		
		//echo '</pre>';
		
		printItemDetails($mysqli, $data);
			
		?> </section> <!-- end article section --> <footer>

		<p class="clearfix">
		<?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?>
		</p>

		</footer> <!-- end article footer --> </article>
		<!-- end article -->

		<?php //comments_template(); ?>

		<?php endwhile; ?>

		<?php else : ?>

		<article id="post-not-found"> <header>
		<h1>
		<?php _e("Not Found", "bonestheme"); ?>
		</h1>
		</header> <section class="post_content">
		<p>
		<?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?>
		</p>
		</section> <footer> </footer> </article>

		<?php endif; ?>

	</div>
	<!-- end #main -->

	<?php //get_sidebar(); // sidebar 1 ?>

</div>
<!-- end #content -->

<?php 

if($mysqli)
$mysqli->close();

?>

<?php get_footer(); ?>