<?php
/*
Template Name: Full Width Page - Order Form - Review Info
*/
?>
<?php 

$order_session = $_GET['session'];
include_once 'core-functions.php';
include_once 'config.php';
//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');
function printReview(mysqli $mysqli, $data){
	
	$list_to_ignore_excountertop = array('DealerName','DealerContact','DealerEmail','DealerPhone','DealerFax','DealerPO','CustomerFirstName','CustomerLastName','CustomerAddress','CustomerCity','CustomerState','CustomerZip','CustomerPhone','CustomerAltPhone','OrderDate','QuoteID','Design','HighRise','CustomerReadyDate','BuildType','Cabinets','Prior1978','OrderNotes');
	
	foreach($data as $d){
		if($d['TYPE']=='countertop'){
			echo '<table class="table table-striped table-condensed">';
			echo '<thead><tr><th><h1><strong>Countertop</strong></h1></th><th>Value</th></tr></thead>';
			echo '<tbody>';
			foreach($d as $k=>$value){
				echo '<tr><td><strong>'.$k.'</strong></td><td><em>'.$d[$k].'</em></td></tr>';
			}
			echo '</tbody>';
			echo '</table>';
		}elseif($d['TYPE']=='ex-countertop'){
			echo '<table class="table table-striped table-condensed">';
			echo '<thead><tr><th><h1><strong>Extra Countertop</strong></h1></th><th>Value</th></tr></thead>';
			echo '<tbody>';
			foreach($d as $k=>$value){
				if(in_array($k, $list_to_ignore_excountertop)){
					//echo '<tr><td><strong>'.$k.'</strong></td><td><em>WTF?</em></td></tr>';
				}else{
					echo '<tr><td><strong>'.$k.'</strong></td><td><em>'.$d[$k].'</em></td></tr>';
				}
			}
			echo '</tbody>';
			echo '</table>';
		}elseif($d['TYPE']=='ex-sink'){
			echo '<table class="table table-striped table-condensed">';
			echo '<thead><tr><th><h1><strong>Sink</strong></h1></th><th>Value</th></tr></thead>';
			echo '<tbody>';
			foreach($d as $k=>$value){
				if(stripos($k, 'Sink') || stripos($k, 'ORDER_GROUP')!==FALSE)
				echo '<tr><td><strong>'.$k.'</strong></td><td><em>'.$d[$k].'</em></td></tr>';
			}
			echo '</tbody>';
			echo '</table>';
		}
		
	}
	
}

?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section style="background-color: #EEEEEE;padding: 10px;">
							<?php 
							
							$parts = explode('-', $order_session);
							
							$order = readSQL($mysqli, 'SELECT * FROM `ORDER` WHERE ORDER_GROUP LIKE \''.$parts[0].'%\'');
							
							echo '<form action="/process-post/?next=/thrower/&from=/review-info/&session='.$order_session.'" method="post"  role="form">';
							echo '<field>';
							
							printReview($mysqli, $order);
							
							$quoteId = explode('-', $order[0]['QuoteID']);
							$quoteId = $quoteId[1];
							
							echo 'By pressing the following button below your order would be finalized.<br/>';
							echo '<input type="hidden" name="QuoteID" value="'.$quoteId.'">';
							echo '<button type="submit" class="btn btn-primary" name="Submit" value="1">Submit Order</button><br/><hr/>';
							echo '<button type="submit" class="btn btn-inverse" name="Submit" value="0">Start Over</button>';
							echo '</field>';
							echo '</form>';
							
							?>
						</section> <!-- end article section -->
						
						<footer>
						
							<p class="clearfix"></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>