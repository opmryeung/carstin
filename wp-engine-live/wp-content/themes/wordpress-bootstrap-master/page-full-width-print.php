<?php
/*
Template Name: Full Width Page - Print
*/
?>
<?php 

include_once 'core-functions.php';
include_once 'config.php';

//$mysqli = new mysqli('localhost','dealersc_mdata','Letmein11','dealersc_data');

$stones = readSQL($mysqli, 'SELECT name FROM Pricing WHERE name != \'*\' GROUP BY name', 'name');

$FullBackMod = readSQL($mysqli, 'SELECT * FROM Settings WHERE `option` = \'Full Height Backsplash SqFt Mod\'');

if(isset($_POST['hidden_json']))
$data = json_decode(stripslashes($_POST['hidden_json']),TRUE);
else $data = $_POST;

$fileName = processUploadedFile();

if(!isset($_POST['DiscardSubmit']) && !isset($_POST['LoadProject']) && !isset($_POST['EditItem']) && !isset($_POST['RemoveItem']))
$store_result = storeData($mysqli, $data, $fileName);

if(isset($_POST['RemoveItem']))
removeItem($mysqli, $_POST['RemoveItem']);

echo '<!-- ';
var_dump($_POST);
echo ' -->';	

$ITEMS = getItemList($mysqli, $_POST['ProjectName'], $_POST['Account'], $_POST['ContactEmail']);


?>

<?php get_header(); ?>

<script type="text/javascript">
function selectRow(row)
{
    var firstInput = row.getElementsByTagName('input')[0];
    firstInput.checked = !firstInput.checked;
}
</script>

			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><strong><?php echo $data['ProjectName']; ?></strong> Pricing Quote</h1></div>
						
						</header> <!-- end article header -->
						
						<section class="post_content">
						
							<?php
							
							//$test = json_decode(stripslashes($ITEMS[count($ITEMS)-1]['DATA']),TRUE);
							
							//echo getCollectionOptionCost($mysqli, $test['option']);
							
							if(TRUE){
							
							$measurements = SQFT_LNFT_DATA($mysqli, $ITEMS,1);
							
							if($ITEMS && !empty($measurements[0]))
							printItemList($ITEMS);
							
							echo SQFT_LNFT_DATA($mysqli, $ITEMS); 
							
							if(!empty($measurements[0])){
							//echo '<h3 style="page-break-before:always;">Carstin Brands Granite/Quartz Collection</h3>';
							//echo COLLECTION_TABLE($mysqli, $ITEMS); 
							
							?>
							<!-- <ul>
								<li>Granite Colors Included:  Black Pearl, Caledonia, New Venetian Gold, Tan Brown, Santa Cecelia, and Uba Tuba.</li>
								<li>Hanstone Colors Included:  Aspen, Capri, Indian Pearl, Rolling Stone, Royal Blanc, and Walnut Luster</li>
								<li>Lg Viatera Colors Included:  Bourbon, Kilauea, Monet, Royal Teak, Silver Lake, and Solar Canyon</li>
							</ul> -->
							
							<?php
							
							echo '<h3 style="page-break-before:always;">General Stone</h3>';
							echo STONES_TABLE($mysqli, $ITEMS); ?>
							
							<div class="no-print">
							<form action="/results/" method="post" class="form-horizontal" role="form">
							<input name="hidden_json" type="hidden" value='<?php echo json_encode($data); ?>'>
							<input name="up_scale" type="text" placeholder="Up Scale Prices (%)" value="<?php echo $_POST['up_scale']; ?>">
							
							<input name="Account" type="hidden" value='<?php echo $data['Account']; ?>'>
							<input name="ProjectName" type="hidden" value='<?php echo $data['ProjectName']; ?>'>
							<input name="ContactName" type="hidden" value='<?php echo $data['ContactName']; ?>'>
							<input name="CompanyName" type="hidden" value='<?php echo $data['CompanyName']; ?>'>
							<input name="ContactPhone" type="hidden" value='<?php echo $data['ContactPhone']; ?>'>
							<input name="ContactEmail" type="hidden" value='<?php echo $data['ContactEmail']; ?>'>
							<input name="Comment" type="hidden" value='<?php echo $data['Comment']; ?>'>
							
							<input type="submit" class="btn" name="CalculationSubmit" value="Submit">
							</form>
							
							<?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
							<form action="/quote-submission/" method="post" class="form-horizontal" role="form">
							<input name="up_scale" type="hidden" value="<?php echo $_POST['up_scale']; ?>">
							
							<input name="Account" type="hidden" value='<?php echo $data['Account']; ?>'>
							<input name="ProjectName" type="hidden" value='<?php echo $data['ProjectName']; ?>'>
							<input name="ContactName" type="hidden" value='<?php echo $data['ContactName']; ?>'>
							<input name="CompanyName" type="hidden" value='<?php echo $data['CompanyName']; ?>'>
							<input name="ContactPhone" type="hidden" value='<?php echo $data['ContactPhone']; ?>'>
							<input name="ContactEmail" type="hidden" value='<?php echo $data['ContactEmail']; ?>'>
							<input name="Comment" type="hidden" value='<?php echo $data['Comment']; ?>'>
							<a class="btn btn-info" onClick="window.print()">Print</a>
							<input type="submit" class="btn btn-success" name="FINAL_SUBMIT" value="Finalize Quote and Submit Porject">
							</form>
							</div>
							
							<p class="text-right">All Quotes are based on information provided by Dealer. Quotes are subject to change after template is completed.</p>
							
							<?php
							
							}else{
								echo '<br/><hr/>';
								echo '<h3 class="text-error">Data was insufficient<br/>Please return to Calculation page and check your measurements.</h3>';	
							}
							
							}
							
							?>
							
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","bonestheme") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php //comments_template(); ?>
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->
			
			<?php

			/*echo '<pre>';
			echo !empty($debug_string)?$debug_string:'';
			echo '</pre>';
			*/
			
			if($mysqli)
			$mysqli->close();
			
			?>

<?php get_footer(); ?>